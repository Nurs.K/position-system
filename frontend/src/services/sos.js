import client from '../helpers/client';
import axios from 'axios';

export const sendSos = (keeperId) => {
  return client.post(process.env.REACT_APP_LOCAL_API_BASE_URL + `/BA-API/v0.1/tag_signal/${keeperId}`, {});
};

export const sendGroupSos = (keeperIds) => {
  const requestsList = keeperIds.map((id) => sendSos(id));
  return axios.all(requestsList);
};
