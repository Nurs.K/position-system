import client from '../helpers/client';

const user = {
  changePassword,
  getData,
};

function changePassword(login, body) {
  return client.patch(process.env.REACT_APP_LOCAL_API_BASE_URL + `/BA-API/v0.1/users/${login}/pass`, body);
}

function getData(login) {
  return client.get(process.env.REACT_APP_LOCAL_API_BASE_URL + `/BA-API/v0.1/users/${login}`);
}

export default user;
