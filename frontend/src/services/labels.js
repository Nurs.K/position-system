import client from '../helpers/client';
import store from '../store/index';

import {
  setLabelsData,
  setLabelsError,
  setLabelsLoading,
} from '../store/actions/labels';

export const getLabels = () => {
  store.dispatch(setLabelsLoading(true));
  // ?offset=0&sort=label
  return client
    .get(process.env.REACT_APP_LOCAL_API_BASE_URL + '/labels/')
    .then((res) => {
      store.dispatch(setLabelsData(res.data));
    })
    .catch((err) => {
      store.dispatch(setLabelsError(err));
    })
    .finally(() => {
      store.dispatch(setLabelsLoading(false));
    });
};
