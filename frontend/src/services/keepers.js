import client from '../helpers/client';
import store from '../store';
import {
  setKeepersData,
  setKeepersError,
  setKeepersLoading,
} from '../store/actions/keepers';

export const getKeepers = (headers) => {
  store.dispatch(setKeepersLoading(true));
  return client
    .get(process.env.REACT_APP_LOCAL_API_BASE_URL + '/keepers', { params: headers })
    .then((res) => {
      store.dispatch(setKeepersData(res.data));
      return res;
    })
    .catch((err) => {
      store.dispatch(setKeepersError(err));
    })
    .finally(() => {
      store.dispatch(setKeepersLoading(false));
    });
};
