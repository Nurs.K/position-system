import client from '../helpers/client';
import store from '../store/index';
import {
  setNotificationsData,
  setNotificationsError,
  setNotificationsLoading,
} from '../store/actions/notifications';

export const getNotifications = () => {
  store.dispatch(setNotificationsLoading(true));

  return client
    .get(process.env.REACT_APP_LOCAL_API_BASE_URL + '/BA-API/v0.1/notifications')
    .then((res) => {
      const list = [...res.data];
      store.dispatch(setNotificationsData(list));
      return list;
    })
    .catch((err) => {
      store.dispatch(setNotificationsError(err));
    })
    .finally(() => {
      store.dispatch(setNotificationsLoading(false));
    });
};
