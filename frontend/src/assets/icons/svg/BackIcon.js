import React from "react";

export default function BackIcon() {
    return (
        <svg width="97" height="102" viewBox="0 0 97 102" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g filter="url(#filter0_d)">
                <path d="M32.2871 28.4275C30.3451 29.5927 30.3451 32.4073 32.2871 33.5725L61.7895 51.2739C63.7891 52.4737 66.333 51.0333 66.333 48.7014L66.333 13.2986C66.333 10.9667 63.7891 9.52635 61.7895 10.7261L32.2871 28.4275Z" fill="#25ACE3"/>
            </g>
            <defs>
                <filter id="filter0_d" x="0.830627" y="0.293945" width="95.5024" height="101.412" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                    <feOffset dy="20"/>
                    <feGaussianBlur stdDeviation="15"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
            </defs>
        </svg>
    );
};