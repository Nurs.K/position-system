import React from "react";

export default function NextIcon() {
    return (
        <svg width="96" height="102" viewBox="0 0 96 102" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g filter="url(#filter0_d)">
                <path d="M64.0455 28.4275C65.9876 29.5927 65.9876 32.4073 64.0455 33.5725L34.5432 51.2739C32.5436 52.4737 29.9997 51.0333 29.9997 48.7014L29.9997 13.2986C29.9997 10.9667 32.5436 9.52635 34.5432 10.7261L64.0455 28.4275Z" fill="#25ACE3"/>
            </g>
            <defs>
                <filter id="filter0_d" x="-0.000305176" y="0.293945" width="95.5024" height="101.412" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                    <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                    <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                    <feOffset dy="20"/>
                    <feGaussianBlur stdDeviation="15"/>
                    <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.15 0"/>
                    <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/>
                    <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/>
                </filter>
            </defs>
        </svg>
    );
};