import { ReactComponent as TrackIcon } from './track.svg';
import { ReactComponent as PlayIcon } from './play.svg';
import { ReactComponent as SquareSolidIcon } from './square-solid.svg';

export { TrackIcon, PlayIcon, SquareSolidIcon };
