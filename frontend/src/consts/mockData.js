export const mockKeepers = [
  {
    id: 1,
    name: 'Тест Murat ',
    _links: {
      self: {
        id: 1,
        label: 'Тест С. ',
        type: 'employees',
        href: '/employees/1',
      },
    },
    type: 'EMPLOYEE',
    deleted: false,
    _embedded: {
      tag: {
        status: 'IDLE',
        _links: {
          self: {
            id: 20002191,
            label: '20002191',
            type: 'tags',
            href: '/tags/20002191',
          },
        },
        site_id: 29,
        deleted: false,
        since_lastupdate: 4,
        sensors: {
          beacons: [],
          temperature: '30',
          tag_cfg_site: '0',
          motion: 'STILL',
          barometer: '-9719',
          orientation: 'ZH',
          impact_fall: 'NORMAL',
          battery_state: 'GOOD',
          barometer_temperature: '28',
        },
        id: 20002191,
        time: '2021-08-24T12:44:25.989+03:00',
        descr: '',
        z: 0.5,
        y: 0.14,
        x: 0.99,
      },
    },
  },
];

export const mockKeeper = {
  id: 1,
  name: 'Сергеев Сергей Сергеевич',
  _links: {
    self: {
      id: 1,
      label: 'Тест С. ',
      type: 'employees',
      href: '/employees/1',
    },
  },
  type: 'EMPLOYEE',
  deleted: false,
  _embedded: {
    tag: {
      status: 'IDLE',
      _links: {
        self: {
          id: 20002191,
          label: '20002191',
          type: 'tags',
          href: '/tags/20002191',
        },
      },
      site_id: 29,
      deleted: false,
      since_lastupdate: 4,
      sensors: {
        beacons: [],
        temperature: '7',
        tag_cfg_site: '0',
        motion: 'Подвижность',
        barometer: '-4568',
        orientation: 'YH',
        impact_fall: 'Normal',
        battery_state: '50%',
        barometer_temperature: '11',
      },
      id: 20002191,
      time: '2021-08-24T12:44:25.989+03:00',
      descr: '',
      z: 0.5,
      y: 0.14,
      x: 0.15,
    },
  },
};

export const mockNotifications = [
  {
    message:
      'Иван Иванов нажал на кнопку красного сигнала',
    type: 'AREA_ENTER',
    time: '2021-08-27T12:59:25.111+03:00',
    showPopup: false,
    showBlockingPopup: true,
    playSound: true,
  }
];

export const mockSandwichData = [
  {
    title: 'Дробление',
    list: [
      {
        label: 'дробление',
        height: 18.9,
        blockColor: 'red',
        amount: 0,
      },
      {
        label: 'дробление',
        height: 16.4,
        blockColor: 'yellow',
        amount: 0,
      },
      {
        label: 'дробление',
        height: 11.3,
        blockColor: 'green',
        amount: 0,
      },
      {
        label: 'дробление',
        height: 8,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: 'дробление',
        height: 5,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: 'дробление',
        height: 0,
        blockColor: 'white',
        amount: 0,
      },
    ],
  },
  {
    title: 'Галереи/тоннель',
    list: [
      {
        label: 'Галереи',
        height: null,
        blockColor: 'red',
        amount: 23,
      },
      {
        label: 'дробление',
        height: 4.4,
        blockColor: 'yellow',
        amount: 27,
      },
      {
        label: 'растарка извести',
        height: 2.2,
        blockColor: 'green',
        amount: 14,
      },
      {
        label: 'дробление',
        height: 2.1,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: 'Тоннель',
        height: null,
        blockColor: 'white',
        amount: 0,
      },
    ],
  },
  {
    title: 'Основное здание',
    list: [
      {
        label: 'измельчение',
        height: 17,
        blockColor: 'red',
        amount: 23,
      },
      {
        label: 'регенерация',
        height: 16,
        blockColor: 'yellow',
        amount: 27,
      },
      {
        label: '14 - измельчение, 12 - сорбция, 8 - сорбция',
        height: null,
        blockColor: 'green',
        amount: 14,
      },
      {
        label: 'измельчение',
        height: 11.2,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: 'регенерация',
        height: 9.43,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: '6.2 - измельчение, 5 - собрция',
        height: null,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: 'цех ГП',
        height: 4,
        blockColor: 'white',
        amount: 0,
      },
      {
        label: 'элюация',
        height: 3.65,
        blockColor: 'white',
        amount: 0,
      },
      // {
      //   label: '3.11 - регенерация',
      //   blockColor: 'white',
      //   amount: 0,
      // },
      // {
      //   label: '0 - ЗИиОФ',
      //   blockColor: 'white',
      //   amount: 0,
      // },
    ],
  },
  {
    title: 'Сорбция лестница, выщелачивание, регенерация',
    list: [
      {
        label: '19 - сорбция лестница',
        blockColor: 'red',
        amount: 23,
      },
      {
        label: '19 - выщелачивание',
        blockColor: 'yellow',
        amount: 27,
      },
      {
        label: '15 - сорбция лестница',
        blockColor: 'green',
        amount: 14,
      },
      {
        label: '12 - сорбция лестница',
        blockColor: 'white',
        amount: 0,
      },
      {
        label: '9.3 - сорбция лестница',
        blockColor: 'white',
        amount: 0,
      },
      {
        label: '6 - сорбция лестница',
        blockColor: 'white',
        amount: 0,
      },
      {
        label: '4 - растворение',
        blockColor: 'white',
        amount: 0,
      },
      // {
      //   label: '3 - сорбция лестница',
      //   blockColor: 'white',
      //   amount: 0,
      // },
      // {
      //   label: '0 - растворение',
      //   blockColor: 'white',
      //   amount: 0,
      // },
    ],
  },
];
