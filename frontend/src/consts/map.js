export const accessToken = process.env.REACT_APP_LEAFLET_ACCESS_TOKEN || "";
export const mapTileLayerUrl = 'https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png';
export const mapTileAttribution = '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';