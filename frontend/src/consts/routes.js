import Authorization from '../pages/Authorization';
import UploadMap from '../pages/UploadMap';
import EditMap from '../pages/EditMap';
import ShowMap from '../pages/ShowMap/ShowMap';
import Sandwich from '../pages/Sandwich';
import CategoriesPage from '../pages/Settings/Categories';
import SettingsPage from '../pages/Settings';
import NotificationConfiguration from '../pages/Settings/Notifications';
import MapMarkerConfiguration from '../pages/Settings/Markers';
import MapsConfiguration from '../pages/Settings/Maps';
import DrawPolygons from '../pages/DrawPolygons';
import ReportsPage from '../pages/Reports';
import PutAnchors from "../pages/PutAnchors";
import Home from "../pages/Home";
import ChartsPage from "../pages/Statistics";
import DevicesPage from '../pages/Settings/Devices';
import Macroscop from '../pages/Settings/Macroscop';

import {
  UPLOAD_MAP,
  SHOW_MAP,
  EDIT_MAP,
  SHOW_MAP_ID,
  EDIT_MAP_ID,
  DRAW_POLYGONS,
  DRAW_POLYGONS_MAP_ID,
  LOGIN,
  REPORTS,
  MAIN,
  SANDWICH,
  SETTINGS,
  SETTINGS_CATEGORIES,
  SETTINGS_MAPS,
  SETTINGS_NOTIFICATIONS,
  SETTINGS_MAP_MARKERS,
  PUT_ANCHORS,
  PUT_ANCHORS_URL,
  STATISTICS,
  SETTINGS_DEVICES,
  SETTINGS_MACROSCOP,
} from './urls';

export const toShowMapId = (mapId) => SHOW_MAP + mapId;
export const toEditMapId = (mapId) => EDIT_MAP + mapId;
export const toDrawPolygonsMapId = (mapId) => DRAW_POLYGONS + mapId;
export const toPutAnchors = mapId => `${PUT_ANCHORS}/${mapId}`;

export const ROUTES = [
  {
    isPrivate: false,
    exact: true,
    path: LOGIN,
    component: Authorization,
  },
  {
    isPrivate: true,
    exact: true,
    path: MAIN,
    component: Home,
  },
  {
    isPrivate: true,
    exact: true,
    path: UPLOAD_MAP,
    component: UploadMap,
  },
  {
    isPrivate: true,
    exact: true,
    path: EDIT_MAP_ID,
    component: EditMap,
  },
  {
    isPrivate: true,
    exact: true,
    path: SHOW_MAP_ID,
    component: ShowMap,
  },
  {
    isPrivate: true,
    exact: true,
    path: SANDWICH,
    component: Sandwich,
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS,
    component: SettingsPage,
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS_CATEGORIES,
    component: CategoriesPage,
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS_NOTIFICATIONS,
    component: NotificationConfiguration,
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS_MAP_MARKERS,
    component: MapMarkerConfiguration,
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS_MAPS,
    component: MapsConfiguration,
  },
  {
    isPrivate: true,
    exact: true,
    path: DRAW_POLYGONS_MAP_ID,
    component: DrawPolygons,
  },
  {
    isPrivate: true,
    exact: true,
    path: PUT_ANCHORS_URL,
    component: PutAnchors
  },
  {
    isPrivate: true,
    exact: true,
    path: REPORTS,
    component: ReportsPage,
  },
  {
    isPrivate: true,
    exact: true,
    path: STATISTICS,
    component: ChartsPage
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS_DEVICES,
    component: DevicesPage,
  },
  {
    isPrivate: true,
    exact: true,
    path: SETTINGS_MACROSCOP,
    component: Macroscop,
  }
];
