export const EMPLOYEES = 'employees';
export const USERS = 'users';
export const ITEMS = 'items';
export const AREAS = 'areas';
export const ANCHORS = 'anchors';

export const keysCollections = {
  [EMPLOYEES]: [
    'lastname',
    'name',
    'secondname',
    'organization',
    'descr',
    'labels',
    'tag.id',
  ],
  [ITEMS]: ['name', 'descr', 'labels', 'tag.id'],
  [AREAS]: ['name', 'descr', 'labels', 'map.name'],
  [ANCHORS]: ['sn', 'ip_address', 'status'],
  [USERS]: ['login', 'descr', 'role.name'],
};
export const NOT_DELETED = 'deleted==false';

export const floorOptions = [
  { id: 1, name: 1 },
  { id: 2, name: 2 },
  { id: 3, name: 3 },
  { id: 4, name: 4 },
  { id: 5, name: 5 },
];

export const objectTypeOptions = [
  { id: EMPLOYEES, name: 'Сотрудники' },
  { id: ITEMS, name: 'Обьекты' },
  { id: AREAS, name: 'Области' },
  { id: ANCHORS, name: 'Анкеры' },
  { id: USERS, name: 'Пользователи' },
];

export const defaultParams = {
  size: 10
}
