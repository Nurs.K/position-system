import { safeGet } from "../../helpers/getters";

const selectState = state => state.cameras;

export const makeSelectIsLoading = state => safeGet(selectState(state), 'config.fetching', false);
export const makeSelectConfigData = state => safeGet(selectState(state), 'config.data');

export const makeSelectIsEditFormOpen = state => safeGet(selectState(state), 'updating.isOpen', false);
export const makeSelectUpdatingLoading = state => safeGet(selectState(state), 'updating.fetching', false);
