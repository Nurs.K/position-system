const getState = state => state.search

export const makeSelectSearchData = state => getState(state).searchState.data?.items;

