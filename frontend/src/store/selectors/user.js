const getState = state => state.user

export const makeSelectGetFavorites = state => getState(state).favorites.data?.results

export const makeSelectGetFrequentlyVisited = state => getState(state).frequentlyVisited.data?.results

export const makeSelectGetHistory = state => getState(state).history.data?.results

export const makeSelectGetUserColor = state => getState(state).color.data
