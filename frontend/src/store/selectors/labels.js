const getState = state => state.labels;

export const makeSelectIsUserDrawerVisible = state => getState(state).isUserDrawerVisible
export const makeSelectIsFavoriteDrawerVisible = state => getState(state).isFavoriteDrawerVisible
