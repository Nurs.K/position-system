const getState = state => state.keepers;

export const makeSelectKeepersData = state => getState(state).keepersData
