const getState = state => state.notifications

export const makeSelectGetNotifications = state => getState(state).allNotifications
export const makeSelectGetBlockingNotifications = state => getState(state).blockingNotifications
export const makeSelectGetShowPopupNotifications = state => getState(state).bottomRightNotifications
