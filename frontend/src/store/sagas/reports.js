import { takeLatest, all, call, put } from 'redux-saga/effects';
import {
  GET_MILEAGE_POINT_REQUEST,
  getMileagePointSuccess,
  getMileagePointFailure,
} from '../actions/reports';
import client from '../../helpers/client';

function* getMileagePoint({ payload }) {
  const { slug, params } = payload;
  try {
    const response = yield call(
      client.get,
      `${process.env.REACT_APP_LOCAL_API_BASE_URL}/BA-API/v0.1/reports/mileage/point/${slug}`,
      { params }
    );

    yield put(getMileagePointSuccess(response.data));
  } catch (err) {
    yield put(getMileagePointFailure(err));
  }
}

function* Saga() {
  yield all([takeLatest(GET_MILEAGE_POINT_REQUEST, getMileagePoint)]);
}

export default Saga;
