import { takeLatest, all, call, put } from 'redux-saga/effects';
import {
  SEARCH_REQUEST,
  setSearchError,
  setSearchLoading,
  setSearchData,
} from '../actions/search';
import client from '../../helpers/client';
import { errorNotification } from '../../helpers/notificationHelpers';

function* getSearch({ payload }) {
  const { slug, params } = payload;
  try {
    yield put(setSearchLoading(true));
    const response = yield call(client.get, `${process.env.REACT_APP_LOCAL_API_BASE_URL}/BA-API/v0.1/${slug}`, { params });

    const sortedItems = response.data.items.sort((a, b) => {
      if(a.name === b.name){
        // last name is important when names are same
        return ('' + a.lastname).localeCompare(b.lastname);
      }
      return ('' + a.name).localeCompare(b.name);
    })
    const data = {...response.data, items: sortedItems}

    yield put(setSearchData(data));
  } catch (err) {
    const {
      status,
      data: { descr },
    } = err.response;

    errorNotification(`Код ошибки ${status}. Сообщение ошибки "${descr}"`);
    yield put(setSearchError(err));
  } finally {
    yield put(setSearchLoading(false));
  }
}

function* Saga() {
  yield all([takeLatest(SEARCH_REQUEST, getSearch)]);
}

export default Saga;
