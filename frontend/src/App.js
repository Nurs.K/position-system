import React, { useEffect, useState } from 'react';
import { Switch, Route, useHistory } from 'react-router-dom';
import { useDispatch, useSelector, connect } from "react-redux";
import {notification} from "antd";

import ContentLayout from './hoc/ContentLayout';
import PrivateRoute from './components/PrivateRoute';
import {REPORTS} from './consts/urls';
import {ROUTES} from './consts/routes';

import { selectConfigList } from './store/selectors/notificationConfigSelectors';
import { getListRequest } from './store/actions/notificationConfigurations';
import { setNotificationsData } from './store/actions/notifications';
import { getMainMapFetching } from './store/actions/svgMap';
import { getNotifications } from './services/notifications';
import { showPopupNotification } from './helpers/notificationHelpers';

import './App.css';
import { getLabels } from './services/labels';
import {getUserColorRequest} from "./store/actions/user";

function App({ isAuthenticated }) {

  const configList = useSelector(selectConfigList)

  const dispatch = useDispatch()
  const history = useHistory()


  const goToReports = () => {
    history.push(REPORTS)
    notification.destroy()
  }

  const requestNotifications = () => {
    if (isAuthenticated) {
      // need remove to store
      getNotifications().then((list = []) => {
        setNotificationsData(list)
        showPopupNotification(list.filter(({ showPopup }) => showPopup), configList, goToReports);
      });
    }
  };

  useEffect(() => {
    if (isAuthenticated) {
      getLabels();
    }

    if(configList.length) {
      const notificationsInterval = setInterval(requestNotifications, 15000);
      return () => {
        clearInterval(notificationsInterval);
      };
    }
  }, [isAuthenticated, configList]); // eslint-disable-line

  useEffect(() => {
    dispatch(getListRequest())
    dispatch(getMainMapFetching())
    dispatch(getUserColorRequest())
  }, []) // eslint-disable-line

  return (
    <ContentLayout isAuthenticated={isAuthenticated}>
      <Switch>
        {ROUTES.map(({ isPrivate, ...rest }, idx) => {
          return isPrivate ? <PrivateRoute {...rest} key={`POSITION_ROUTE_${idx}`} /> : <Route {...rest} key={`POSITION_ROUTE_${idx}`} />;
        })}
      </Switch>
    </ContentLayout>
  );
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated,
  };
}

export default connect(mapStateToProps)(App);
