
export const getQuery = () => {
  const url = new URL(document.location.href);
  const result = {};

  for (const key of url.searchParams.keys()) {
    result[key] = url.searchParams.get(key);
  }

  return result;
};
