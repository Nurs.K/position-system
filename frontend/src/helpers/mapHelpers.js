import L from 'leaflet';
import 'leaflet-rotatedmarker';
import 'leaflet-freehandshapes';
import 'leaflet-providers';
import "leaflet-draw"
import "leaflet-draw/dist/leaflet.draw.css"

import {mapTileAttribution, mapTileLayerUrl} from '../consts/map';
import countries from "../consts/countries.json";

export const GLOBAL_MAP_MAX_ZOOM = 22;


export const initGlobalMap = ({ mapId, position }) => {
  clearMap(mapId);

  const map = L.map(mapId, {
    maxZoom: GLOBAL_MAP_MAX_ZOOM,
    attributeControl: false,
    zoomControl: false,
  }).setView(position, 17);

  map.off();

  L.tileLayer(mapTileLayerUrl, {
    maxZoom: GLOBAL_MAP_MAX_ZOOM,
    attribution: mapTileAttribution,
    maxNativeZoom: 19,
  }).addTo(map);

  return map;
}

export const initMap = ({ mapId, image, bounds }) => {
  const map = L.map(mapId, {
    crs: L.CRS.Simple,
    zoomControl: false,
  });
  map.off()

  L.imageOverlay(image, bounds).addTo(map);

  map.fitBounds(bounds);

  // map.zoomIn(1);
  // map.zoomOut(5);

  return map;
};

export const zoomInMap = ({map}) => {
  map.zoomIn()
}

export const zoomOutMap = ({map}) => {
  map.zoomOut()
}

export function clearMap(map = 'map') {
  const container = L.DomUtil.get(map);
  if (container != null) {
    container._leaflet_id = null;
  }
}

export const createMarker = ({
  latLng,
  draggable,
  setPosition,
  rotationAngle,
  icon,
}) => {
  const options = {
    draggable,
    rotationAngle,
  };

  if (icon) {
    options.icon = icon;
  }

  const marker = L.marker(latLng, options);

  marker.on('dragend', () => {
    if (setPosition) setPosition(marker.getLatLng());
  });

  marker.on('move', () => {
    if (setPosition) setPosition(marker.getLatLng());
  });

  return marker;
};

export const addMarker = ({
  map,
  latLng,
  draggable,
  setPosition,
  rotationAngle,
  icon,
  onClick,
}) => {
  const marker = createMarker({
    latLng,
    draggable,
    setPosition,
    rotationAngle,
    icon,
  });

  if (onClick) {
    marker.off('click', onClick(marker));
    marker.on('click', onClick(marker));
  }

  marker.addTo(map);
  return marker;
};

export const addPolyLine = ({ map, latLngs, options }) => {
  return L.polyline(latLngs, options).addTo(map);
};

export const addPolygon = ({ map, coords, tooltip = {}, options = {}, handleShapeClick }) => {
  const polygon = L.polygon(coords, options);
  polygon.on('click', handleShapeClick)

  if (tooltip.text) {
    polygon.bindTooltip(tooltip.text, Object.assign({ sticky: true }, tooltip.options));
  }

  polygon.addTo(map);
  return polygon;
};

export const addColorToLayer = ({mapColor, colorLayer}) => {
  if(colorLayer){
    colorLayer.setStyle({fillColor: mapColor, fillOpacity: '0.2'})
  }
}

export const createColorLayer = ({map, style, setColorLayer}) => {
  const geoJsonLayer = L.geoJson(countries.features, {style})
  geoJsonLayer.addTo(map)
  setColorLayer(geoJsonLayer)
}

export const addGeoJSON = ({ map, geoJSON, options }) => {
  const geoJSONLayer = L.geoJson(geoJSON, options);
  geoJSONLayer.addTo(map);
  return geoJSONLayer;
};

export const addImageOverlay = ({ map, image, bounds, options }) => {
  const overlay = L.imageOverlay(image, bounds, options);
  overlay.addTo(map);
  return overlay;
};

export const resetImageOverlay = ({imageOverlay, image}) => {
  imageOverlay.setUrl(image)
}

export const addLayerGroup = ({ map, markers }) => {
  return L.layerGroup(markers).addTo(map);
};

export const onClickMarker = ({ marker, setVisible}) => {
  marker.on('click', () => setVisible());
};

export const convertCoordinatesToLatLng = (coordinate) => {
  const [x, y] = coordinate;
  return [y, x];
};

export const getMarkerByType = (type, config) => {
  if (!config) return null;

  const target = config.find((item) => item['marker_type'] === type);
  if (!target) return null;

  return L.icon({ iconUrl: target.icon, iconSize: [24, 24], className: '' });
};

export const initDrawer = ({ map }) => {
  const drawer = new L.FreeHandShapes({
    simplify_tolerance: 0.0001,
  });

  map.addLayer(drawer);
  return drawer;
};

export const setDrawMode = (drawer) => {
  drawer.setMode('add');
};

export const setDrawerToolbar = ({map, removeSelectedLayer, setToolbarLayer}) => {
  removeSelectedLayer()

  const editableLayers = new L.FeatureGroup();
  map.addLayer(editableLayers);

  const drawPluginOptions = {
    position: 'topright',
    draw: {
      polygon: {
        allowIntersection: false, // Restricts shapes to simple polygons
        drawError: {
          color: '#e1e100', // Color the shape will turn when intersects
          message: '<strong>Извините!<strong> Вы не можете нарисовать так!' // Message that will show when intersect
        },
        shapeOptions: {
          color: '#97009c'
        },
      },
      // disable toolbar item by setting it to false
      polyline: false,
      circle: true, // Turns off this drawing tool
      rectangle: true,
      marker: false,
      circlemarker: false,
    },
    edit: {
      featureGroup: editableLayers,
      remove: true,
    }
  };

  const drawControl = new L.Control.Draw(drawPluginOptions)
  map.addControl(drawControl);

  setToolbarLayer(drawControl)

  // L.drawLocal = {
  //   draw: {
  //     toolbar: {
  //       actions: {
  //         title: 'Отмена',
  //         text: 'Отмена'
  //       },
  //       finish: {
  //         title: 'Завершить',
  //         text: 'Завершить',
  //       },
  //       undo: {
  //         title: 'Отменить последнее действие',
  //         text: 'Отменить последнее действие'
  //       },
  //       buttons: {
  //         polygon: 'Полигон',
  //         rectangle: 'Прямоугольник',
  //         circle: 'Круг',
  //       }
  //     },
  //     handlers: {
  //       circle: {
  //         tooltip: {
  //           start: 'Начало круга'
  //         },
  //         radius: 'Круг'
  //       },
  //       polygon: {
  //         tooltip: {
  //           start: 'Поставьте точку',
  //           cont: 'Следующая точка',
  //           end: 'Чтобы закончить выберите первую точку'
  //         }
  //       },
  //       rectangle: {
  //         tooltip: {
  //           start: 'Нажмите и тяните'
  //         }
  //       },
  //     }
  //   },
  // };


  map.on('draw:created', function(e) {
    map.removeLayer(e)
    const type = e.layerType,
      layer = e.layer;

    if (type === 'circle'){
      console.log(e)
    }

    editableLayers.addLayer(layer);
    // handlePolygonAdd(e)
  });

  map.on('draw:drawstart', function(){
    editableLayers.clearLayers()
  })

  map.on('draw:edited', function(e){
    const layers = e.layers;
    map.removeLayer(e)
    // editableLayers.clearLayers();
    layers.eachLayer(function (layerItem) {
      //do whatever you want; most likely save back to db
      const type = layerItem.layerType,
        layer = layerItem.layer;

      if (type === 'circle'){
        console.log(layerItem)
      }

      editableLayers.addLayer(layer);
      // handlePolygonAdd(e)
    });

    // editableLayers.addLayer(layer);
    // handlePolygonAdd(e)
  })
}

export const deleteDrawerToolbar = ({map}) => {
  map.removeLayer()
};

export const setViewMode = (drawer) => {
  drawer.setMode('view');
};

export const initAnchorPlacer = (map, handleMarkerAdded) => {
  const onMapClick = e => {
    const marker = addMarker({
      map,
      latLng: e.latlng,
      draggable: false,
      onClick: marker => () => {
        map.removeLayer(marker);
      }
    });

    if (handleMarkerAdded) {
      handleMarkerAdded(marker, e.latlng);
    }
  }

  if (map) {
    map.off('click', onMapClick);
    map.on('click', onMapClick);
  }
};

export const removeAnchorPlacer = (map) => {
  if(map){
    map.off('click')
  }
}

export const removeAllLayersAndResetImage = ({bounds, image, map}) => {
  // if(map){
    map.eachLayer((layer) => {
      if(map){
        map.removeLayer(layer);
      }
    });

    addImageOverlay({map, image, bounds})
    // if (!map && bounds) setMap(initMap({ mapId, image, bounds }));
  // }
}

export const getGridLayer = (opts = {}) => {
  let grid = '<ul class="tile-child-grid">';

  for (let i = 0; i < 5; ++i) {
    grid += '<li class="grid-row"><ul>';
    for (let k = 0; k < 5; ++k) {
      grid += '<li class="grid-column"></li>';
    }
    grid += '</ul></li>';
  }

  grid += '</ul>';

  const Klass = L.GridLayer.extend({
    createTile: function () {
      const tile = document.createElement('div');
      tile.style.outline = '1px solid rgba(0, 0, 0, 0.4)';
      tile.style.fontWeight = 'bold';
      tile.style.fontSize = '14pt';
      tile.innerHTML = grid;
      return tile;
    },
  });

  return new Klass(opts);
}

export const removeMap = ({map}) => {
  map.remove();
}
