import React from 'react';
import { notification } from 'antd';
import { ExclamationCircleFilled } from '@ant-design/icons';

import mp3Url from '../../src/assets/mp3/notification.mp3';
import ShowTime from "../pages/Notification/ShowTime";
import ShowMessage from "../pages/Notification/ShowMessage";

export const playSound = (url = mp3Url) => {
  const audio = new Audio(url);
  const playPromise = audio.play();

  if (playPromise !== undefined) {
    playPromise
      .then(_ => {
        // Automatic playback started!
        // Show playing UI.
        console.log("audio played auto");
      })
      .catch(error => {
        // Auto-play was prevented
        // Show paused UI.
        console.log("playback prevented", error);
      });
  }
};

export const showPopupNotification = (list, configList, goToReports) => {
  let notifications = []

  list?.forEach((item) => {
    configList.forEach(configItem => {
      let obj = {}
      if(item.type === configItem.event_type) {
        obj = {...item, ...configItem}
        notifications.push(obj)
        return notifications
      }
    })
  });

  if(notifications.length) {
    notifications.forEach(((item, idx) => {
      if(item.playSound) playSound(item.audio);
      notification.open({
        message: <ShowTime idx={idx} image={item.thumbnail} title={item.time}/>,
        description: <ShowMessage handleClick={goToReports} type={item.type} title={item.message}/>,
        placement: 'bottomRight',
        className: "showPopup",
        duration: 8
      });
    }))
  }
};

export const successNotification = (message) => {
  notification.success({
    message,
    className: 'success',
    icon: <ExclamationCircleFilled />,
  });
};

export const errorNotification = (message) => {
  notification.error({
    message,
    className: 'error',
    icon: <ExclamationCircleFilled />,
  });
};

export const filterBlockingNotifications = (payload) => payload?.filter(item => !!item.showBlockingPopup)
export const filterBottomRightNotifications = (payload) => payload?.filter(item => !!item.showPopup)
