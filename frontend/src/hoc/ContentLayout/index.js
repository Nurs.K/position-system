import React, {useState} from 'react';
import {Link} from 'react-router-dom';
import {useSelector, useDispatch} from "react-redux";
import {Layout, Drawer, Modal} from 'antd';
import moment from "moment";

import HeaderNav from '../../components/HeaderNav';
import SwtichGroup from '../../components/SwtichGroup';

import {
  REPORTS,
} from '../../consts/urls';
import {
  makeSelectGetBlockingNotifications,
} from "../../store/selectors/notifications";
import {
  deleteBlockingNotification,
} from "../../store/actions/notifications";

import {sideBarMenuItems} from "./consts";
import LinkPage from "./LinkPage";

const { Content } = Layout;


function ContentLayout (props) {
  const { isAuthenticated } = props;
  const [visible, setVisible] = useState(false);

  const blockingNotificationsList = useSelector(makeSelectGetBlockingNotifications)

  const dispatch = useDispatch()

  const closeDrawer = () => setVisible(false);

  if (!isAuthenticated) {
    return (
      <>
        {/* {isLoading && <Loader />} */}
        {props.children}
      </>
    );
  }

  const deleteBlockingPopup = (idx) => {
    dispatch(deleteBlockingNotification(idx))
  }

  return (
    <>
      <Layout style={{ height: '100vh' }}>
        <Layout className="site-layout">
          <Drawer
            width={350}
            placement={'left'}
            onClose={closeDrawer}
            visible={visible}
            key={'left'}
            footer={
              <div className="d-flex-center-center mb-38">
                <SwtichGroup
                  list={[
                    { id: 0, title: 'KZ' },
                    { id: 1, title: 'RU' },
                    { id: 2, title: 'EN' },
                  ]}
                />
              </div>
            }
          >
            <div className="auth-container__logo"></div>
            <div className="auth-container__content">
              {
                sideBarMenuItems.map((item) => (
                  <LinkPage logo={item.logo} divider={item.divider} onClick={closeDrawer} to={item.to} text={item.text} />
                ))
              }
            </div>
          </Drawer>
          <HeaderNav
            onOpen={() => {
              setVisible(true);
            }}
          />
          <Content>{props.children}</Content>
        </Layout>
      </Layout>
      {
        blockingNotificationsList?.length && blockingNotificationsList.map((item, idx) => (
          <Modal
            key={idx}
            title='Внимание'
            visible={!!item?.showBlockingPopup}
            className='notification-modal'
            onCancel={() => deleteBlockingPopup(idx)}
            onOk={() => deleteBlockingPopup(idx)}
            cancelButtonProps={{ style: { display: 'none' } }}
          >
            <div className='notification-modal__content'>
              <p>Кнопка нажата</p>
              <p>{moment(item?.time).format('DD.MM.YY HH:mm:ss')}</p>
              <p>{item?.message}</p>
              <div>
                <Link to={REPORTS} onClick={() => deleteBlockingPopup(idx)}>Отчет</Link>
              </div>
            </div>
          </Modal>
        ))
      }
    </>
  );
}

export default ContentLayout;
