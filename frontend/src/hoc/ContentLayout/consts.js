import {MAIN, SETTINGS_CATEGORIES, STATISTICS} from "../../consts/urls";

import { ReactComponent as DocumentLogo } from '../../assets/icons/document.svg';
import { ReactComponent as DashboardLogo } from '../../assets/icons/dashboard.svg';
import { ReactComponent as MapLocationLogo } from '../../assets/icons/map-location.svg';
import { ReactComponent as SettingsLogo } from '../../assets/icons/settings.svg';

export const sideBarMenuItems = [
  {
    to: MAIN,
    logo: <MapLocationLogo />,
    text: 'Карты',
    divider: false,
  },
  {
    to: STATISTICS,
    logo: <DashboardLogo/>,
    text: 'Дэшборды',
    divider: false,
  },
  {
    to: MAIN,
    logo: <DocumentLogo />,
    text: 'Отчеты',
    divider: true,
  },
  {
    to: SETTINGS_CATEGORIES,
    logo: <SettingsLogo/>,
    text: 'Конфигурация',
    divider: false,
  },
]
