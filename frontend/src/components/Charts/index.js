import React from 'react'
import Infrastructure from "./components/Infrastructure";
import EmployeesCount from "./components/EmployeesCount";
import Tags from "./components/Tags";
import TagsCharger from "./components/TagsCharger";
import SosCount from "./components/SosCount";
import SystemEvents from "./components/SystemEvents";
import DroppedEvents from "./components/DroppedEvents";
import ImmobilityEvents from "./components/ImmobilityEvents";
import AddDashboard from "./components/AddDashboard";

const Charts = () => {
  return (
    <div className="statistics-page">
      <Infrastructure />
      <Tags />
      <TagsCharger />
      <SystemEvents />
      <DroppedEvents />
      <ImmobilityEvents />
      <EmployeesCount />
      <SosCount />
      <AddDashboard />
    </div>
  )
}

export default Charts
