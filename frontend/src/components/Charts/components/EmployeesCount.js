import React from 'react'
import {Bar} from "react-chartjs-2";

const EmployeesCount = () => {
  const data = {
    labels: ['Сотрудники', 'Объекты', 'Ярлыки', 'Области', 'Карты'],
    datasets: [
      {
        label: 'Показать (убрать) все',
        data: [12, 19, 3, 5, 2],
        backgroundColor: [
          'rgba(73, 140, 202, 1)',
          'rgba(83, 124, 162, 1)',
          'rgba(119, 153, 185, 1)',
          'rgba(155, 182, 207, 1)',
          'rgba(191, 211, 230, 1)',
        ],
      },
    ],
  }

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
    }
  }

  return(
    <div className="char-component-wrapper">
      <div className="char-component-title">
        <span>Кол-во сотрудников по отметкам</span>
      </div>
      <div className="char-component">
        <Bar data={data} options={options} />
      </div>
    </div>
  )
}

export default EmployeesCount
