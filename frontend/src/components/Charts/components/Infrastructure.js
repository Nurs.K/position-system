import {Pie} from "react-chartjs-2";
import React from "react";

const Infrastructure = () => {
  const data = {
    labels: ['Полная', 'Средняя', 'Разряжена', 'Выкл'],
    datasets: [
      {
        label: '# of Votes',
        data: [12, 19, 3, 5],
        backgroundColor: [
          'rgba(94, 63, 190, 1)',
          'rgba(203, 182, 248, 1)',
          'rgba(229, 218, 251, 1)',
          '#F4F0FD'
        ],
        borderColor: [
          'rgba(94, 63, 190, 0.2)',
          'rgba(203, 182, 248, 0.2)',
          'rgba(229, 218, 251, 0.2)',
          '#F4F0FD'
        ],
        borderWidth: 1,
      },
    ],
  }
  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
    }
  }
  return (
    <div className="char-component-wrapper">
      <div className="char-component-title">
        <span>Инфраструктура</span>
      </div>
      <div className="char-component">
        <Pie data={data} options={options} />
      </div>
    </div>
  )
}

export default Infrastructure
