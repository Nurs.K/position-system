import React from 'react'
import {Pie} from "react-chartjs-2";

const TagsCharger = () => {
  const data = {
    labels: ['Полная', 'Средняя', 'Разряжена', 'Выкл', 'Нет данных', 'Ошибка'],
    datasets: [
      {
        label: '# of Votes',
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: [
          '#CA498C',
          '#FDE3DF',
          '#E6BFCE',
          '#CF9BBD',
          '#B977AC',
          '#A2539B'
        ],
        borderColor: [
          '#CA498C',
          '#FDE3DF',
          '#E6BFCE',
          '#CF9BBD',
          '#B977AC',
          '#A2539B'
        ],
        borderWidth: 1,
      },
    ],
  }

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
    }
  }

  return(
    <div className="char-component-wrapper">
      <div className="char-component-title">
        <span>Уровень заряда меток</span>
      </div>
      <div className="char-component">
        <Pie data={data} options={options} />
      </div>
    </div>
  )
}

export default TagsCharger
