import React from "react";

const SosCount = () => {

  const data = {
    label: 'Полная',
    count: 40
  }

  return (
    <div className="char-component-wrapper">
      <div className="char-component-title">
        <span>События SOS</span>
      </div>
      <div className="char-component">
        <div className="count-pie-label">
          <span className="count-color purple"> </span>
          <span>Полная</span>
        </div>
        <div className="char-wrapper">
          <div className="content">
            <div className="char-pie-count purple">
              <span className="char-pie-text">{data.count}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SosCount
