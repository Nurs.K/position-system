import React from 'react'

import plusIcon from '../../../assets/icons/plusIcon.svg'

const AddDashboard = () => {
  return (
    <div className="char-component-wrapper">
      <div className="char-component">
        <div className="add-dashboard-wrapper">
          <span>
            <img src={plusIcon} alt="plus icon" className="plus-icon"/>
          </span>
          <span>Добавить дешборд</span>
        </div>
      </div>
    </div>
  )
}

export default AddDashboard
