import React from "react";

const SystemEvents = () => {

  const data = {
    label: 'Полная',
    count: 200
  }

  return (
    <div className="char-component-wrapper">
      <div className="char-component-title">
        <span>События падения</span>
      </div>
      <div className="char-component">
        <div className="count-pie-label">
          <span className="count-color yellow"> </span>
          <span>Полная</span>
        </div>
        <div className="char-wrapper">
          <div className="content">
            <div className="char-pie-count yellow">
              <span className="char-pie-text">{data.count}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SystemEvents
