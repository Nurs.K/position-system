import React from 'react'
import {Pie} from "react-chartjs-2";

const Tags = () => {
  const data = {
    labels: ['Полная', 'Средняя', 'Разряжена'],
    datasets: [
      {
        label: '# of Votes',
        data: [12, 19, 3],
        backgroundColor: [
          'rgba(103, 197, 135, 1)',
          'rgba(201, 234, 212,  1)',
          '#EAF6ED'
        ],
        borderColor: [
          'rgba(103, 197, 135, 1)',
          'rgba(201, 234, 212,  1)',
          '#EAF6ED'
        ],
        borderWidth: 1,
      },
    ],
  }

  const options = {
    responsive: true,
    plugins: {
      legend: {
        position: 'top',
      },
    }
  }

  return (
    <div className="char-component-wrapper">
      <div className="char-component-title">
        <span>Метки</span>
      </div>
      <div className="char-component">
        <Pie data={data} options={options} />
      </div>
    </div>
  )
}

export default Tags
