import React from 'react';
import { Form as BaseForm, Input, Select, Switch } from 'antd';
import { CloseOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from 'react-redux';

import { setFormValue } from '../../store/actions/forms';
import { selectFormValues } from '../../store/selectors/forms';

import { errorMessage } from '../../helpers/errorHandlers';

import store from '../../store';

const Form = ({ name, loading, config, error }) => {
  const dispatch = useDispatch();
  const formValues = useSelector(selectFormValues(name));

  const handleSubmit = e => {
    e.preventDefault();
  };

  const handleFieldUpdate = (field, value) => {
    dispatch(setFormValue({
      form: name,
      field,
      value: value
    }));
  };

  const handleFileFieldChange = (field, type) => e => {
    const file = e.target.files[0];

    if (!file) return;

    if (type.startsWith('image')) {
      const fileReader = new FileReader();
      fileReader.readAsDataURL(file);
      fileReader.onload = event => {
        handleFieldUpdate(field, {
          file,
          url: event.target.result
        });
      };
    } else {
      handleFieldUpdate(field, {
        file,
        url: file.name,
      })
    }
  };

  const getValueSetLoading = item => {
    if (item.selectType === 'raw') return false;
    return item.getValueSetLoading(store.getState());
  };

  const getOptions = (item) => {
    if (item.selectType === 'raw') return item.options;
    return item.getValueSet(store.getState());
  };

  const { fields: errors } = errorMessage(error);

  return (
    <BaseForm
      onSubmit={handleSubmit}
      labelCol={{
        span: 6,
      }}
      wrapperCol={{
        span: 18,
      }}
    >
      {config.map((item, idx) => {
        const key = `FORM_${name}_${item.name}_FIELD_${idx}`;
        const value = formValues[item.name];

        if (item.type === 'file') {
          return (
            <BaseForm.Item
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 18 }}
              label={item.label}
              className="form-row"
              validateStatus={errors[item.name] ? 'error' : ''}
              help={errors[item.name]}
              key={key}
            >
              {
                (value && value.url)
                  ? (
                    (item.accept || '').startsWith('image')
                      ? (
                        <div className="image-field-preview">
                          <CloseOutlined
                            className="remove"
                            onClick={() => handleFieldUpdate(item.name, { file: null, url: null })}
                          />
                          <img src={value.url} alt={item.name} />
                        </div>
                      )
                      : (
                        <div className="audio-field-preview">
                          <p>Выбрано:</p>
                          <span className="link-chip">
                              <span className="link-el">{value.url}</span>
                              <CloseOutlined
                                className="close"
                                onClick={() => handleFieldUpdate(item.name, { file: null, url: null })}
                              />
                            </span>
                        </div>
                      )
                  )
                  : (
                    <Input
                      type="file"
                      shape="rounded"
                      onChange={handleFileFieldChange(item.name, item.accept)}
                      accept={item.accept}
                      disabled={loading}
                    />
                  )
              }
            </BaseForm.Item>
          );
        }

        if (item.type === 'boolean') {
          return (
            <BaseForm.Item
              key={key}
              labelCol={{ span: 6 }}
              wrapperCol={{ span: 18 }}
              label={item.label}
              className="form-row"
              validateStatus={errors[item.name] ? 'error' : ''}
              help={errors[item.name]}
            >
              <Switch
                checked={value}
                onChange={checked => handleFieldUpdate(item.name, checked)}
                loading={loading}
              />
            </BaseForm.Item>
          );
        }

        return (
          <BaseForm.Item
            label={item.label}
            rules={[{ required: Boolean(item.required), message: 'Введите текст' }]}
            className="form-row"
            key={key}
            initialValue={value}
            validateStatus={errors[item.name] ? "error" : ""}
            help={errors[item.name]}
          >
            {
              item.type === 'valueSet'
                ? (
                  <Select
                    shape="round"
                    backfill={false}
                    onChange={selected => handleFieldUpdate(item.name, selected)}
                    value={value}
                    loading={getValueSetLoading(item) || loading}
                  >
                    {
                      getOptions(item).map((option, index) => (
                        <Select.Option
                          key={`FORM_${name}_SELECT_OPTION_${index}`}
                          value={option.name}
                        >
                          {option.label}
                        </Select.Option>
                      ))
                    }
                  </Select>
                )
                : (
                  item.type === 'textarea'
                    ? (
                      <Input.TextArea
                        shape="round"
                        onChange={e => handleFieldUpdate(item.name, e.target.value)}
                        value={value}
                        disabled={loading}
                      />
                    )
                    : (
                      <Input
                        shape="round"
                        onChange={e => handleFieldUpdate(item.name, e.target.value)}
                        value={value}
                        disabled={loading}
                      />
                    )
                )
            }
          </BaseForm.Item>
        )
      })}
    </BaseForm>
  );
};

export default React.memo(Form);
