import React from 'react';
import { Modal } from 'antd';
import { useDispatch } from 'react-redux';

import { clearForm } from '../../store/actions/forms';

import Form from './form';

const Dialog = ({
  title,
  visible,
  onClose,
  buttonText,
  form,
  handleSubmit,
  loading,
  config,
  error,
}) => {
  const dispatch = useDispatch();

  const handleClose = () => {
    if (onClose) onClose();
    dispatch(clearForm({ form }));
  };

  return (
    <Modal
      title={title}
      visible={visible}
      onCancel={handleClose}
      okText={buttonText}
      cancelText="Отмена"
      onOk={handleSubmit}
      okButtonProps={{ loading }}
      keyboard={!loading}
      cancelButtonProps={{ loading }}
    >
      <Form
        name={form}
        config={config}
        error={error}
      />
    </Modal>
  );
};

export default React.memo(Dialog);
