import React from 'react';
import { Modal, Button } from 'antd';

function ConfirmModal({
  title,
  message,
  handleConfirm,
  handleCancel,
  visible,
}) {
  return (
    <Modal
      centered
      visible={visible}
      footer={null}
      onOk={handleConfirm}
      onCancel={handleCancel}
    >
      <p className="sos-modal-title">{title}</p>
      <p className="sos-modal-text">{message}</p>
      <div className="d-flex-sb">
        <Button className="sos-modal-send-btn" onClick={handleConfirm}>
          Да
        </Button>
        <Button className="sos-modal-cancel-btn" onClick={handleCancel}>
          Отмена
        </Button>
      </div>
    </Modal>
  );
}

export default ConfirmModal;
