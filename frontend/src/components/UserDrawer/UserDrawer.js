import React, { useState } from 'react';
import { Avatar, Drawer, Divider } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';

import { ReactComponent as PlanLogo } from '../../assets/icons/plane.svg';
import { ReactComponent as TemperatureLogo } from '../../assets/icons/temperature.svg';
import { ReactComponent as WarningLogo } from '../../assets/icons/warning-triangle.svg';
import { ReactComponent as BrowserSafariLogo } from '../../assets/icons/browser-safari.svg';
import { ReactComponent as DashboardLogo } from '../../assets/icons/dashboard.svg';
import { ReactComponent as CelsiusLogo } from '../../assets/icons/celsius.svg';
import { ReactComponent as SignalGraphLogo } from '../../assets/icons/signal-graph.svg';
import { ReactComponent as StepsLogo } from '../../assets/icons/steps.svg';
import { ReactComponent as BatteryLogo } from '../../assets/icons/battery.svg';

import IconsGroup from '../../pages/Main/IconsGroup';
import ModalComponent from '../../pages/Main/ModalComponent';
import { sendSos } from '../../services/sos';
import {
  errorNotification,
  successNotification,
} from '../../helpers/notificationHelpers';
import UserHistory from './UserHistory/index';
import { setCenterCoordinates } from '../../store/actions/crsMap';

function UserDrawer({ visible, onClose, keeper, setKeeperCoordinates }) {
  const sensors = keeper?._embedded?.tag?.sensors;

  const [sosVisible, setSosVisible] = useState(false);
  const [isHistoryVisible, setHistoryVisible] = useState(false);

  const elementsList = [
    {
      text: 'Движение',
      logo: <PlanLogo />,
      value: sensors?.motion,
    },
    {
      text: 'Температура',
      logo: <TemperatureLogo />,
      value: sensors?.temperature,
    },
    {
      text: 'Падение/удар',
      logo: <WarningLogo />,
      value: sensors?.impact_fall,
    },
    {
      text: 'Положение',
      logo: <BrowserSafariLogo />,
      value: sensors?.orientation,
    },
    {
      text: 'Барометр',
      logo: <DashboardLogo />,
      value: sensors?.barometer,
    },
    {
      text: 'Температура барометра',
      logo: <CelsiusLogo />,
      value: sensors?.barometer_temperature,
    },
    {
      text: 'Пульс (браслет)',
      logo: <SignalGraphLogo />,
      value: '~82',
    },
    {
      text: 'Шагомер (браслет)',
      logo: <StepsLogo />,
      value: '~1222',
    },
    {
      text: 'Батарея (браслет)',
      logo: <BatteryLogo />,
      value: sensors?.battery_state,
    },
  ];

  const handleClickSendSos = () => {
    sendSos(keeper.id)
      .then(() => {
        successNotification('Сигнал на метку отправлен!');
      })
      .catch((err) => {
        errorNotification('Сигнал не доставлен, повторите отправку!');
        throw err;
      });
  };

  const handleClickClock = () => {
    setHistoryVisible(true);
  };

  const handleClickAim = () => {
    const tag = keeper?._embedded?.tag;
    if (tag && tag.x && tag.y) {
      const latLng = { lat: tag.y, lng: tag.x };
      setKeeperCoordinates(latLng);
    }
  };

  const closeHistory = () => {
    setHistoryVisible(false);
  };

  return (
    <>
      <ModalComponent
        sosVisible={sosVisible}
        handleClickSendSos={handleClickSendSos}
        onCloseSos={() => {
          setSosVisible(false);
        }}
      />
      <Drawer
        placement="left"
        closable={true}
        onClose={() => {
          onClose();
        }}
        visible={visible}
        width={350}
      >
        <div
          style={{
            marginTop: 36,
          }}
        >
          {isHistoryVisible ? (
            <UserHistory closeHistory={closeHistory} keeper={keeper} />
          ) : (
            <>
              <div className="d-flex-center-center">
                <Avatar size="large" icon={<UserOutlined />} />
              </div>
              <p className="stack-drawer-title mt-20">{keeper?.name}</p>
              <p className="d-flex-sb">
                <span>Табельный номер:</span>
                <span className="green">~№45123345</span>
              </p>
              <p className="d-flex-sb">
                <span>Должность:</span>
                <span className="green">~Сварщик</span>
              </p>
              <p className="d-flex-sb">
                <span>Батарея:</span>
                <span className="green">{sensors?.battery_state}</span>
              </p>
              <Divider />
              <IconsGroup
                onShowSosModal={() => {
                  setSosVisible(true);
                }}
                onClickClock={handleClickClock}
                handleClickAim={handleClickAim}
              />
              <Divider />
              {elementsList.map((item, idx) => {
                return (
                  item.value && (
                    <div key={idx} className={'data-wrapper'}>
                      {item.logo}
                      <span className="data-text" style={{ color: '#373737' }}>
                        {item.text}
                      </span>
                      <span className="data-value green">{item.value}</span>
                    </div>
                  )
                );
              })}
            </>
          )}
        </div>
      </Drawer>
    </>
  );
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    setKeeperCoordinates: (coordinates) =>
      dispatch(setCenterCoordinates(coordinates)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDrawer);
