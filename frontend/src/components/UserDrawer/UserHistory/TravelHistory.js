import React, { useEffect, useRef, useState } from 'react';
import { Button, Slider } from 'antd';
import { PauseOutlined } from '@ant-design/icons';
import moment from 'moment';
import { connect } from 'react-redux';

import { PlayIcon, SquareSolidIcon, TrackIcon } from '../../../assets/icons';
import { findKeeperCoordinate } from './utils';
import { setKeeperCoordinate } from '../../../store/actions/ows';

let timeCounter;
const UPDATE_INTERVAL = 0.1;

const TravelHistory = ({
  startTime,
  endTime,
  owsData,
  setKeeperCoordinate,
}) => {
  const start = +startTime;
  const end = +endTime;
  const duration = end - start;

  const timeInterval = useRef();

  const [speed, setSpeed] = useState(1);
  const [progress, setProgress] = useState(0);
  const [isPlaying, setPlaying] = useState(false);
  const [playingTime, setPlayingTime] = useState(1);

  const handleChangeSlider = (progress) => {
    const current = progress * duration;
    const seekTime = start + current;
    timeCounter = seekTime;
    setPlayingTime(seekTime);
    setProgress(progress);
    setPlaying(false);
  };

  const handleClickSpeed = () => {
    setSpeed(speed === 32 ? 1 : speed * 2);
    setPlaying(false);
  };

  const togglePlay = () => {
    setPlaying(!isPlaying);
  };

  const handlePlaying = () => {
    timeCounter = +moment(timeCounter).add(speed * UPDATE_INTERVAL, 's');
    setPlayingTime(timeCounter);
  };

  useEffect(() => {
    if (owsData && playingTime) {
      const keeperCoordinate = findKeeperCoordinate(owsData, playingTime);
      setKeeperCoordinate(keeperCoordinate);
    }
  }, [playingTime, owsData]);

  useEffect(() => {
    timeCounter = start;
    setPlayingTime(timeCounter);
    setPlaying(false);
    setProgress(0);
  }, [start, end]);

  useEffect(() => {
    if (isPlaying) {
      timeInterval.current = setInterval(handlePlaying, 1000 * UPDATE_INTERVAL);
    } else {
      clearInterval(timeInterval.current);
    }
  }, [isPlaying]);

  useEffect(() => {
    const currentTime = playingTime - start;
    setProgress(currentTime / duration);
  }, [playingTime]);

  useEffect(() => {
    if (progress >= 1) {
      clearInterval(timeInterval.current);
    }
  }, [progress]);

  return (
    <div className="travel-history-wrapper">
      <p className="blue-title">История перемещений</p>
      <p className="history-sub-title">
        {moment(playingTime).format('YY.MM.DD HH:mm:ss')}
      </p>
      <Button className="speed-btn" onClick={handleClickSpeed}>
        X{speed}
      </Button>
      <Slider
        min={0}
        max={1}
        value={progress}
        step={0.01}
        onChange={handleChangeSlider}
      />
      <div className="controller-wrapper">
        <TrackIcon className="c-pointer" />
        {isPlaying ? (
          <PauseOutlined onClick={togglePlay} />
        ) : (
          <PlayIcon className="c-pointer" onClick={togglePlay} />
        )}
        <SquareSolidIcon className="c-pointer" />
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    owsData: state.ows.owsState.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setKeeperCoordinate: (keeperCoordinate) =>
      dispatch(setKeeperCoordinate(keeperCoordinate)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TravelHistory);
