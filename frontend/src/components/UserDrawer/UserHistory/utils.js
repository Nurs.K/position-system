import moment from 'moment';

export const getInitialStartTime = () => {
  const time = moment().set('hour', 0).set('minute', 0).set('second', 0);
  return time;
};

export const getInitialEndTime = () => {
  const time = getInitialStartTime().add(1, 'd');
  return time;
};

export const getUpdatedDate = (prevDate, newDate) => {
  const momentDate = moment(newDate);
  const year = momentDate.get('year');
  const month = momentDate.get('month');
  const date = momentDate.get('date');
  const updatedTime = moment(prevDate)
    .set('year', year)
    .set('month', month)
    .set('date', date);

  return updatedTime;
};

export const getUpdatedTime = (prevTime, newTime) => {
  const momentDate = moment(newTime);

  const hour = momentDate.get('hour');
  const minute = momentDate.get('minute');
  const second = momentDate.get('second');
  const updatedTime = moment(prevTime)
    .set('hour', hour)
    .set('minute', minute)
    .set('second', second);

  return updatedTime;
};

export const getHistoryParams = (stime, etime, keeperId) => {
  return {
    service: 'WFS',
    version: '1.1.0',
    request: 'GetFeature',
    typeName: 'rtls:mileage_points',
    outputFormat: 'text/javascript',
    viewparams: `keeperId:${keeperId};stime:${+stime};etime:${+etime};siteId:1;limit:3000`,
  };
};

export const findKeeperCoordinate = (owsData, searchTime) => {
  const keeperCoordinate = owsData.features.find(({ properties: { time } }) => {
    return time >= searchTime && searchTime >= time - 3000;
  });
  return keeperCoordinate;
};
