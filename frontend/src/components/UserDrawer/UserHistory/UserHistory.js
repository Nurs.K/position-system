import React, { useEffect, useState } from 'react';
import { Avatar, Divider, PageHeader } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';

import { ReactComponent as HistoryLogo } from '../../../assets/icons/history.svg';
import DateForm from './DateForm';
import TravelHistory from './TravelHistory';
import {
  getHistoryParams,
  getInitialEndTime,
  getInitialStartTime,
} from './utils';
import { owsRequest, resetOwsData } from '../../../store/actions/ows';

function UserHistory({ closeHistory, keeper, getHistory, resetOws }) {
  const [startTime, setStartTime] = useState(getInitialStartTime());
  const [endTime, setEndTime] = useState(getInitialEndTime());

  useEffect(() => {
    getHistory(getHistoryParams(startTime, endTime, keeper.id));
  }, [startTime, endTime]);

  useEffect(() => {
    return () => {
      resetOws();
    };
  }, []);

  return (
    <>
      <PageHeader
        className="site-page-header"
        onBack={closeHistory}
        title="Назад"
      />
      <div className="d-flex-center-center">
        <Avatar size="large" icon={<UserOutlined />} />
      </div>
      <p className="stack-drawer-title mt-20">{keeper?.name}</p>
      <p className="d-flex-sb">
        <span>Табельный номер:</span>
        <span className="green">№45123345</span>
      </p>
      <p className="d-flex-sb">
        <span>Должность:</span> <span className="green">Сварщик</span>
      </p>
      <Divider />
      <DateForm
        startTime={startTime}
        endTime={endTime}
        setStartTime={setStartTime}
        setEndTime={setEndTime}
      />
      <Divider />
      <TravelHistory startTime={startTime} endTime={endTime} />
      <Divider />
      <p className="blue-title">Отчеты</p>
      <div className={'data-wrapper'}>
        <HistoryLogo />
        <span className="data-text" style={{ color: '#373737' }}>
          Отчет о рабочем времени
        </span>
      </div>
      <div className={'data-wrapper'}>
        <HistoryLogo />
        <span className="data-text" style={{ color: '#373737' }}>
          Отчет по нахождению в области
        </span>
      </div>
    </>
  );
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    getHistory: (params) => dispatch(owsRequest(params)),
    resetOws: () => dispatch(resetOwsData()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserHistory);
