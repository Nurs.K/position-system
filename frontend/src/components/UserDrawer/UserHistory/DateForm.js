import React from 'react';
import { DatePicker, TimePicker, Form, Row, Col } from 'antd';

function DateForm({ startTime, endTime, setEndTime, setStartTime }) {
  const handleChangeStartDate = (value) => {
    setStartTime(value);
  };

  const handleChangeEndDate = (value) => {
    setEndTime(value);
  };

  const handleChangeStartTime = (value) => {
    setStartTime(value);
  };

  const handleChangeEndTime = (value) => {
    setEndTime(value);
  };

  return (
    <Form layout={'vertical'}>
      <Row gutter={8}>
        <Col span={12}>
          <Form.Item label="Дата с">
            <DatePicker
              onChange={handleChangeStartDate}
              style={{
                width: '100%',
              }}
              value={startTime}
            />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Время">
            <TimePicker
              onChange={handleChangeStartTime}
              style={{
                width: '100%',
              }}
              value={startTime}
            />
          </Form.Item>
        </Col>
      </Row>
      <Row gutter={8}>
        <Col span={12}>
          <Form.Item label="Дата по">
            <DatePicker
              onChange={handleChangeEndDate}
              style={{
                width: '100%',
              }}
              value={endTime}
            />
          </Form.Item>
        </Col>
        <Col span={12}>
          <Form.Item label="Время">
            <TimePicker
              onChange={handleChangeEndTime}
              style={{
                width: '100%',
              }}
              value={endTime}
            />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
}

export default DateForm;
