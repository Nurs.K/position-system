const submit = (formValues, instance) => {
  const formData = new FormData();

  if (formValues['category']) {
    if (instance && instance['category'] && instance['category']['id'] !== formValues['category']) {
      formData.append('category', formValues['category']);
    }
    if (!instance) {
      formData.append('category', formValues['category']);
    }
  }
  if (formValues['floor']) {
    if (instance && instance['floor'] !== formValues['floor']) {
      formData.append('floor', formValues['floor']);
    }
    if (!instance) {
      formData.append('floor', formValues['floor']);
    }
  }
  if (typeof formValues['is_main'] === 'boolean') {
    if (instance && instance['is_main'] !== formValues['is_main']) {
      formData.append('is_main', formValues['is_main'].toString());
    }
    if (!instance) {
      formData.append('is_main', formValues['is_main'].toString());
    }
  }
  if (formValues['max_x']) {
    if (instance && instance['max_x'] !== formValues['max_x']) {
      formData.append('max_x', formValues['max_x']);
    }
    if (!instance) {
      formData.append('max_x', formValues['max_x']);
    }
  }
  if (formValues['max_y']) {
    if (instance && instance['max_y'] !== formValues['max_y']) {
      formData.append('max_y', formValues['max_y']);
    }
    if (!instance) {
      formData.append('max_y', formValues['max_y']);
    }
  }
  if (formValues['min_x']) {
    if (instance && instance['min_x'] !== formValues['min_x']) {
      formData.append('min_x', formValues['min_x']);
    }
    if (!instance) {
      formData.append('min_x', formValues['min_x']);
    }
  }
  if (formValues['min_y']) {
    if (instance && instance['min_y'] !== formValues['min_y']) {
      formData.append('min_y', formValues['min_y']);
    }
    if (!instance) {
      formData.append('min_y', formValues['min_y']);
    }
  }
  if (formValues['name']) {
    if (instance && instance['name'] !== formValues['name']) {
      formData.append('name', formValues['name']);
    }
    if (!instance) {
      formData.append('name', formValues['name']);
    }
  }
  if (formValues['remote_id']) {
    if (instance && instance['remote_id'] !== formValues['remote_id']) {
      formData.append('remote_id', formValues['remote_id']);
    }
    if (!instance) {
      formData.append('name', formValues['name']);
    }
  }
  if (formValues['z']) {
    if (instance && instance['z'] !== formValues['z']) {
      formData.append('z', formValues['z']);
    }
    if (!instance) {
      formData.append('z', formValues['z']);
    }
  }
  if (formValues['y']) {
    if (instance && instance['y'] !== formValues['y']) {
      formData.append('y', formValues['y']);
    }
    if (!instance) {
      formData.append('y', formValues['y']);
    }
  }
  if (formValues['x']) {
    if (instance && instance['x'] !== formValues['x']) {
      formData.append('x', formValues['x']);
    }
    if (!instance) {
      formData.append('x', formValues['x']);
    }
  }
  if (formValues['segment_length']) {
    if (instance && instance['segment_length'] !== formValues['segment_length']) {
      formData.append('segment_length', formValues['segment_length']);
    }
    if (!instance) {
      formData.append('segment_length', formValues['segment_length']);
    }
  }
  return formData;
}

export default submit
