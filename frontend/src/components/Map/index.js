import React, {useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useParams} from 'react-router-dom';

import submit from './submit';
import SideBar from './components/SideBar';
import CRSMap from './components/CRSMap/index';

import {getFinalBounds} from '../../helpers/getters';

import './index.scss';
import {categoriesRequest} from '../../store/actions/categories';
import {makeSelectGetSingleMap} from '../../store/selectors/svgMap';
import {uploadMapRequest, editMapRequest, setGetSingleMapData} from '../../store/actions/svgMap';
import {buildFormData} from "../../helpers/formData";

const UploadMap = () => {
  const submitRef = useRef(null);
  const {mapId} = useParams();
  const dispatch = useDispatch();

  const [imageFile, setImageFile] = useState();
  const [coordinatesFromValues, setCoordinatesFormValues] = useState({});
  const [pointA, setPointA] = useState({lat: 0, lng: 0});
  const [pointL0, setPointL0] = useState();
  const [pointL1, setPointL1] = useState();
  const [bounds, setBounds] = useState();

  const instance = useSelector(makeSelectGetSingleMap)
  const singleMap = useSelector(makeSelectGetSingleMap)

  const onSubmitName = (values) => {
    onSubmit({...values, ...coordinatesFromValues});
  };

  const uploadMap = (formData) => {
    dispatch(uploadMapRequest(formData))
  }

  const getCategoriesList = () => {
    dispatch(categoriesRequest())
  }

  const onSubmitCoordinates = (values) => {
    setCoordinatesFormValues(values);
    submitRef.current.click();
  };

  // console.log(pointL1, pointL1, pointA)

  const onSubmit = (values) => {
    const {x, y, segment_length, ...rest} = values;
    const [, {lat: height, lng: width}] = bounds;
    const finalBounds = getFinalBounds({
      segment_length,
      x,
      y,
      pointA,
      pointL0,
      pointL1,
      height,
      width,
    });

    const result = {
      ...finalBounds,
      ...rest,
      // pointA,
      // pointL0,
      // pointL1,
      file: imageFile
    };

    const formData = new FormData();

    if (pointA && pointL0 && pointL1){
      formData.append('pointA', JSON.stringify(pointA));
      formData.append('pointL1', JSON.stringify(pointL1));
      formData.append('pointL0', JSON.stringify(pointL0));
    }

    Object.entries(result).forEach(([key, value]) => {
      console.log( key, '===> ', value)
      buildFormData(formData, value, key);
    });

    if (!!mapId) {
      const data = submit(result, instance)

      for (const key of data.keys()) {
        console.log(key, ' => ', data.get(key));
      }
      dispatch(editMapRequest({formData: data, id: mapId}))
    } else uploadMap({formData})
  };

  useEffect(() => {
    getCategoriesList();
  }, []); // eslint-disable-line

  useEffect(() => {
    return () => {
      dispatch(setGetSingleMapData(null))
    }
  }, []);

  return (
    <div className="content">
      <SideBar
        imageFile={imageFile}
        setImageFile={setImageFile}
        submitRef={submitRef}
        onSubmit={onSubmitName}
        onSubmitCoordinates={onSubmitCoordinates}
      />
      <div className="content-page">
        {imageFile || !!singleMap ? (
          <CRSMap
            image={imageFile ? URL.createObjectURL(imageFile) : singleMap.file}
            pointA={pointA}
            pointL0={pointL0}
            pointL1={pointL1}
            bounds={bounds}
            setPointA={setPointA}
            setPointL0={setPointL0}
            setPointL1={setPointL1}
            setBounds={setBounds}
          />
        ) : (
          <h4>
            Select image from the previews list for coordinate system
            configuration
          </h4>
        )}
      </div>
    </div>
  );
};

export default UploadMap;
