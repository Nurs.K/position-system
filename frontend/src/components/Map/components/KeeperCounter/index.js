import React from 'react';
import { Tooltip } from 'antd';

import { TOTAL_AMOUNT_OBJECTS } from '../../../../consts/tooltip';

import './index.scss';

const KeeperCounter = ({ amount }) => {
  return (
    <Tooltip title={TOTAL_AMOUNT_OBJECTS}>
      <div className="counter-wrapper">{amount}</div>
    </Tooltip>
  );
};

export default KeeperCounter;
