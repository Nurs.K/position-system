import React, {useEffect, useState} from 'react';
import { useParams } from 'react-router-dom';
import {useSelector} from "react-redux";
import {Button, Form, Input, Select, Switch } from 'antd';
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";

import { floorOptions } from '../../../../consts/options';
import {
  makeSelectGetMainMap,
  makeSelectGetMainMapFetching
} from '../../../../store/selectors/svgMap'
import {makeSelectCategoryList} from "../../../../store/selectors/categories";
import './index.scss';

const CoordinatesForm = ({ onSubmit, singleMap, clickImageInput, imageFile, submitBtnRef}) => {
  const { mapId } = useParams();
  const isMain = useSelector(makeSelectGetMainMap)
  const isMainLoading = useSelector(makeSelectGetMainMapFetching)
  const categories = useSelector(makeSelectCategoryList)

  const [isMainChecked, setIsMainChecked] = useState(mapId === isMain?.id);

  const handleChangeMain = () => {
    setIsMainChecked(prevState => !prevState)
  }

  useEffect(() => {
    setIsMainChecked(mapId === isMain?.id)
  }, [mapId, isMain])

  const submitHandler = (values) => {
    onSubmit({...values, is_main: isMainChecked})
  }

  return (
    <div className="form-wrapper">
      <Form
        onFinish={submitHandler}
        layout="vertical"
      >
        <Form.Item
          label="Upload"
        >
          <Button style={{margin: 0}} disabled={!!imageFile} onClick={clickImageInput}>click to Upload</Button>
        </Form.Item>
        <Form.Item
          name="remote_id"
          label="RTLS map id"
          initialValue={singleMap?.remote_id || ""}
          rules={[{ required: false, message: 'Введите RTLS id карты' }]}
        >
          <Input className="input-max-width" shape="round" placeholder="RTLS map id" />
        </Form.Item>
        <Form.Item
          name="name"
          label="Имя"
          initialValue={singleMap?.name || ""}
          rules={[{ required: false, message: 'Введите название карты' }]}
        >
          <Input className="input-max-width" shape="round" placeholder="Имя" />
        </Form.Item>
        <Form.Item
          name="x"
          label="X"
          initialValue={singleMap?.x || 0}
          rules={[{ required: false, message: 'Введите x' }]}
        >
          <Input shape="round" type="number" />
        </Form.Item>
        <Form.Item
          name="y"
          label="Y"
          initialValue={singleMap?.y || 0}
          rules={[{ required: false, message: 'Введите y' }]}
        >
          <Input shape="round" type="number" />
        </Form.Item>
        <Form.Item
          name="min_z"
          label="Min Z"
          initialValue={singleMap?.min_z || 0}
          rules={[{ required: false, message: 'Введите min z' }]}
        >
          <Input shape="round" type="number" />
        </Form.Item>
        <Form.Item
          name="max_z"
          label="Max Z"
          initialValue={singleMap?.max_z || 0}
          // rules={[{ required: true, message: 'Введите max z' }]}
        >
          <Input shape="round" type="number" />
        </Form.Item>
        <Form.Item
          name="segment_length"
          label="Segment length"
          initialValue={singleMap?.segment_length || 0}
          rules={[{ required: true, message: 'Введите segment length' }]}
        >
          <Input shape="round" type="number" />
        </Form.Item>

        <Form.Item
          label="Коэффициент (readonly)"
          name='ratio'
          initialValue={singleMap?.ratio || 0}
        >
          <Input shape="round" readOnly type="number" />
        </Form.Item>

        <Form.Item
          label="min X (readonly)"
          name='min_x'
          initialValue={singleMap?.min_x || 0}
        >
          <Input shape="round" readOnly type="number" />
        </Form.Item>
        <Form.Item
          label="max X (readonly)"
          name='max_x'
          initialValue={singleMap?.max_x || 0}
        >
          <Input shape="round" readOnly type="number" />
        </Form.Item>
        <Form.Item
          label="min Y (readonly)"
          name='min_y'
          initialValue={singleMap?.min_y || 0}
        >
          <Input shape="round" readOnly type="number" />
        </Form.Item>
        <Form.Item
          label="max Y (readonly)"
          name='max_y'
          initialValue={singleMap?.max_y || 0}
        >
          <Input shape="round" readOnly type="number" />
        </Form.Item>
        { !isMainLoading &&
          <Form.Item
            label="Главный"
            name="is_main"
            initialValue={isMainChecked}
            shouldUpdate
            className="form-item-horizontal"
          >
            <Switch
              defaultChecked={mapId === isMain?.id}
              checked={isMainChecked}
              onChange={handleChangeMain}
              checkedChildren={<CheckOutlined />}
              unCheckedChildren={<CloseOutlined />}
              // disabled={mapId === isMain?.id || !isMain}
              disabled={!isMain || mapId !== isMain?.id}
            />
          </Form.Item>
        }
        {/* eslint-disable-next-line no-mixed-operators */}
        <Form.Item label="Floor" name="floor" initialValue={singleMap?.floor || floorOptions && floorOptions[0] && floorOptions[0].id}>
          <Select>
            {floorOptions?.map(({ id, name }) => {
              return (
                <Select.Option key={id} value={id}>
                  {name}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item
          label="Category"
          name="category"
          initialValue={singleMap?.category?.id}
        >
          <Select>
            {categories?.map(({ id, name }) => {
              return (
                <Select.Option key={id} value={id}>
                  {name}
                </Select.Option>
              );
            })}
          </Select>
        </Form.Item>

        <Form.Item hidden={true}>
          <Button hidden={true} ref={submitBtnRef} htmlType="submit">{!!mapId ? 'Изменить'  : 'Создать'}</Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default CoordinatesForm;
