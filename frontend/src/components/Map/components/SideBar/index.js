import React, {useRef} from 'react';
import { useSelector } from "react-redux";
import { Form } from 'antd';
import {useParams} from "react-router-dom";

import { makeSelectGetSingleMap } from '../../../../store/selectors/svgMap';
import CoordinatesForm from "../CoordinatesForm";

const SideBar = ({ setImageFile, submitRef, onSubmit, onSubmitCoordinates, imageFile }) => {
  const imageInput = useRef(null);
  const submitBtnRef = useRef(null)

  const { mapId } = useParams();

  const singleMap = useSelector(makeSelectGetSingleMap)

  const clickImageInput = () => {
    imageInput.current.click();
  };

  const handleSubmit = () => {
    submitBtnRef.current.click();
  };

  const handleImageLoad = (e) => {
    const [file] = e.target.files;
    if (file) setImageFile(file);
  };

  return (
    <div className="sidebar">
      <Form hidden={true} onFinish={onSubmit}>
        <input
          ref={submitRef}
          type="submit"
          value="submit"
          className="d-none"
        />
      </Form>

      <input
        ref={imageInput}
        className="d-none"
        type="file"
        name="image"
        onChange={handleImageLoad}
      />
      <CoordinatesForm
        onSubmit={onSubmitCoordinates}
        singleMap={singleMap}
        clickImageInput={clickImageInput}
        imageFile={imageFile}
        submitBtnRef={submitBtnRef}
      />
      <button className="btn" onClick={handleSubmit}>
        {!!mapId ? 'Изменить'  : 'Создать'}
      </button>
    </div>
  );
};

export default SideBar;
