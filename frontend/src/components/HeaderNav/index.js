import React from 'react';
import {Layout, PageHeader, Space, Tooltip} from 'antd';
import {MenuOutlined} from '@ant-design/icons';

import BellPopover from './BellPopover/BellPopover';
import SettingsPopover from './SettingsPopover';
import SearchPopover from './SearchPopover/SearchPopover';
import FavoriteMaps from './FavoriteMaps'

import { MENU } from '../../consts/tooltip';

import './index.css';
import MapTypes from "../../components/MapButtons/MapTypes";

const { Header } = Layout;

function HeaderNav ({ onOpen }) {

  return (
    <>
      <Header>
        <PageHeader
          className="site-page-header"
          title={
            <div style={{display: 'flex'}}>
              <Tooltip title={MENU}>
                <div
                  className="header-menu-wrapper"
                  onClick={() => {onOpen()}}
                >
                  <MenuOutlined
                    style={{
                      color: '#fff',
                    }}
                  />
                </div>
              </Tooltip>
              <MapTypes className='header-types'/>
            </div>
          }
          extra={[
            // <MapTypes />,
            <div key="1">
              <Space size={48}>
                <SearchPopover />
                <FavoriteMaps />
                <BellPopover />
                <SettingsPopover />
              </Space>
            </div>,
          ]}
        />
      </Header>
    </>
  );
}

export default HeaderNav;
