import React from 'react';
import {useSelector} from 'react-redux';

import { ReactComponent as UserGreenLogo } from '../../../assets/icons/user-green.svg';
import { ReactComponent as GoalLogo } from '../../../assets/icons/goal.svg';
import { ReactComponent as SosLogo } from '../../../assets/icons/sos.svg';

import '../index.css';
import { EMPLOYEES } from '../../../consts/options';
import { sendSos } from '../../../services/sos';
import {
  errorNotification,
  successNotification,
} from '../../../helpers/notificationHelpers';
import {makeSelectSearchData} from "../../../store/selectors/search";

function SearchResult({ objectType }) {
  const handleClickSos = (id) => {
    sendSos(id)
      .then(() => {
        successNotification('Сигнал на метку отправлен!');
      })
      .catch((err) => {
        errorNotification('Сигнал не доставлен, повторите отправку!');
        throw err;
      });
  };

  const dataList = useSelector(makeSelectSearchData)

  return (
    <>
      {!!dataList?.length ? dataList.map((item, idx) => {
        return (
          <div
            key={idx}
            className='search-result__content'
          >
            <div className="user-logo">
              <UserGreenLogo
                style={{
                  flex: '1 0 0%',
                }}
              />
              <p
                style={{
                  padding: 0,
                  margin: 0,
                  fontSize: 14,
                  lightHeight: '16px',
                  flex: '1 0 55%',
                }}
              >
                {item.name} {item.lastname} {item.login}
              </p>
            </div>
            <div className="goal-sos-logo">
              <GoalLogo />
              {objectType === EMPLOYEES && (
                <SosLogo onClick={() => handleClickSos(item.id)} />
              )}
            </div>
          </div>
        );
      }) : <p className="empty-text">Список пуст</p>}
    </>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     dataList: state.search.searchState.data?.items || [],
//   };
// };

export default SearchResult;
