import React, { useState } from 'react';
import { Popover, Tooltip } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

import { SEARCH } from '../../../consts/tooltip';
import SearchPopoverContent from './SearchPopoverContent';

import '../index.css';

function SearchPopover() {
  const [visible, setVisible] = useState(false);

  return (
    <Popover
      placement="bottomRight"
      content={<SearchPopoverContent setVisible={setVisible} />}
      trigger="click"
      overlayClassName='search-header__popover notifications-header__popover'
      visible={visible}
      onVisibleChange={() => {
        setVisible(!visible);
      }}
    >
      <Tooltip title={SEARCH}>
        <SearchOutlined
          style={{
            color: '#fff',
            fontSize: 28,
            cursor: 'pointer',
          }}
          />
      </Tooltip>
    </Popover>
  );
}

export default SearchPopover;
