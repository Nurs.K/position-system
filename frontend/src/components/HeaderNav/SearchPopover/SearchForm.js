import React, { useEffect } from 'react';
import { Input, Select, Form } from 'antd';
import { connect } from 'react-redux';
import _ from 'lodash';

import { searchRequest } from '../../../store/actions/search';
import { objectTypeOptions } from '../../../consts/options';
import { getFilter, getSort } from '../../../helpers/getters';

import '../index.css';

const { Option } = Select;

function SearchForm({ search, objectType, setObjectType }) {
  const [form] = Form.useForm();

  const sendSearchRequest = ({ type = objectType, searchValue = '' }) => {
    const params = {
      filter: getFilter(type, searchValue),
      sort: getSort(type),
    };

    search({
      slug: type,
      params,
    });
  };

  const handleSelect = (value) => {
    form.resetFields();
    setObjectType(value);
    sendSearchRequest({ type: value });
  };

  const handleSearchChange = _.debounce((e) => {
    const searchValue = e.target.value;
    sendSearchRequest({ searchValue });
  }, 200);

  useEffect(() => {
    sendSearchRequest({});
  }, []);     // eslint-disable-line

  return (
    <div className="search-popover-wrapper">
      <div className="search-popover-item-left">
        <p>Тип объекта</p>
        <Select
          defaultValue={objectType}
          style={{ width: 120 }}
          onChange={handleSelect}
        >
          {objectTypeOptions.map(({ id, name }) => {
            return (
              <Option key={id} value={id}>
                {name}
              </Option>
            );
          })}
        </Select>
      </div>
      <div className="search-popover-item-right">
        <p>Номер метки</p>
        <Form form={form}>
          <Form.Item name="search">
            <Input
              placeholder="Введите для поиска"
              onChange={handleSearchChange}
            />
          </Form.Item>
        </Form>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    search: (arg) => {
      dispatch(searchRequest(arg));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);
