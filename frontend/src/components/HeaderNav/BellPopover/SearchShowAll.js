import React from "react";
import {Input} from "antd";
import {ReactComponent as SettingAltLogo} from "../../../assets/icons/settings-alt.svg";
import {ReactComponent as TrashLogo} from "../../../assets/icons/trash.svg";
const { Search } = Input;

const SearchShowAll = ({setShowSettings}) => {
  return(
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
      }}
    >
      <Search style={{ width: '80%' }} />
      <SettingAltLogo
        style={{ cursor: 'pointer' }}
        onClick={() => {
          setShowSettings(true);
        }}
      />
      <TrashLogo style={{ cursor: 'pointer' }} />
    </div>
  )
}

export default SearchShowAll
