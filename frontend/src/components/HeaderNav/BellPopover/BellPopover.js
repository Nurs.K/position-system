import React, { useState } from 'react';
import {useSelector} from 'react-redux';
import {
  Popover,
  Badge,
  Tooltip,
} from 'antd';
import { BellFilled } from '@ant-design/icons';

import { makeSelectGetNotifications } from '../../../store/selectors/notifications';
import { selectConfigList } from '../../../store/selectors/notificationConfigSelectors';

import { NOTIFICATIONS } from '../../../consts/tooltip';

import '../index.css';
import BellPopoverContent from "./BellPopoverContent";


const BellPopover = () => {

  const [showAll, setShowAll]= useState(false)
  const [showSettings, setShowSettings] = useState(false);

  const notificationsList = useSelector(makeSelectGetNotifications)
  const configList = useSelector(selectConfigList)

  const getNotificationIcon = (type) => {
    let icon = null
    configList?.forEach(item => {
      if(item?.event_type === type ) icon = item?.thumbnail
    })
    return icon
  }

  return (
    <Popover
      placement="bottomRight"
      overlayClassName="notifications-header__popover"
      content={
        <BellPopoverContent
          getNotificationIcon={getNotificationIcon}
          setShowSettings={setShowSettings}
          showSettings={showSettings}
          showAll={showAll}
        />
      }
      trigger="click"
    >
      <Badge size="small" count={notificationsList?.length}>
        <Tooltip title={NOTIFICATIONS}>
          <BellFilled
            style={{
              color: '#FFB800',
              fontSize: 28,
              cursor: 'pointer',
            }}
          />
        </Tooltip>
      </Badge>
    </Popover>
  );
}

export default BellPopover;
