import React from "react";
import {Avatar, Card, Checkbox, Input, List} from "antd";
import {CloseOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";

import {ReactComponent as ParkingLogo} from "../../../assets/icons/parking.svg";

import {makeSelectGetNotifications} from "../../../store/selectors/notifications";
import {deleteNotification} from "../../../store/actions/notifications";

import {notificationSettingsData} from "../consts";
import SearchShowAll from "./SearchShowAll";


const BellPopoverContent = (
  {
    showAll,
    showSettings,
    setShowSettings,
    getNotificationIcon
  }
) => {
  const notificationsList = useSelector(makeSelectGetNotifications)

  const dispatch = useDispatch()

  const closeNotification = (idx) => {
    dispatch(deleteNotification(idx))
  }

  return(
    <div className="bell-content-wrapper">
      {showSettings === false ? (
        <>
          {showAll === true && (
            <SearchShowAll setShowSettings={setShowSettings} />
          )}
          <List
            itemLayout="vertical"
            dataSource={notificationsList || []}
            renderItem={(item, idx) => (
              <List.Item key={idx}>
                <Card
                  style={{
                    width: 400,
                    borderRadius: 10,
                    boxShadow: '0px 5px 10px rgba(0, 0, 0, 0.1)',
                  }}
                  title={
                    <div className="card-title" onClick={() => console.log('log', getNotificationIcon(item.type))}>
                      <Avatar src={getNotificationIcon(item.type)} />
                      {item.time}
                      <CloseOutlined
                        onClick={() => closeNotification(idx)}
                        style={{
                          marginLeft: 'auto',
                          color: '#25ACE3',
                          cursor: 'pointer',
                        }}
                      />
                    </div>
                  }
                >
                  {item.message}
                </Card>
              </List.Item>
            )}
          />
        </>
      ) : (
        <div style={{ width: 250, padding: 24, position: 'relative' }}>
          <p
            style={{
              fontSize: 16,
              lineHeight: '19px',
              textAlign: 'center',
              color: '#000000',
              paddingBottom: 20,
            }}
          >
            Настройки уведомлений
          </p>
          <CloseOutlined
            style={{
              position: 'absolute',
              top: 5,
              right: 5,
            }}
            onClick={() => {
              setShowSettings(false);
            }}
          />

          {notificationSettingsData.map((item, idx) => {
            return (
              <div
                key={idx}
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  margin: '12px 0',
                }}
              >
                <ParkingLogo />
                <p
                  style={{
                    padding: 0,
                    margin: 0,
                    fontSize: 14,
                    lightHeight: '16px',
                  }}
                >
                  {item.text}
                </p>
                <Checkbox />
              </div>
            );
          })}
        </div>
      )}
    </div>
  )
}

export default BellPopoverContent
