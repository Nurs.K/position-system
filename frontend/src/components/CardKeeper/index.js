import React from 'react';
import { Card, Avatar } from 'antd';

const { Meta } = Card;

function CardKeeper({ keeper, onClickCard }) {
  return (
    <Card
      style={{ width: 300, cursor: 'pointer' }}
      bordered={false}
      bodyStyle={{ padding: 8 }}
      onClick={onClickCard}
    >
      <Meta
        avatar={
          <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
        }
        title={
          <span
            style={{
              color: '#00A2E8',
              fontWeight: 900,
              fontSize: 16,
            }}
          >
            {keeper.name}
          </span>
        }
        description={
          <>
            <p
              className="d-flex-sb"
              style={{
                padding: 0,
                margin: 0,
                fontSize: 14,
                lineHeight: '16px',
                color: '#000000',
              }}
            >
              Табельный номер: <span className="green">№45123345</span>
            </p>
            <p
              className="d-flex-sb"
              style={{
                padding: 0,
                margin: 0,
                fontSize: 14,
                lineHeight: '16px',
                color: '#000000',
              }}
            >
              Должность: <span className="green">Сварщик</span>
            </p>
          </>
        }
      />
    </Card>
  );
}

export default CardKeeper;
