import React from 'react';
import { Pagination as BasePagination } from 'antd';

const ruLocale = require('rc-pagination/lib/locale/ru_RU');

const Pagination = ({
  page,
  count,
  size = 20,
  pageSizeOptions = [
    10,
    20,
    30,
    40,
  ],
  onChange,
  onSizeChange,
}) => {

  return (
    <BasePagination
      defaultCurrent={page}
      total={count}
      current={page}
      pageSizeOptions={pageSizeOptions}
      pageSize={size}
      onChange={onChange}
      locale={ruLocale}
      onShowSizeChange={onSizeChange}
      showSizeChanger={true}
    />
  );
};


export default React.memo(Pagination);
