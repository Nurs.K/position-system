import React, {useCallback} from 'react';
import {Affix, Button, Tooltip} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {useHistory, useParams} from "react-router-dom";

import SosPopover from './components/SosPopover/SosPopover';

import {ReactComponent as BookMarkLogo} from '../../assets/icons/bookmark.svg';
import {ReactComponent as CameraLogo} from '../../assets/icons/camera.svg';
import {ReactComponent as StackLogo} from "../../assets/icons/stack.svg";
import {DownArrow} from "../../assets/icons/downArrow";
import {UpArrow} from "../../assets/icons/upArrow";
import {ReactComponent as ZoomIn} from "../../assets/icons/zoomIn.svg";
import {ReactComponent as ZoomOut} from "../../assets/icons/zoomOut.svg";

import {
  BOOKMARKS,
  CAMERA,
  DOWN,
  MAP_OPTIONS,
  UP,
  ZOOM_IN,
  ZOOM_OUT,
} from '../../consts/tooltip';
import {SHOW_MAP} from "../../consts/urls";

import {makeSelectGetSingleMap} from "../../store/selectors/svgMap";
import {mapNextFloor} from "../../store/actions/svgMap";

import {commonButtonStyles, styleFloorButtons} from "./styles";
import {floorModes} from "./consts";


const Buttons = ({showBook, showMapSettings, showFloors, zoomIn, zoomOut, disableNextFloor,disablePrevFloor}) => {
  const singleMapData = useSelector(makeSelectGetSingleMap);

  const dispatch = useDispatch()
  const {mapId} = useParams()
  const history = useHistory()

  const nextFloorCallback = useCallback((id) => {
    history.push(`${SHOW_MAP}${id}`)
  }, [mapId])

  const handleNextFloor = (mode) => {
    dispatch(mapNextFloor({id: mapId, nextFloorCallback, mode}))
  }

  return (
    <>
      <SosPopover/>
      <div className="buttons-position">
        <Affix >
          <Tooltip title={BOOKMARKS}>
            <Button
              style={{
                filter: 'drop-shadow(0px 20px 30px rgba(0, 0, 0, 0.15))',
                ...commonButtonStyles,
              }}
              shape="circle"
              onClick={() => {
                showBook();
              }}
            >
              <BookMarkLogo/>
            </Button>
          </Tooltip>
        </Affix>
      {
        showFloors && (
          <Affix style={styleFloorButtons}>
            <Tooltip
              title={UP}
              style={{
                ...commonButtonStyles,
                borderTopRightRadius: '1000px',
                borderTopLeftRadius: '1000px',
                borderBottomLeftRadius: '0',
                borderBottomRightRadius: '0',
              }}
             >
              <Button
                onClick={() => handleNextFloor(floorModes.up)}
                disabled={disableNextFloor}
              >
                <UpArrow/>
              </Button>
            </Tooltip>
            <div className="buttons-divider"> </div>
            <div
              style={{
                ...commonButtonStyles,
                color: '#373737',
                borderRadius: '0',
                lineHeight: 16,
                fontWeight: 'normal',
              }}
            >
              {singleMapData?.floor}
            </div>
            <div className="buttons-divider"> </div>
            <Tooltip
              title={DOWN}
              style={{
                ...commonButtonStyles,
                borderBottomLeftRadius: '1000px',
                borderBottomRightRadius: '1000px',
                borderTopRightRadius: '0',
                borderTopLeftRadius: '0',
              }}
              >
              <Button
                onClick={() => handleNextFloor(floorModes.down)}
                disabled={disablePrevFloor}
              >
                <DownArrow/>
              </Button>
            </Tooltip>
          </Affix>
        )
      }
      <Affix>
        <Tooltip title={ZOOM_IN}>
          <Button
            onClick={zoomIn}
            style={{
            ...commonButtonStyles,
            borderTopRightRadius: '1000px',
            borderTopLeftRadius: '1000px',
            borderBottomLeftRadius: '0',
            borderBottomRightRadius: '0',
            }}
          >
            <ZoomIn />
          </Button>
        </Tooltip>
        <div className="buttons-divider"> </div>
        <Tooltip title={ZOOM_OUT}>
          <Button
            onClick={zoomOut}
            style={{
            ...commonButtonStyles,
            borderTopRightRadius: '0',
            borderTopLeftRadius: '0',
            borderBottomLeftRadius: '1000px',
            borderBottomRightRadius: '1000px',
          }}>
            <ZoomOut />
          </Button>
        </Tooltip>
      </Affix>
      <Affix>
        <Tooltip title={CAMERA}>
          <Button
            style={{
              filter: 'drop-shadow(0px 20px 30px rgba(0, 0, 0, 0.15))',
              ...commonButtonStyles,
              marginBottom: '40px'
            }}
            shape="circle"
            onClick={() => {
              console.log(1);
            }}
          >
            <CameraLogo/>
          </Button>
        </Tooltip>
        <Tooltip title={MAP_OPTIONS}>
          <Button
            style={{
              filter: 'drop-shadow(0px 20px 30px rgba(0, 0, 0, 0.15))',
              ...commonButtonStyles,
            }}
            shape="circle"
            onClick={showMapSettings}
          >
            <StackLogo/>
          </Button>
        </Tooltip>
      </Affix>
      </div>
    </>
  );
}

export default Buttons;
