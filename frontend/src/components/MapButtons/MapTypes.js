import React from 'react';
import { NavLink } from "react-router-dom";
import { connect, useSelector } from 'react-redux';
import { Tooltip } from 'antd';

import { makeSelectGetMainMap } from '../../store/selectors/svgMap';
import { toShowMapId } from '../../consts/routes';
import { SANDWICH } from '../../consts/urls';
import { ReactComponent as MapLogo } from '../../assets/icons/map.svg';
import { ReactComponent as BuildingLogo } from '../../assets/icons/building.svg';
import { ReactComponent as PlanLogo } from '../../assets/icons/plan.svg';

import '../../pages/Main/index.css';

function MapTypes({ changeMapTypes, className, history }) {

  const isMain = useSelector(makeSelectGetMainMap)

  const logos = [
    {
      id: 1,
      to: '/',
      icon: <MapLogo />,
      mapTypes: 'MAP',
      title: 'Home',
      show: true
    },
    {
      id: 2,
      icon: <BuildingLogo />,
      to: SANDWICH,
      mapTypes: 'BUILDING',
      title: 'Sandwich',
      show: true
    },
    {
      id: 3,
      to: toShowMapId(isMain?.id),
      icon: <PlanLogo />,
      mapTypes: 'PLAN',
      title: 'Main',
      show: !!isMain
    },
  ];

  return (
    <div className={`logos-wrapper ${className}`}>
      {logos.map((item) => {
        return (
          <div key={item.id}>
            { item.show &&
              <Tooltip title={item.title}>
                <NavLink to={item.to} exact className='logos-wrapper__link' activeClassName='logos-wrapper__link-active'>
                  <span>{item.icon}</span>
                </NavLink>
              </Tooltip>
            }
          </div>
        );
      })}
    </div>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    changeMapTypes: (type) => dispatch({ type }),
  };
}

export default connect(null, mapDispatchToProps)(MapTypes);
