import React  from 'react';

import Buttons from './Buttons';

import './index.scss';

const recentZone = [
  {title: 'Корпус ЗИиОФ 0', isFavorite: false},
  {title: 'Добление 0', isFavorite: false},
  {title: 'Корпус ЗИиОФ 6.2', isFavorite: false},
  {title: 'Корпус ЗИиОФ 11.2', isFavorite: false},
  {title: 'Добление 18', isFavorite: false},
];

function MapButtons({showMapSettings, showFloors, zoomIn, zoomOut, disablePrevFloor, disableNextFloor}) {

  return (
    <>
      <Buttons
        showMapSettings={showMapSettings}
        showFloors={showFloors}
        zoomIn={zoomIn}
        zoomOut={zoomOut}
        disableNextFloor={disableNextFloor}
        disablePrevFloor={disablePrevFloor}
      />
    </>
  );
}

export default MapButtons;
