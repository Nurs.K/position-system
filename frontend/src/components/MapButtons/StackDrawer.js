import React, {useState, useRef} from 'react';
import { Drawer, Divider, Checkbox } from 'antd';
import cn from 'classnames';
import isEqual from 'lodash/isEqual';

import { ReactComponent as SettingWrenchLogo } from '../../assets/icons/interface-setting-wrench.svg';
import { ReactComponent as ToolLogo } from '../../assets/icons/tool.svg';
import { ReactComponent as GridLogo } from '../../assets/icons/grid.svg';
import { ReactComponent as ChangeColorLogo } from '../../assets/icons/color-change.svg';
import { ReactComponent as MapTypeLogo } from '../../assets/icons/map-type.svg';
import { ReactComponent as ElementsLogo } from '../../assets/icons/elements.svg';
import { ReactComponent as UserGrayLogo } from '../../assets/icons/user-gray.svg';
import { ReactComponent as ObjectLogo } from '../../assets/icons/object.svg';
import { ReactComponent as MapLocationLogo } from '../../assets/icons/map-location-logo.svg';
import { ReactComponent as ShoppingBagLogo } from '../../assets/icons/shopping-bag.svg';
import { ReactComponent as PriceTagLogo } from '../../assets/icons/price-tag.svg';

import ColorPicker from '../../components/ColorPicker';

import { getGridLayer } from '../../helpers/mapHelpers';

import MapTypes from './MapTypes';
import {useDispatch, useSelector} from "react-redux";
import {setUserColorRequest} from "../../store/actions/user";
import {makeSelectGetUserColor} from "../../store/selectors/user";


const elementsList = [
  {
    text: 'Сотрудники',
    logo: <UserGrayLogo />,
  },
  {
    text: 'Подписи объектов',
    logo: <ObjectLogo />,
  },
  {
    text: 'Области',
    logo: <MapLocationLogo />,
  },
  {
    text: 'Инфраструктура',
    logo: <ShoppingBagLogo />,
  },
  {
    text: 'Ярлыки',
    logo: <PriceTagLogo />,
  },
];

function StackDrawer({ visible, setVisible, map }) {
  const [isGridDisplayed, setIsGridDisplayed] = useState(false);
  const [colorPickerAnchor, setColorPickerAnchor] = useState(null);
  const [isColorPickerOpen, setIsColorPickerOpen] = useState(false);
  const gridLayerRef = useRef(getGridLayer()).current;
  const userMapColor = useSelector(makeSelectGetUserColor)

  const dispatch = useDispatch()

  const _setColorPickerAnchor = node => {
    if (node && !isEqual(node, colorPickerAnchor)) {
      setColorPickerAnchor(node);
    }
  };

  const handleMapColorChange = (color) => {
    setVisible(false);
    dispatch(setUserColorRequest(color))
    // setTimeout(
    //   () => {
    //     setMapColor(color);
    //   },
    //   150,
    // )
  };

  const handleDrawGrid = () => {
    if (!map) return;

    if (isGridDisplayed) {
      setIsGridDisplayed(false);
      map.removeLayer(gridLayerRef);
    } else {
      setIsGridDisplayed(true);
      map.addLayer(gridLayerRef);
    }

    setVisible(false);
  };

  return (
    <React.Fragment>
      <Drawer
        placement="right"
        closable={!isColorPickerOpen}
        keyboard={!isColorPickerOpen}
        maskClosable={!isColorPickerOpen}
        onClose={() => {
          setVisible(false);
        }}
        visible={visible}
        width={350}
      >
        <div
          style={{
            marginTop: 36,
          }}
        >
          <p className="stack-drawer-title">Параметры карты</p>
          <p className="stack-drawer-sub-title">
            <SettingWrenchLogo />
            <span>Инструменты</span>
          </p>
          <div className="logos-wrapper">
            <ToolLogo className="stack-icon" />
            <GridLogo className={cn('stack-icon', { active: isGridDisplayed })} onClick={handleDrawGrid} />
            <div
              style={{
                position: 'relative',
              }}
              ref={_setColorPickerAnchor}
              onClick={() => setIsColorPickerOpen(value => !value)}
            >
              <ChangeColorLogo
                style={{
                  cursor: 'pointer',
                  zIndex: 999,
                }}
              />
            </div>
          </div>
          <Divider />
          <p className="stack-drawer-sub-title">
            <MapTypeLogo />
            <span>Тип карты</span>
          </p>
          <MapTypes className='drawer-types'/>
          <Divider />
          <p className="stack-drawer-sub-title">
            <ElementsLogo />
            <span>Элементы</span>
          </p>
          {elementsList.map((item, idx) => {
            return (
              <div key={idx} className={'element-wrapper'}>
                {item.logo}
                <span>{item.text}</span>
                <Checkbox />
              </div>
            );
          })}
        </div>
      </Drawer>

      <ColorPicker
        color={userMapColor}
        onChange={handleMapColorChange}
        open={isColorPickerOpen}
        anchor={colorPickerAnchor}
        onClose={() => setIsColorPickerOpen(false)}
      />
    </React.Fragment>
  );
}

export default React.memo(StackDrawer);
