import React from 'react';
import { connect } from 'react-redux';
import { Affix, Popover, Tooltip } from 'antd';

import { ReactComponent as SosLargeLogo } from '../../../../assets/icons/sos-large.svg';
import { SEND_SOS } from '../../../../consts/tooltip';
import SosPopoverContent from './SosPopoverContent';

function SosPopover() {
  return (
    <Affix style={{ position: 'absolute', bottom: 60, left: 60, zIndex: 999 }}>
      <Popover
        placement="topLeft"
        content={<SosPopoverContent />}
        trigger="click"
      >
        <Tooltip title={SEND_SOS}>
          <SosLargeLogo
            style={{
              filter: 'drop-shadow(0px 20px 30px rgba(0, 0, 0, 0.15))',
              cursor: 'pointer',
            }}
          />
        </Tooltip>
      </Popover>
    </Affix>
  );
}

export default connect()(SosPopover);
