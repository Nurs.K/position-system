import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Checkbox, Input, notification } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

import { ReactComponent as SosLargeLogo } from '../../../../assets/icons/sos-large.svg';
import { getKeepers } from '../../../../services/keepers';
import { sendGroupSos } from '../../../../services/sos';
import {
  errorNotification,
  successNotification,
} from '../../../../helpers/notificationHelpers';

const CheckboxGroup = Checkbox.Group;

function SosPopoverContent({ labels }) {
  const plainOptions = labels.map(
    ({
      _links: {
        self: { id },
      },
    }) => id
  );

  const [checkedList, setCheckedList] = React.useState([]);
  const [options, setOptions] = React.useState([]);
  const [indeterminate, setIndeterminate] = React.useState(true);
  const [checkAll, setCheckAll] = React.useState(false);

  const onChange = (list) => {
    setCheckedList(list);
    setIndeterminate(!!list.length && list.length < options.length);
    setCheckAll(list.length === options.length);
  };

  const onCheckAllChange = (e) => {
    setCheckedList(e.target.checked ? options : []);
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  const onSearch = (e) => {
    const value = e.target.value;
    if (value) {
      const searchedOptions = plainOptions.filter((item) =>
        item.toLowerCase().includes(value.toLowerCase())
      );
      setOptions(searchedOptions);
    } else {
      setInitialOptions();
    }
  };

  const handleSubmit = () => {
    const str = checkedList.length
      ? checkedList.reduce((result, item) => {
          return `${result},${item}`;
        })
      : '';

    getKeepers({
      embed: 'tag',
      filter: `[deleted==false;name=@;type==EMPLOYEE;labels=#${str};tag.id!=null]`,
      sort: 'name',
    }).then((res) => {
      const keepersList = res.data.items;
      const keeperIds = keepersList.map(({ id }) => id);
      if (keeperIds.length) {
        sendGroupSos(keeperIds)
          .then(() => {
            successNotification('Сигнал на метку отправлен!');
          })
          .catch((err) => {
            errorNotification(
              'Сигнал не доставлен одному или нескольким сотрудникам\n' +
                'Повторите отправку!'
            );
            throw err;
          });
      } else {
        notification.warn({ message: 'По выбранным группам людей не найдено' });
      }
    });
  };

  const setInitialOptions = () => {
    setOptions(plainOptions);
  };

  useEffect(() => {
    setInitialOptions();
  }, [labels]);

  return (
    <div className="sos-checkbox__wrapper">
      <div className="footer">
        <Input
          className="search"
          placeholder="Начните вводить"
          onChange={onSearch}
          suffix={<SearchOutlined />}
        />
        <span>
          <SosLargeLogo
            onClick={handleSubmit}
            style={{
              cursor: 'pointer',
            }}
          />
        </span>
      </div>
      <div className="checkbox-group">
        <Checkbox
          className="head"
          indeterminate={indeterminate}
          onChange={onCheckAllChange}
          checked={checkAll}
        >
           Зоны и области
        </Checkbox>
        <CheckboxGroup
          options={options}
          value={checkedList}
          onChange={onChange}
        />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    labels: state.labels.labelsData.data?.items || [],
  };
};

export default connect(mapStateToProps)(SosPopoverContent);
