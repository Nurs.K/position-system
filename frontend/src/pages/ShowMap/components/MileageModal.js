import React from 'react';
import { Modal } from 'antd';

import LogArrowAltUp from '../../../components/Icons/LogArrowAltUp';
import { GREEN, RED } from '../../../consts/color';
import { toDegree } from '../../../helpers/convertHelpers';
import moment from 'moment';

function MileageModal({ handleCancel, mileagePointData }) {
  return (
    <Modal
      title="Прохождение через участок"
      wrapClassName="mileage-modal-wrapper"
      centered
      visible={true}
      footer={null}
      onCancel={handleCancel}
    >
      {mileagePointData.items.map((item) => {
        const isStatusOk = item.status === 'OK';
        return (
          <div className="item-wrapper" key={item.id}>
            <LogArrowAltUp
              style={{ transform: `rotate(${toDegree(item.rotate)}deg)` }}
              color={isStatusOk ? GREEN : RED}
            />
            <div className="text-wrapper">
              <p className="header">
                Время: {moment(item.time).format('DD.MM.YY HH:mm:ss')}
              </p>
              <p className="text">Состояние: {isStatusOk ? 'Ок' : 'Ошибка'}</p>
            </div>
          </div>
        );
      })}
    </Modal>
  );
}

export default MileageModal;
