import React, {useEffect, useRef, useState} from 'react';
import {connect, useSelector} from 'react-redux';
import ReactDOMServer from 'react-dom/server';
import isNil from 'lodash/isNil';
import L from 'leaflet';

import { LocationArrow } from '../../../../components/Icons';

import {
  addLayerGroup,
  addMarker,
  addPolyLine,
  convertCoordinatesToLatLng,
  createMarker,
  getMarkerByType,
  initMap,
  onClickMarker,
} from '../../../../helpers/mapHelpers';
import { getKeepers } from '../../../../services/keepers';
import { toDegree } from '../../../../helpers/convertHelpers';

import './index.scss';
import { getMileagePointRequest } from '../../../../store/actions/reports';
import { clearCenterCoordinates } from '../../../../store/actions/crsMap';
import { selectMarkerConfigList } from "../../../../store/selectors/markerConfigurations";
import { getListRequest } from '../../../../store/actions/markerConfigurations';
import {makeSelectGetUserColor} from "../../../../store/selectors/user";

let isHistory = false;

function CRSMap(props) {
  const {
    handleClickKeeper,
    singleMap,
    owsData,
    showDrawer,
    keeperCoordinate,
    getMileagePointHistory,
    centerCoordinates,
    clearCenterCoordinates,
    getMarkerTypes,
    markerTypes,
    map,
    setMap,
    mapIdUrl
  } = props;

  const keepersInterval = useRef();

  const mapId = 'map';

  const [data, setData] = useState();
  const [markersGroup, setMarkersGroup] = useState();
  const [keeperMarker, setKeeperMarker] = useState();
  const [line, setLine] = useState();

  const mapColor = useSelector(makeSelectGetUserColor)

  const style = {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: mapColor ? mapColor : "#ddd",
    opacity: 0.2,
    zIndex: 399,
  }

  const getBounds = () => {
    const { min_x: minX, min_y: minY, max_x: maxX, max_y: maxY } = singleMap;
    return [
      { lat: minY, lng: minX },
      { lat: maxY, lng: maxX },
    ];
  };

  const setMarkers = (keepers) => {
    const {min_z, max_z} = singleMap;
    const markers = keepers
      .map((keeper) => {
        const { _embedded } = keeper;

        if (!_embedded) return null;
        const {
          tag: { x, y, z },
        } = _embedded;

        if (isNil(x) || isNil(y) || isNil(z)) {
          return null;
        }

        if (z < min_z || z > max_z){
          return null
        }

        const marker = createMarker({
          map,
          latLng: { lat: y, lng: x },
          icon: getMarkerByType(keeper.type, markerTypes)
        });

        onClickMarker({
          marker,
          setVisible: () => handleClickKeeper(keeper)
        });

        return marker;
      })
      .filter(Boolean);

    setMarkersGroup(addLayerGroup({ map, markers }));
  };

  const requestKeepers = () => {
    getKeepers({
      embed: 'tag',
      filter: '[deleted==false;name=@;type==EMPLOYEE]',
      sort: 'name',
    })
      .then((res) => setData(res?.data?.items))
      .catch((err) => {
        clearKeepersInterval();
        throw err;
      });
  };

  const setKeepersInterval = () => {
    keepersInterval.current = setInterval(requestKeepers, 3000);
  };

  const clearKeepersInterval = () => {
    clearInterval(keepersInterval.current);
  };

  const drawLine = () => {
    const coordinatesList = owsData.features.map(
      ({ geometry: { coordinates } }) => convertCoordinatesToLatLng(coordinates)
    );

    setLine(
      addPolyLine({
        map,
        latLngs: coordinatesList,
      })
    );
  };

  const onClickMap = () => {
    if (isHistory) {
      const params = {
        limit: 20,
        filter:
          '[time>=2021-09-07T00:00:00.0+06:00;time<=2021-09-14T00:00:00.0+06:00;site.id==1]',
        sort: 'time',
        geom: '[31.917787341957723,-17.622006951840362,0.6968306590637847]',
      };

      getMileagePointHistory({
        slug: '1519',
        params,
      });
    }
  };

  useEffect(
    () => {
      getMarkerTypes();

      return () => {
        clearKeepersInterval();
      };
    },
    // eslint-disable-next-line
    []
  );

  useEffect(() => {
    if (map && singleMap) {
      map.off()
      map.remove()
      setMap(initMap({mapId, image: singleMap.file, bounds: getBounds()}))
    }
  }, [mapIdUrl, singleMap])

  useEffect(() => {
    if (!map && singleMap) {
      setMap(initMap({ mapId, image: singleMap.file, bounds: getBounds()}));
    }
  }, [singleMap]);

  useEffect(() => {
    if (map) map.on('click', onClickMap);
  }, [map]);

  useEffect(() => {
    if (map && data) {
      markersGroup?.remove();
      setMarkers(data);
    }
  }, [map, data]);

  useEffect(() => {
    line?.remove();
    isHistory = !!owsData;

    if (owsData) {
      clearKeepersInterval();
      setData([]);
      drawLine();
    } else {
      requestKeepers();
      setKeepersInterval();
      keeperMarker?.remove();
    }
  }, [owsData]);

  useEffect(() => {
    if (keeperCoordinate) {
      keeperMarker?.remove();
      const {
        geometry: { coordinates },
        properties: { rotate },
      } = keeperCoordinate;
      const latLng = convertCoordinatesToLatLng(coordinates);
      const icon = L.divIcon({ html: ReactDOMServer.renderToString(<LocationArrow />) });
      setKeeperMarker(
        addMarker({
          map,
          latLng,
          rotationAngle: toDegree(rotate, -45),
          icon,
        })
      );
    }
  }, [keeperCoordinate]);

  useEffect(() => {
    if (centerCoordinates) {
      map?.flyTo(centerCoordinates, 3);
      clearCenterCoordinates();
    }
  }, [centerCoordinates]);

  return (
    <div className="map-wrapper">
      {owsData && <button onClick={showDrawer}>open drawer</button>}
      <div id={mapId}>
        <div style={style} />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    singleMap: state.svgMap.singleMapState.data,
    owsData: state.ows.owsState.data,
    keeperCoordinate: state.ows.keeperCoordinate,
    centerCoordinates: state.crsMap.centerCoordinates,
    markerTypes: selectMarkerConfigList(state),
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getMileagePointHistory: (args) => dispatch(getMileagePointRequest(args)),
    clearCenterCoordinates: () => dispatch(clearCenterCoordinates()),
    getMarkerTypes: () => dispatch(getListRequest())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(CRSMap);
