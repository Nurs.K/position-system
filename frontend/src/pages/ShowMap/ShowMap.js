import React, {useEffect, useState} from 'react';
import {useParams} from 'react-router-dom';
import {connect, useDispatch, useSelector} from 'react-redux';

import CRSMap from './components/CRSMap';
import UserDrawer from '../../components/UserDrawer';
import MapButtons from '../../components/MapButtons';
import {getMapByIdRequest, getMapsListRequest} from '../../store/actions/svgMap';
import Spinner from '../../components/Spinner';
import MileageModal from './components/MileageModal';
import {clearMileagePoint} from '../../store/actions/reports';

import StackDrawer from '../../components/MapButtons/StackDrawer';

import './index.scss';
import {makeSelectIsUserDrawerVisible} from "../../store/selectors/labels";
import {setIsUserDrawerVisible} from "../../store/actions/labels";
import {zoomInMap, zoomOutMap} from "../../helpers/mapHelpers";
import {DEFAULT_SIZE} from "../Sandwich/constants";
import {makeSelectGetMapsList} from "../../store/selectors/svgMap";

function ShowMap({
                   getMapById,
                   mileagePointFetching,
                   mileagePointData,
                   closeModal,
                 }) {
  const {mapId} = useParams();
  // const [isDrawerVisible, setDrawerVisible] = useState(false);
  const isDrawerVisible = useSelector(makeSelectIsUserDrawerVisible)
  const mapList = useSelector(makeSelectGetMapsList)
  const [keeper, setKeeper] = useState();
  const [disableNextFloor, setDisableNexFloor] = useState(false);
  const [disablePrevFloor, setDisablePrevFloor] = useState(false);
  const [isMapSettingsVisible, setIsMapSettingsVisible] = useState(false);
  const [map, setMap] = useState();
  const dispatch = useDispatch()

  const closeDrawer = () => {
    dispatch(setIsUserDrawerVisible());
  };

  const showDrawer = () => {
    dispatch(setIsUserDrawerVisible());
  };

  const handleClickKeeper = (keeper) => {
    setKeeper(keeper);
    showDrawer();
  };

  const zoomIn = () => {
    if (map) {
      zoomInMap({map})
    }
  }

  const zoomOut = () => {
    if (map) {
      zoomOutMap({map})
    }
  }

  const handleCancel = () => {
    closeModal();
  };

  const checkHasFloors = () => {
    const currentMap = mapList?.find(item => item.id === mapId)

    const currentSandwich = mapList?.length
      ? mapList.filter(item => item.category.id === currentMap.category.id)
      : []
    const nextFloors = currentSandwich?.length
      ? currentSandwich.filter(item => item.floor > currentMap.floor)
      : []
    const prevFloors = currentSandwich?.length
      ? currentSandwich.filter(item => item.floor < currentMap.floor)
      : []

    setDisableNexFloor(!nextFloors.length)
    setDisablePrevFloor(!prevFloors.length)
  }

  useEffect(() => {
    if (mapList) {
      checkHasFloors()
    }
  }, [mapId, mapList])

  useEffect(() => {
    dispatch(getMapsListRequest({size: DEFAULT_SIZE}))
    getMapById(mapId);
  }, [mapId]);

  return (
    <>
      <UserDrawer
        visible={isDrawerVisible}
        keeper={keeper}
        onClose={closeDrawer}
      />
      {mileagePointFetching && <Spinner/>}
      <CRSMap
        handleClickKeeper={handleClickKeeper}
        showDrawer={showDrawer}
        map={map}
        setMap={setMap}
        mapIdUrl={mapId}
      />
      <MapButtons
        showMapSettings={() => setIsMapSettingsVisible(true)}
        showFloors={true}
        disableNextFloor={disableNextFloor}
        disablePrevFloor={disablePrevFloor}
        zoomOut={zoomOut}
        zoomIn={zoomIn}
      />
      {mileagePointData && (
        <MileageModal
          mileagePointData={mileagePointData}
          handleCancel={handleCancel}
        />
      )}

      <StackDrawer
        visible={isMapSettingsVisible}
        setVisible={setIsMapSettingsVisible}
        map={map}
      />
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    mileagePointFetching: state.reports.mileagePointState.fetching,
    mileagePointData: state.reports.mileagePointState.data,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMapById: (mapId) => dispatch(getMapByIdRequest(mapId)),
    closeModal: () => dispatch(clearMileagePoint()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ShowMap);
