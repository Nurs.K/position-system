import React from 'react';
import moment from 'moment';

import {ReactComponent as BxsHotel} from '../../assets/icons/bxsHotel.svg';

export default function ShowTime({title, image, idx}) {
  return (
    <div className="show-time">
      {image ? <img src={image} alt='notification' className='show-time__image'/> : <BxsHotel/>}
      <div className="show-text"> {moment(title).format('DD.MM.YY HH:mm:ss')} </div>
    </div>
  );
};
