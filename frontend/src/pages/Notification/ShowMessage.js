import React from 'react';

export default function ShowMessage ({ title, handleClick }) {
    return(
        <div className="show-message">
            <div className="message-text">{title}</div>
            <div className="message-text-next" onClick={handleClick}>
                <a>Отчет</a>
            </div>
        </div>
    );
};
