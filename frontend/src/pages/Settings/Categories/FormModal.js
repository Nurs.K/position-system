import React, { useRef } from 'react';
import { Modal } from 'antd';
import CategoryForm from './CategoryForm';

const FormModal = ({ handleOk, handleCancel, category, loading }) => {
  const submitRef = useRef();

  const handleClickOk = () => {
    submitRef.current.click();
  };
  return (
    <>
      <Modal
        title={category?.id ? "Редактировать": "Добавить"}
        visible={true}
        onOk={handleClickOk}
        onCancel={handleCancel}
        okText={
          loading
            ? "Загрузка..."
            : (
              category?.id
                ? 'Сохранить'
                : "Создать"
            )
        }
        cancelText="Отмена"
      >
        <CategoryForm
          category={category}
          submitRef={submitRef}
          handleSubmit={handleOk}
          loading={loading}
        />
      </Modal>
    </>
  );
};

export default FormModal;
