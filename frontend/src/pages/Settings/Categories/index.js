import React, { useEffect, useState } from 'react';
import {useSelector} from "react-redux";
import {Avatar, Button, Table} from 'antd';
import { connect } from 'react-redux';
import { EditFilled, DeleteFilled, PlusOutlined } from '@ant-design/icons';

import {
  categoriesRequest,
  categoryByIdRequest,
  clearSingleCategoryState,
  updateCategoryRequest,
  createCategoryRequest,
  deleteCategoryRequest,
} from '../../../store/actions/categories';
import {
  makeSelectConfirmModalOpen,
  makeSelectCreateModalOpen,
  makeSelectCategoryTableData,
  makeSelectCategoryFetching,
  makeSelectCategoriesCount
} from "../../../store/selectors/categories";
import { openConfirmModal, openCreateModal } from "../../../store/actions/categories";

import SettingsLayout from '../SettingsLayout';
import ConfirmModal from '../../../components/Modal/ConfirmModal';
import FormModal from './FormModal';
import {getQuery} from "../../../helpers/query";
import {DEFAULT_PAGE_SIZE} from "../../../components/Pagination/constants";
import {MIN_PAGINATION_ITEMS} from "../consts";

const CategoriesPage = ({
  history,
  dispatch,
  singleCategory,
  createCategory,
  updateProcessing,
}) => {
  const [deletedId, setDeletedId] = useState();
  const [page, setPage] = useState(getQuery().page || 1);
  const [size, setSize] = useState(getQuery().size || DEFAULT_PAGE_SIZE);

  const loading = useSelector(makeSelectCategoryFetching);
  const count = useSelector(makeSelectCategoriesCount);
  const isConfirmModalOpen = useSelector(makeSelectConfirmModalOpen)
  const isCreateFormModalOpen = useSelector(makeSelectCreateModalOpen)

  const closeConfirmModal = () => {
    dispatch(openConfirmModal(false))
  }

  const handleClickDelete = (id) => {
    setDeletedId(id);
    dispatch(openConfirmModal(true))
  };

  const handleClickEdit = (category) => {
    dispatch(categoryByIdRequest({ id: category.id }));
  };

  const closeFormModal = () => {
    dispatch(clearSingleCategoryState());
    dispatch(openCreateModal(false))
  };

  const handleClickCreate = () => {
    dispatch(openCreateModal(true));
  };

  const handleEditSubmit = ({ id, name, order, thumbnail }) => {
    const formData = new FormData();
    try {
      if (name !== singleCategory?.name) {
        formData.append('name', name);
      }
      if (order !== singleCategory?.order) {
        formData.append('order', order);
      }

      if (thumbnail) {
        formData.append('thumbnail', thumbnail);
      }
      for (const key of formData.keys()) {
        console.log(key, '=>', formData.get(key));
      }
      if (Array.from(formData.keys()).length) {
        dispatch(updateCategoryRequest({ formData, id }));
      }
    } catch (error) {
      console.log('error', error)
    }
  };

  const handleCreateSubmit = ({ name, thumbnail }) => {
    const formData = new FormData();
    formData.append('name', name);

    if (thumbnail) {
      formData.append('thumbnail', thumbnail);
    }
    if (Array.from(formData.keys()).length) {
      dispatch(createCategoryRequest({ formData }));
    }
  };

  const handleConfirm = () => {
    dispatch(deleteCategoryRequest({ id: deletedId }))
  };

  useEffect(
      () => {
        dispatch(categoriesRequest({ page, size }));
      },
      // eslint-disable-next-line
      []
  );

  const handlePaginationChange = (page, size) => {
    setPage(page);
    setSize(size);
  };

  useEffect(
    () => {
      dispatch(categoriesRequest({ page, size }));
    },
    // eslint-disable-next-line
    [page, size],
  );

  const columns = [
    {
      title: 'Название',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Изображение',
      dataIndex: 'thumbnail',
      key: 'thumbnail',
      render: thumbnail => (
        thumbnail
          ? (
            <Avatar src={thumbnail} />
          )
          : null
      )
    },
    {
      title: 'Действия',
      dataIndex: 'instance',
      key: 'actions',
      render: instance => (
        <>
          <EditFilled
            className="table-action edit"
            onClick={() => handleClickEdit(instance)}
            style={{ fontSize: 25 }}
          />
          <DeleteFilled
            className="table-action"
            onClick={() => handleClickDelete(instance.id)}
            style={{ fontSize: 25 }}
          />
        </>
      )
    }
  ];

  const data = useSelector(makeSelectCategoryTableData(columns));

  return (
    <SettingsLayout history={history}>
      <div className="table-wrapper">
        <div className="btn-create-wrapper">
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleClickCreate}
          >
            Добавить
          </Button>
        </div>
        <Table
          columns={columns}
          dataSource={data}
          loading={loading}
          pagination={data.length < MIN_PAGINATION_ITEMS ? false : {
            total: count,
            current: page,
            pageSize: size,
            onChange: handlePaginationChange,
            hideOnSinglePage: false,
            showSizeChanger: true,
            position: ['bottomLeft'],
          }}
        />
      </div>
      <ConfirmModal
        visible={isConfirmModalOpen}
        title="Удалить!!!"
        message="Вы действительно хотите удалить?"
        handleConfirm={handleConfirm}
        handleCancel={closeConfirmModal}
      />
      {isCreateFormModalOpen && (
        <FormModal
          category={createCategory}
          handleOk={handleCreateSubmit}
          handleCancel={closeFormModal}
          // loading={updateProcessing}
        />
      )}
      {singleCategory && (
        <FormModal
          category={singleCategory}
          handleOk={handleEditSubmit}
          handleCancel={closeFormModal}
          loading={updateProcessing}
        />
      )}
    </SettingsLayout>
  );
};

const mapStateToProps = (state) => ({
  singleCategory: state.categories.singleCategory.data,
  updateProcessing: state.categories.categoryUpdateState.fetching,
});

export default connect(mapStateToProps)(CategoriesPage);
