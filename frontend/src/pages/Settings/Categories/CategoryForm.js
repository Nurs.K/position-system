import React, { useRef, useState } from 'react';
import { Button, Form, Input } from 'antd';

export default function CategoryForm({ submitRef, handleSubmit, category, loading }) {
  const imageInput = useRef(null);
  const [file, setFile] = useState();
  const [imgUrl, setImgUrl] = useState(category?.thumbnail);

  const handleClickUpload = () => {
    imageInput.current.click();
  };

  const handleImageLoad = (e) => {
    const [file] = e.target.files;
    if (file) {
      setFile(file);
      setImgUrl(URL.createObjectURL(file));
    }
  };

  const handleClickSubmit = ({ name, order }) => {
    handleSubmit({order, name, thumbnail: file, id: category?.id });
  };

  return (
    <div className="categories">
      <div className="categories-form">
        <Form onFinish={handleClickSubmit}>
          <Form.Item
            name="name"
            label="Название картинки"
            initialValue={category?.name}
            rules={[{ required: true, message: 'Введите текст' }]}
          >
            <Input shape="round" disabled={loading} />
          </Form.Item>
          <Form.Item
            name="order"
            label="Порядок сортировки"
            initialValue={category?.order}
            rules={[{ required: true, message: 'Введите порядок сортировки' }]}
          >
            <Input min={0} type="number" shape="round" disabled={loading} />
          </Form.Item>
          <Form.Item>
            <Button className="form-btn" onClick={handleClickUpload} disabled={loading}>
              Загрузка картинки
            </Button>
          </Form.Item>
          <Form.Item>
            <Button ref={submitRef} className="d-none" htmlType="submit" loading={loading}>
              {' '}
            </Button>
          </Form.Item>
        </Form>
      </div>
      <div className="categories-image">
        <input
          ref={imageInput}
          className="d-none"
          type="file"
          name="image"
          onChange={handleImageLoad}
        />
        {imgUrl && <img src={imgUrl} alt="category img" />}
      </div>
    </div>
  );
}
