import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import {Button, Switch, Table, Tooltip} from 'antd';
import { EditFilled, PlusOutlined } from '@ant-design/icons';

import SettingsLayout from '../SettingsLayout';
import {
  getMapsListRequest,
  editMapRequest,
} from '../../../store/actions/svgMap';
import {
  makeSelectMapsListFetching,
  makeSelectGetMainMap,
  makeSelectGetMainMapFetching,
  makeSelectMapListCount,
  makeSelectTableData,
} from '../../../store/selectors/svgMap';
import {
  toEditMapId,
  toDrawPolygonsMapId,
  toPutAnchors,
} from '../../../consts/routes';
import { UPLOAD_MAP } from '../../../consts/urls';
import { getQuery } from '../../../helpers/query';
import { DEFAULT_PAGE_SIZE } from '../../../components/Pagination/constants';
import { EDIT_MAP, EDIT_POLYGON, EDIT_ANCHORS } from '../../../consts/tooltip';
import { ExpandIcon, Anchors } from '../../../components/Icons';
import {MIN_PAGINATION_ITEMS} from "../consts";

const MapsConfiguration = ({ history }) => {
  const count = useSelector(makeSelectMapListCount);
  const [page, setPage] = useState(getQuery().page || 1);
  const [size, setSize] = useState(getQuery().size || DEFAULT_PAGE_SIZE);

  const loading = useSelector(makeSelectMapsListFetching);
  const main = useSelector(makeSelectGetMainMap);
  const isMainLoading = useSelector(makeSelectGetMainMapFetching);

  const dispatch = useDispatch();

  useEffect(
    () => {
      dispatch(getMapsListRequest({ page, size }));
    },
    // eslint-disable-next-line
    []
  );

  const updateUrl = () => {
    const params = new URLSearchParams();
    params.append('page', page.toString());
    params.append('size', size.toString());
    history.push({ search: `?${params.toString()}` });
  };

  const handlePaginationChange = (page, size) => {
    setPage(page);
    setSize(size);
  };

  useEffect(
    () => {
      dispatch(getMapsListRequest({ page, size }));
      updateUrl();
    },
    // eslint-disable-next-line
    [page, size, dispatch]
  );

  const handleSwitch = (checked, id) => {
    let formData = new FormData();
    formData.append('is_main', checked);
    dispatch(editMapRequest({ formData, id }));
  };

  const columns = [
    {
      title: 'Название',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Категория (Дробление)',
      dataIndex: 'category.name',
      key: 'category',
    },
    {
      title: 'Этаж',
      dataIndex: 'floor',
      key: 'floor',
    },
    {
      title: 'Главная карта',
      dataIndex: 'instance',
      key: 'is_main',
      render: (instance) => (
        <Switch
          checked={instance.id === main?.id}
          onChange={(checked) => handleSwitch(checked, instance.id)}
          disabled={
            (instance.id === main?.id ? false : Boolean(main)) || isMainLoading
          }
        />
      ),
    },
    {
      title: 'Действия',
      dataIndex: 'id',
      key: 'actions',
      render: (id) => (
        <div className="actions-wrapper">
          <Tooltip title={EDIT_MAP}>
            <NavLink to={toEditMapId(id)} className="demo-list__link" exact>
              <EditFilled style={{ fontSize: 25 }} />
            </NavLink>
          </Tooltip>
          <Tooltip title={EDIT_POLYGON}>
            <NavLink
              to={toDrawPolygonsMapId(id)}
              className="demo-list__link"
              exact
            >
              <ExpandIcon />
            </NavLink>
          </Tooltip>
          <Tooltip title={EDIT_ANCHORS}>
            <NavLink
              to={toPutAnchors(id)}
              className="demo-list__link"
              exact
            >
              <Anchors />
            </NavLink>
          </Tooltip>
        </div>
      ),
    },
  ];

  const data = useSelector(makeSelectTableData(columns));

  return (
    <SettingsLayout history={history}>
      <div className="table-wrapper">
        <div className="btn-create-wrapper">
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={() => history.push(UPLOAD_MAP)}
          >
            Добавить Карту
          </Button>
        </div>
        <Table
          columns={columns}
          dataSource={data}
          pagination={data.length < MIN_PAGINATION_ITEMS ? false : {
            total: count,
            current: page,
            pageSize: size,
            onChange: handlePaginationChange,
            hideOnSinglePage: false,
            showSizeChanger: true,
            position: ['bottomLeft'],
          }}
          loading={loading}
        />

      </div>
    </SettingsLayout>
  );
};

export default MapsConfiguration;
