import React from 'react';
import {Layout, Menu} from 'antd';

import {menu} from './constants';

const {SubMenu} = Menu;
const {Content, Sider} = Layout;

const SettingsLayout = ({history, children}) => {
  const {location} = history;

  const filerPathName = (arrayObjects) => {
    return arrayObjects
      .filter(item =>
        item.hasSubMenu
          ? !!item.subItems.filter(subItem => subItem.path === location.pathname).length
          : item.path === location.pathname
      )
  }

  const selectedKeys = () => {
    const selected = filerPathName(menu)

    let subMenu = []
    let actualMenu = []

    for (let i in selected) {
      if (selected[i].hasSubMenu) {
        const selectedSubMenu = filerPathName(selected[i].subItems).map(item => item.key)
        subMenu = [...selectedSubMenu]
      } else {
        actualMenu.push(selected[i].key)
      }
    }
    return [...subMenu, ...actualMenu]
  }

  return (
    <Layout>
      <Content style={{padding: '0 50px'}}>
        <Layout
          className="site-layout-background"
          style={{padding: '24px 0'}}
        >
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              style={{height: '100%'}}
              selectedKeys={selectedKeys()}
            >
              {menu.map(item => (
                item.hasSubMenu ? (
                  <SubMenu
                    key={item.key}
                    title={item.label}
                  >
                    {item.subItems.map(sub => (
                      <Menu.Item
                        onClick={() => {
                          history.push(sub.path);
                        }}
                        key={sub.key}
                      >
                        {sub.label}
                      </Menu.Item>
                    ))}
                  </SubMenu>
                ) : (
                  <Menu.Item
                    onClick={() => {
                      history.push(item.path);
                    }}
                    key={item.key}
                  >
                    {item.label}
                  </Menu.Item>
                )
              ))}
            </Menu>
          </Sider>
          <Content style={{padding: '0 24px', minHeight: 280}}>
            {children}
          </Content>
        </Layout>
      </Content>
    </Layout>
  );
};
export default SettingsLayout;
