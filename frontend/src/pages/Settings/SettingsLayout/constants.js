import {
  SETTINGS_CATEGORIES,
  SETTINGS_MAP_MARKERS,
  SETTINGS_MAPS,
  SETTINGS_NOTIFICATIONS,
  SETTINGS_DEVICES,
  SETTINGS_MACROSCOP
} from "../../../consts/urls";


export const menu = [
  {
    path: SETTINGS_CATEGORIES,
    label: 'Категории',
    key: 'categories',
    hasSubMenu: false,
  },
  {
    path: SETTINGS_NOTIFICATIONS,
    label: 'Реакции',
    key: 'notifications',
    hasSubMenu: false,
  },
  {
    path: SETTINGS_MAP_MARKERS,
    label: 'Метки на карте',
    key: 'markers',
    hasSubMenu: false,
  },
  {
    path: SETTINGS_MAPS,
    label: 'Список карт',
    key: 'maps',
    hasSubMenu: false,
  },
  {
    label: 'Устройства',
    key: 'devicesMenu',
    hasSubMenu: true,
    subItems: [
      {
        path: SETTINGS_MACROSCOP,
        label: 'Macroscop',
        key: 'macroscop',
      },
      {
        path: SETTINGS_DEVICES,
        label: 'Объекты',
        key: 'devices',
      },
    ]
  }
];
