
const config = [
  {
    type: 'text',
    name: 'name',
    label: 'Название',
    required: true,
  },
  {
    type: 'boolean',
    name: 'status',
    label: 'Статус',
    defaultChecked: false,
  }
];

export default config;