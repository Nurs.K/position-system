const DEVICE_BASE = 'devices';
export const CREATE_FORM = `${DEVICE_BASE}_create`;
export const EDIT_FORM = `${DEVICE_BASE}_edit`;