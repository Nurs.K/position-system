
const submit = (formValues, initials) => {
  const data = {};

  if (formValues.name) {
    if (initials) {
      if (initials.name !== formValues.name) {
        data.name = formValues.name;
      }
    } else {
      data.name = formValues.name;
    }
  }

  if (typeof formValues.status === 'boolean') {
    if (initials) {
      if (initials.status !== formValues.status) {
        data.status = formValues.status;
      }
    } else {
      data.status = formValues.status;
    }
  }

  return data;
};

export default submit;
