import React, { useEffect, useState } from 'react';
import { Table, Button, Typography, Modal } from "antd";
import { useDispatch, useSelector } from 'react-redux';
import { PlusOutlined } from '@ant-design/icons';

import FormDialog from '../../../components/SettingsFormDialog';
import { DEFAULT_PAGE_SIZE } from "../../../components/Pagination/constants";

import {
  getDevicesRequest,
  nominateToDelete,
  setIsCreateModalOpen,
  setIsUpdateModalOpen,
  createDevicesRequest,
  updateDevicesRequest,
  deleteDevicesRequest,
} from '../../../store/actions/devices';
import {
  makeSelectDevicesTableData,
  makeSelectDevicesLoading,
  makeSelectDevicesCount,
  makeSelectCreatingLoading,
  makeSelectUpdatingLoading,
  makeSelectCreatingModalOpen,
  makeSelectUpdatingModalOpen,
  makeSelectDevicesList,
  makeSelectNominatedToDelete,
  makeSelectDeletingLoading,
} from '../../../store/selectors/devices';
import {
  initForm,
  setFormInitialValues,
  clearForm,
  removeForm
} from '../../../store/actions/forms';
import { selectFormValues } from '../../../store/selectors/forms';

import { getQuery } from "../../../helpers/query";

import SettingsLayout from '../SettingsLayout';

import { CREATE_FORM, EDIT_FORM } from './form/constants';
import config from './form/config';
import submit from './form/submit';

import columns from "./columns";


const MIN_PAGINATION_ITEMS = 20;

const Devices = ({ history }) => {
  const dispatch = useDispatch();
  const [page, setPage] = useState(getQuery().page || 1);
  const [size, setSize] = useState(getQuery().size || DEFAULT_PAGE_SIZE);

  const devicesList = useSelector(makeSelectDevicesList);
  const devicesLoading = useSelector(makeSelectDevicesLoading);
  const devicesTotalCount = useSelector(makeSelectDevicesCount);

  const isCreationLoading = useSelector(makeSelectCreatingLoading);
  const isUpdatingLoading = useSelector(makeSelectUpdatingLoading);
  const isDeletingLoading = useSelector(makeSelectDeletingLoading);
  const isCreationModalOpen = useSelector(makeSelectCreatingModalOpen);
  const isUpdatingModalOpen = useSelector(makeSelectUpdatingModalOpen);

  const createFormValues = useSelector(selectFormValues(CREATE_FORM));
  const editFormValues = useSelector(selectFormValues(EDIT_FORM));
  const nominatedToDelete = useSelector(makeSelectNominatedToDelete);

  const fetchDevices = (page = 1) => {
    dispatch(getDevicesRequest({ page, size }));
  };

  const handleCloseCreateForm = () => {
    dispatch(clearForm({ form: CREATE_FORM }));
    dispatch(setIsCreateModalOpen(false));
  };

  const handleCloseUpdateForm = () => {
    dispatch(setIsUpdateModalOpen(false));
    dispatch(clearForm({ form: EDIT_FORM }));
  };

  const handlePaginationChange = (page, size) => {
    setPage(page);
    setSize(size);
  };

  const updateUrl = () => {
    const params = new URLSearchParams();
    params.append('page', page.toString());
    params.append('size', size.toString());
    history.push({ search: `?${params.toString()}` });
  };

  const handleCreateRequest = () => {
    dispatch(setIsCreateModalOpen(true));
  };

  const handleCreateSubmit = () => {
    const data = submit(createFormValues);

    if (Object.keys(data).length) {
      dispatch(createDevicesRequest({ data }));
    }
  };

  const handleEditSubmit = () => {
    const { id, ...rest } = editFormValues;
    const instance = devicesList.find(item => item.id === id);
    const data = submit(rest, instance);

    if (Object.keys(data).length) {
      dispatch(updateDevicesRequest({ id, data }));
    }
  };

  const handleCloseDeleteModal = () => {
    dispatch(nominateToDelete(null));
  };

  const handleDeleteSubmit = () => {
    dispatch(deleteDevicesRequest({ id: nominatedToDelete }));
  };

  const handleEditRequest = instance => () => {
    dispatch(setFormInitialValues({ form: EDIT_FORM, instance }));
    dispatch(setIsUpdateModalOpen(true));
  };
  const handleDeleteRequest = id => () => {
    console.log({ id });
    dispatch(nominateToDelete(id));
  };
  const columnCallbacks = {
    onEdit: handleEditRequest,
    onDelete: handleDeleteRequest,
  };

  useEffect(
    () => {
      fetchDevices(page);
      updateUrl();
    },
    // eslint-disable-next-line
    [page, size, dispatch]
  );

  useEffect(
    () => {
      fetchDevices();

      dispatch(initForm({ form: CREATE_FORM, config }));
      dispatch(initForm({ form: EDIT_FORM, config }));

      return () => {
        dispatch(removeForm({ form: CREATE_FORM }));
        dispatch(removeForm({ form: EDIT_FORM }));
      };
    },
    // Need to call this only once at render
    // eslint-disable-next-line
    [],
  );

  const preparedColumns = columns(columnCallbacks);
  const devices = useSelector(makeSelectDevicesTableData(preparedColumns));

  return (
    <SettingsLayout history={history}>
      <div className="table-wrapper">
        <div className="table-actions">
          <Typography.Title>Объекты</Typography.Title>
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateRequest}
          >
            Добавить устройство
          </Button>
        </div>
        <Table
          loading={devicesLoading}
          columns={preparedColumns}
          dataSource={devices}
          pagination={devicesTotalCount < MIN_PAGINATION_ITEMS ? false : {
            total: devicesTotalCount,
            current: page,
            pageSize: size,
            onChange: handlePaginationChange,
            hideOnSinglePage: false,
            showSizeChanger: true,
            position: ['bottomLeft'],
          }}
        />
      </div>

      <FormDialog
        title="Добавить устройство"
        visible={isCreationModalOpen}
        onClose={handleCloseCreateForm}
        form={CREATE_FORM}
        config={config}
        buttonText={isCreationLoading ? 'Загрузка...' : 'Создать'}
        loading={isCreationLoading}
        handleSubmit={handleCreateSubmit}
      />
      <FormDialog
        title="Редактировать устройство"
        visible={isUpdatingModalOpen}
        onClose={handleCloseUpdateForm}
        form={EDIT_FORM}
        config={config}
        buttonText={isUpdatingLoading ? 'Загрузка...' : 'Редактировать'}
        loading={isUpdatingLoading}
        handleSubmit={handleEditSubmit}
      />
      <Modal
        title="Подтвердите действие"
        visible={Boolean(nominatedToDelete)}
        onCancel={handleCloseDeleteModal}
        okButtonProps={{ danger: true, disabled: isDeletingLoading }}
        okType="primary"
        okText={isDeletingLoading ? 'Загрузка...' : 'Удалить'}
        onOk={handleDeleteSubmit}
        cancelText="Отмена"
      >
        <Typography.Text>Вы действительно хотите удалить устройство?</Typography.Text>
      </Modal>
    </SettingsLayout>
  );
};

export default Devices;
