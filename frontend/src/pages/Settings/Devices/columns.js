import React from 'react';
import { DeleteFilled, EditFilled } from "@ant-design/icons";

import Status from "./status";


const columns = ({ onEdit, onDelete }) => [
  {
    title: 'Название',
    dataIndex: 'name',
    key: 'name',
    render: name => name,
  },
  {
    title: 'Статус',
    dataIndex: 'status',
    key: 'status',
    render: value => <Status value={value} />
  },
  {
    title: 'Действия',
    dataIndex: 'instance',
    key: 'actions',
    render: instance => (
      <>
        <EditFilled
          className="table-action edit"
          onClick={onEdit(instance)}
        />
        <DeleteFilled
          className="table-action"
          onClick={onDelete(instance.id)}
        />
      </>
    )
  }
];

export default columns;
