export const MARKER_TYPE_SELECT = [
  { name: 'EMPLOYEE', label: 'Сотрудник' },
  { name: 'ITEM', label: 'Объект' },
  { name: 'VEHICLE', label: 'Транспортное средство' },
];

export const getMarkerLabel = marker => {
  const target = MARKER_TYPE_SELECT.find(item => item.name === marker);
  return target ? target.label : marker;
}
