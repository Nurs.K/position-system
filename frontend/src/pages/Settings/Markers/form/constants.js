export const MARKER_FORM = 'markerConfig';
export const CREATE_FORM = `${MARKER_FORM}_create`;
export const EDIT_FORM = `${MARKER_FORM}_edit`;