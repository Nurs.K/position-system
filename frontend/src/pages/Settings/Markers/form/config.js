import { MARKER_TYPE_SELECT } from '../utils';

const config = [
  {
    type: 'valueSet',
    selectType: 'raw',
    options: MARKER_TYPE_SELECT,
    name: 'marker_type',
    label: 'Тип маркера',
    required: true,
  },
  {
    type: 'file',
    name: 'icon',
    label: 'Иконка',
    accept: 'image/*'
  }
];

export default config;
