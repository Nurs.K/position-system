import React, { useEffect, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {Avatar, Button, Modal, Table} from "antd";
import {DeleteFilled, EditFilled, PlusOutlined} from "@ant-design/icons";

import {
  getListRequest,
  setIsCreateFormOpen,
  setIsEditFormOpen,
  createRequest,
  updateRequest,
  askToDelete,
  deleteRequest,
} from '../../../store/actions/markerConfigurations';
import {
  selectMarkerConfigList,
  selectMarkerConfigListFetching,
  selectIsCreateFormOpen,
  selectIsEditFormOpen,
  selectCreationLoading,
  selectEditingLoading,
  selectNominatedToRemoveItem,
  selectDeletingFetching,
  selectCreationError,
  selectMarkerTableData
} from '../../../store/selectors/markerConfigurations';

import { initForm, setFormInitialValues } from '../../../store/actions/forms';
import { selectFormValues } from '../../../store/selectors/forms';

import FormDialog from '../../../components/SettingsFormDialog';

import SettingsLayout from "../SettingsLayout";

import config from './form/config';
import submit from './form/submit';
import { getMarkerLabel } from './utils';
import { CREATE_FORM, EDIT_FORM } from './form/constants';
import {UPLOAD_MAP} from "../../../consts/urls";


const Markers = ({ history }) => {
  const dispatch = useDispatch();

  const list = useSelector(selectMarkerConfigList);
  const listLoading = useSelector(selectMarkerConfigListFetching);

  const isCreateFormOpen = useSelector(selectIsCreateFormOpen);
  const isEditFormOpen = useSelector(selectIsEditFormOpen);
  const isCreationLoading = useSelector(selectCreationLoading);
  const isEditingLoading = useSelector(selectEditingLoading);
  const createFormValues = useSelector(selectFormValues(CREATE_FORM));
  const editFormValues = useSelector(selectFormValues(EDIT_FORM));
  const nominatedToRemoving = useSelector(selectNominatedToRemoveItem);
  const deletingLoading = useSelector(selectDeletingFetching);
  const createError = useSelector(selectCreationError);

  useEffect(
    () => {
      dispatch(getListRequest());
      dispatch(initForm({ form: CREATE_FORM, config }));
      dispatch(initForm({ form: EDIT_FORM, config }));
    },
    // eslint-disable-next-line
    []
  );

  const handleCreateFormOpen = () => {
    dispatch(setIsCreateFormOpen(true));
  };

  const handleEditFormOpen = instance => e => {
    e.preventDefault();
    dispatch(setFormInitialValues({ form: EDIT_FORM, instance }));
    dispatch(setIsEditFormOpen(true));
  };

  const handleCreateSubmit = useCallback(
    () => {
      const data = submit(createFormValues);

      if (Array.from(data.keys()).length) {
        dispatch(createRequest({ data }));
      }
    },
    [createFormValues, dispatch]
  );

  const handleEditSubmit = useCallback(
    () => {
      const { id, ...rest } = editFormValues;
      const instance = list.find(item => item.id === id);
      const data = submit(rest, instance);

      if (Array.from(data.keys()).length) {
        dispatch(updateRequest({ id, data }));
      }
    },
    [editFormValues, dispatch, list]
  );

  const handleAskToDelete = id => () => {
    dispatch(askToDelete(id));
  };

  const handleDelete = id => () => {
    dispatch(deleteRequest({ id }));
  };

  const columns = [
    {
      title: 'Тип метки',
      dataIndex: 'marker_type',
      key: 'marker_type',
      render: type => getMarkerLabel(type),
    },
    {
      title: 'Иконка',
      dataIndex: 'icon',
      key: 'icon',
      render: icon => (
        !icon
          ? null
          : (
            <Avatar
              src={icon}
            />
          )
      )
    },
    {
      title: 'Действия',
      dataIndex: 'instance',
      key: 'actions',
      render: instance => (
        <>
          <EditFilled
            className="table-action edit"
            onClick={handleEditFormOpen(instance)}
            style={{fontSize: 25}}
          />
          <DeleteFilled
            className="table-action"
            onClick={handleAskToDelete(instance.id)}
            style={{fontSize: 25}}
          />
        </>
      )
    }
  ];

  const data = useSelector(selectMarkerTableData(columns));

  return (
    <SettingsLayout history={history}>
      <div className="table-wrapper">
        <div className="btn-create-wrapper">
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleCreateFormOpen}
          >
            Добавить
          </Button>
        </div>
        <Table
          loading={listLoading}
          columns={columns}
          dataSource={data}
          pagination={false}
        />
      </div>

      <FormDialog
        title="Добавить конфигурацию"
        visible={isCreateFormOpen}
        onClose={() => dispatch(setIsCreateFormOpen(false))}
        buttonText={isCreationLoading ? 'Загрузка...' : 'Создать'}
        form={CREATE_FORM}
        handleSubmit={handleCreateSubmit}
        loading={isCreationLoading}
        config={config}
        error={createError}
      />
      <FormDialog
        title="Редактировать конфигурацию"
        visible={isEditFormOpen}
        onClose={() => dispatch(setIsEditFormOpen(false))}
        buttonText={isEditingLoading ? 'Загрузка...' : 'Редактировать'}
        form={EDIT_FORM}
        handleSubmit={handleEditSubmit}
        loading={isEditingLoading}
        config={config}
      />
      <Modal
        visible={Boolean(nominatedToRemoving)}
        onCancel={() => dispatch(askToDelete(null))}
        title="Подтвердите действие"
        okText={deletingLoading ? 'Загрузка...' : 'Удалить'}
        okButtonProps={{ color: 'red', loading: deletingLoading }}
        cancelText="Нет"
        onOk={handleDelete(nominatedToRemoving)}
      >
        <p>Вы действительно хотите удалить конфигурацию?</p>
      </Modal>
    </SettingsLayout>
  )
};

export default React.memo(Markers);
