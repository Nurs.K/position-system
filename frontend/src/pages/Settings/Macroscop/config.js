
const config = [
  {
    type: 'text',
    name: 'login',
    label: 'Логин',
    required: true,
  },
  {
    type: 'text',
    name: 'password',
    label: 'Пароль',
    required: true,
  },
  {
    type: 'text',
    name: 'width',
    label: 'Ширина',
  },
  {
    type: 'text',
    name: 'height',
    label: 'Высота',
  },
];

export default config;
