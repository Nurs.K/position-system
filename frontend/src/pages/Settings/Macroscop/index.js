import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Input, Form, Typography } from 'antd';
import { EditFilled } from "@ant-design/icons";

import FormDialog from '../../../components/SettingsFormDialog';

import {
  makeSelectConfigData,
  makeSelectIsLoading,
  makeSelectIsEditFormOpen,
  makeSelectUpdatingLoading,
} from '../../../store/selectors/cameras';
import {
  getConfigRequest,
  setIsEditFormOpen,
  updateConfigRequest
} from '../../../store/actions/cameras';
import { selectFormValues } from '../../../store/selectors/forms';
import {
  initForm,
  removeForm,
  clearForm,
  setFormInitialValues
} from '../../../store/actions/forms';

import { safeGet } from '../../../helpers/getters';

import SettingsLayout from '../SettingsLayout';

import config from './config';
import './styles.scss';


const FORM = 'macroscop';

const Macroscop = ({ history }) => {
  const dispatch = useDispatch();

  const isLoading = useSelector(makeSelectIsLoading);
  const data = useSelector(makeSelectConfigData);
  const isOpen = useSelector(makeSelectIsEditFormOpen);

  const isUpdating = useSelector(makeSelectUpdatingLoading);
  const editFormValues = useSelector(selectFormValues(FORM));

  const convertPassword = password => {
    if (!password) return '';
    return new Array(password.length).fill(null).map(() => '*').join('');
  };

  useEffect(
    () => {
      dispatch(getConfigRequest());
      dispatch(initForm({ form: FORM, config }));

      return () => {
        dispatch(removeForm({ form: FORM }));
      };
    },
    // Need to call to this effect only once
    // eslint-disable-next-line
    [],
  );

  const handleOpenEditForm = () => {
    dispatch(setFormInitialValues({ form: FORM, instance: data }));
    dispatch(setIsEditFormOpen(true));
  };

  const handleCloseEditForm = () => {
    dispatch(setIsEditFormOpen(false));
    dispatch(clearForm({ form: FORM }));
  };

  const handleUpdateConfigRequest = () => {
    dispatch(updateConfigRequest({ data: editFormValues }));
  };

  return (
    <SettingsLayout history={history}>
      <Typography.Title className="m-config-title">Настройки подключения Macroscop</Typography.Title>
      <Form
        labelCol={{ span: 2 }}
        wrapperCol={{ span: 20 }}
        className="m-config-wrapper"
      >
        <Form.Item className="m-config-item" label="Логин">
          <Input
            type="text"
            readOnly
            className="m-config-value"
            value={safeGet(data, 'login', '')}
            disabled={isLoading}
          />
        </Form.Item>
        <Form.Item className="m-config-item" label="Пароль">
          <Input
            type="text"
            readOnly
            className="m-config-value"
            value={convertPassword(safeGet(data, 'password'))}
            disabled={isLoading}
          />
        </Form.Item>
        <Form.Item className="m-config-item" label="Ширина">
          <Input
            type="text"
            readOnly
            className="m-config-value"
            value={safeGet(data, 'width')}
            disabled={isLoading}
          />
        </Form.Item>
        <Form.Item className="m-config-item" label="Высота">
          <Input
            type="text"
            readOnly
            className="m-config-value"
            value={safeGet(data, 'height')}
            disabled={isLoading}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 2, span: 20 }}>
          <Button
            type="primary"
            icon={<EditFilled />}
            disabled={isLoading}
            onClick={handleOpenEditForm}
          >
            {isLoading ? 'Загрузка...' : 'Редактировать'}
          </Button>
        </Form.Item>
      </Form>

      <FormDialog
        title="Редактировать настройки Macroscop"
        visible={isOpen}
        onClose={handleCloseEditForm}
        form={FORM}
        config={config}
        buttonText={isUpdating ? 'Загрузка...' : 'Сохранить'}
        loading={isUpdating}
        handleSubmit={handleUpdateConfigRequest}
      />
    </SettingsLayout>
  );
};

export default Macroscop;
