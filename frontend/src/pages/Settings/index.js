import React from 'react';
import { Redirect } from 'react-router-dom';

import { SETTINGS_CATEGORIES } from '../../consts/urls';


export default function SettingsPage() {
  return <Redirect to={SETTINGS_CATEGORIES} />;
}
