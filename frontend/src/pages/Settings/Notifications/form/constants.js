export const NOTIFICATION_CONFIG = 'notificationConfig';
export const CREATE_FORM = `${NOTIFICATION_CONFIG}_create`;
export const EDIT_FORM = `${NOTIFICATION_CONFIG}_edit`;