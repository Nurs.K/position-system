import { selectEventTypes, selectEventTypesFetching } from "../../../../store/selectors/notificationConfigSelectors";

const config = [
  {
    type: 'valueSet',
    name: 'event_type',
    label: 'Тип события',
    required: true,
    getValueSet: selectEventTypes,
    getValueSetLoading: selectEventTypesFetching,
  },
  {
    type: 'text',
    name: 'title',
    label: 'Наименование'
  },
  {
    type: 'textarea',
    name: 'description',
    label: 'Описание',
    rows: 3,
  },
  {
    type: 'file',
    name: 'audio',
    label: 'Звук',
    accept: 'audio/*',
  },
  {
    type: 'file',
    name: 'thumbnail',
    label: 'Изображение',
    accept: 'image/*'
  }
];

export default config;
