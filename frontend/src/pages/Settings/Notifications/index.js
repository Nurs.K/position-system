import React, { useEffect, useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DeleteFilled, EditFilled, PlusOutlined } from "@ant-design/icons";
import {Avatar, Button, Modal, Table} from "antd";

import {
  getListRequest,
  getEventTypeRequest,
  setIsEditFormOpen,
  setIsCreateFormOpen,
  createRequest,
  openEditForm,
  editRequest,
  askToDelete,
  deleteRequest,
} from '../../../store/actions/notificationConfigurations';
import {
  selectConfigList,
  selectIsCreateFormOpen,
  selectIsEditFormOpen,
  selectEditingFetching,
  selectCreationFetching,
  selectNominatedToRemoveItem,
  selectDeletingFetching,
  selectConfigListCount,
  selectConfigTableData,
  selectConfigListFetching,
} from '../../../store/selectors/notificationConfigSelectors';
import { initForm } from '../../../store/actions/forms';
import { selectFormValues } from '../../../store/selectors/forms';

import FormDialog from '../../../components/SettingsFormDialog';
import { DEFAULT_PAGE_SIZE } from '../../../components/Pagination/constants';
import { getQuery } from '../../../helpers/query';

import submit from './form/submit';
import config from './form/config';
import { getEventTypeLabel } from './utils';
import SettingsLayout from '../SettingsLayout';

import { CREATE_FORM, EDIT_FORM } from './form/constants';
import {MIN_PAGINATION_ITEMS} from "../consts";


const NotificationConfiguration = ({ history }) => {
  const dispatch = useDispatch();

  const count = useSelector(selectConfigListCount);
  const [page, setPage] = useState(getQuery().page || 1);
  const [size, setSize] = useState(getQuery().size || DEFAULT_PAGE_SIZE);

  const isCreateFormOpen = useSelector(selectIsCreateFormOpen);
  const isEditFormOpen = useSelector(selectIsEditFormOpen);
  const nominatedToRemoving = useSelector(selectNominatedToRemoveItem);
  const list = useSelector(selectConfigList);
  const loading = useSelector(selectConfigListFetching);
  const createFormValues = useSelector(selectFormValues(CREATE_FORM));
  const editFormValues = useSelector(selectFormValues(EDIT_FORM));

  const creationLoading = useSelector(selectCreationFetching);
  const editingLoading = useSelector(selectEditingFetching);
  const deletingLoading = useSelector(selectDeletingFetching);

  useEffect(
    () => {
      getList();
      dispatch(getEventTypeRequest());
      dispatch(initForm({ form: CREATE_FORM, config }));
      dispatch(initForm({ form: EDIT_FORM, config }));

      return () => {
        dispatch(setIsCreateFormOpen(false));
        dispatch(setIsEditFormOpen(false));
        dispatch(askToDelete(null));
      }
    },
    // Need to call this only once at render
    // eslint-disable-next-line
    []
  );

  const updateUrl = () => {
    const params = new URLSearchParams();
    params.append('page', page.toString());
    params.append('size', size.toString());
    history.push({ search: `?${params.toString()}` });
  };

  const getList = () => {
    dispatch(getListRequest({ page, size }));
    updateUrl();
  };

  const handleClickCreate = () => {
    dispatch(setIsCreateFormOpen(true));
  };

  const handleCreateSubmit = useCallback(
    () => {
      const formData = submit(createFormValues);

      if (Array.from(formData.keys()).length) {
        dispatch(createRequest({ data: formData }));
      }
    },
    [createFormValues, dispatch]
  );
  const handleEditSubmit = useCallback(
    () => {
      const { id, ...rest } = editFormValues;
      const instance = list.find(item => item.id === id);
      const formData = submit(rest, instance);

      if (Array.from(formData.keys()).length) {
        dispatch(editRequest({ id, data: formData }))
      }
    },
    [editFormValues, dispatch, list]
  );

  const handleEditButtonClick = instance => e => {
    e.preventDefault();

    dispatch(openEditForm({ instance }));
  };

  const handleRemoveNotification = id => e => {
    e.preventDefault();
    dispatch(deleteRequest({ id }));
  };

  const askToRemoveNotification = id => e => {
    e.preventDefault();
    dispatch(askToDelete(id));
  };

  const handlePaginationChange = (page, size) => {
    setPage(page);
    setSize(size);
  };

  useEffect(
    () => {
      getList();
    },
    // eslint-disable-next-line
    [page, size],
  );

  const columns = [
    {
      title: 'Изображение',
      dataIndex: 'thumbnail',
      key: 'thumbnail',
      render: thumbnail => (
        !thumbnail
          ? null
          : (
            <Avatar
              src={thumbnail}
            />
          )
      )
    },
    {
      title: 'Тип события',
      dataIndex: 'event_type',
      key: 'event_type',
      render: type => getEventTypeLabel(type),
    },
    {
      title: 'Название',
      dataIndex: 'title',
      key: 'name',
    },
    {
      title: 'Описание',
      dataIndex: 'description',
      key: 'description',
    },
    {
      title: 'Действия',
      dataIndex: 'instance',
      key: 'actions',
      render: instance => (
        <>
          <EditFilled
            className="table-action edit"
            onClick={handleEditButtonClick(instance)}
            style={{fontSize: 25}}
          />
          <DeleteFilled
            className="table-action"
            onClick={askToRemoveNotification(instance.id)}
            style={{fontSize: 25}}
          />
        </>
      )
    }
  ];

  const data = useSelector(selectConfigTableData(columns));

  return (
    <SettingsLayout history={history}>
      <div className="table-wrapper">
        <div className="btn-create-wrapper">
          <Button
            type="primary"
            icon={<PlusOutlined />}
            onClick={handleClickCreate}
          >
            Добавить
          </Button>
        </div>
        <Table
          columns={columns}
          loading={loading}
          dataSource={data}
          pagination={data.length < MIN_PAGINATION_ITEMS ? false :{
            total: count,
            current: page,
            pageSize: size,
            onChange: handlePaginationChange,
            hideOnSinglePage: false,
            showSizeChanger: true,
            position: ['bottomLeft'],
          }}
        />
      </div>

      <FormDialog
        title="Создание конфигурации"
        visible={isCreateFormOpen}
        onClose={() => dispatch(setIsCreateFormOpen(false))}
        buttonText={creationLoading ? 'Загрузка...' : 'Создать'}
        form={CREATE_FORM}
        handleSubmit={handleCreateSubmit}
        loading={creationLoading}
        config={config}
      />
      <FormDialog
        title="Редактировать конфигурацию"
        visible={isEditFormOpen}
        onClose={() => dispatch(setIsEditFormOpen(false))}
        buttonText={editingLoading ? 'Загрузка...' : 'Редактировать'}
        form={EDIT_FORM}
        handleSubmit={handleEditSubmit}
        loading={editingLoading}
        config={config}
      />
      <Modal
        visible={Boolean(nominatedToRemoving)}
        onCancel={() => dispatch(askToDelete(null))}
        title="Подтвердите действие"
        okText={deletingLoading ? 'Загрузка...' : 'Удалить'}
        okButtonProps={{ color: 'red', loading: deletingLoading }}
        cancelText="Нет"
        onOk={handleRemoveNotification(nominatedToRemoving)}
      >
        <p>Вы действительно хотите удалить конфигурацию?</p>
      </Modal>
    </SettingsLayout>
  )
};

export default React.memo(NotificationConfiguration);
