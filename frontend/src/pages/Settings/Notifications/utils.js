
export const eventTypeNameMapper = {
  PARKING_START: 'Парковка (начало)',
  PARKING_END: 'Парковка (конец)',
  DANGER_ZONE_ENTER: 'Опасная зона (вход)',
  DANGER_ZONE_EXIT: 'Опасная зона (выход)',
  DANGER_ZONE_EMPTY: 'Опасная зона (пустая)',
  DANGER_ZONE_NONEMPTY: 'Опасная зона (не пустая)',
  BLINK: 'Сигнал от метки',
  AREA_ENTER: 'Области (вход)',
  AREA_EXIT: 'Области (выход)',
  SPEED_EXCEED: 'Превышение скорости',
  UNIDENTIFIED_ENTER: 'Доступ неизвестного объекта (вход)',
  ITV_COUNTER: 'ITV счетчик',
  EXPORT: 'Файл отчета создан',
  WRONG_ORIENTATION: 'Неправильное положение',
  WRONG_ORIENTATION_CANCEL: 'Неправильное положение (отмена)',
  MAN_DOWN: 'Человек без движения',
  MAN_DOWN_CANCEL: 'Человек без движения (отмена)',
  BATTERY_LOW: 'Батарея разряжена',
  NO_SIGNAL: 'Потеря сигнала',
  FREE_FALL: 'Падение',
  IMPACT: 'Удар',
  IN_DIG_ON: 'Цифр. Вх (вкл)',
  IN_DIG_OFF: 'Цифр. Вх (выкл)',
  BUTTON_PRESSED: 'Кнопка нажата',
  PAIRING_START: 'Сопряжение (начало)',
  PAIRING_END: 'Сопряжение (конец)',
  TAG_SIGNAL_REQUEST: 'Запрос на отправку сигнала на метку',
  SITE_ENTER: 'Включение модуля расчета координат',
  SITE_EXIT: 'Выключение модуля расчета координат',
  LE_UP: 'Включение модуля расчета координат',
  LE_DOWN: 'Выключение модуля расчета координат',
  ANCHOR_UP: 'Включение анкера',
  ANCHOR_DOWN: 'Выключение анкера',
};

export const getEventTypeLabel = name => {
  if (eventTypeNameMapper[name]) {
    return eventTypeNameMapper[name];
  }
  return name;
};

export const normalizeFields = config => config.reduce(
  (memo, curr) => {
    if (curr.type === 'file') {
      return Object.assign(memo, { [curr.name]: { file: null, url: null } });
    }
    if (curr.type === 'boolean') {
      return Object.assign(memo, { [curr.name]: Boolean(curr.defaultChecked) })
    }
    return Object.assign(memo, { [curr.name]: '' });
  },
  {},
);
