import React, {useEffect, useRef, useState} from 'react';
import { useHistory } from 'react-router-dom';
import {useDispatch, useSelector} from "react-redux";

import {
  initGlobalMap,
  clearMap,
  addPolygon,
  addColorToLayer,
  createColorLayer,
  zoomOutMap,
  zoomInMap,
} from '../../helpers/mapHelpers';
import {getKeepers} from "../../services/keepers";
import {mockKeeper} from '../../consts/mockData';

import StackDrawer from '../../components/MapButtons/StackDrawer';
import MapButtons from '../../components/MapButtons';
import UserDrawer from '../../components/UserDrawer';
import KeeperCounter from "../../components/Map/components/KeeperCounter";


import {
  mapCenter as position,
  shapes,
} from './fixtures';
import './index.scss';

import {makeSelectIsUserDrawerVisible} from "../../store/selectors/labels";
import {setIsUserDrawerVisible} from "../../store/actions/labels";
import {makeSelectGetUserColor} from "../../store/selectors/user";


const Home = () => {
  const mapId = 'home-global-map';

  const [map, setMap] = useState();
  const [colorLayer, setColorLayer] = useState('');
  const [isKeeperDrawerVisible, setIsKeeperDrawerVisible] = useState(false);
  const isUserDrawerVisible = useSelector(makeSelectIsUserDrawerVisible)
  const mapColor = useSelector(makeSelectGetUserColor)
  const [keepers, setKeepers] = useState('');
  const keepersInterval = useRef();
  const history = useHistory()

  const dispatch = useDispatch()

  const style = {
    fillColor: mapColor ? mapColor : "#fff",
    fillOpacity: '0.2',
  }

  const setIsUserSideDrawerVisible = () => {
    dispatch(setIsUserDrawerVisible())
  }

  const clearKeepersInterval = () => {
    clearInterval(keepersInterval.current);
  };

  const setKeepersInterval = () => {
    keepersInterval.current = setInterval(requestKeepers, 5000);
  };

  const zoomIn = () => {
    if(map){
      zoomInMap({map})
    }
  }

  const zoomOut = () => {
    if(map){
      zoomOutMap({map})
    }
  }

  const requestKeepers = () => {
    getKeepers({
      embed: 'tag',
      filter: '[deleted==false;name=@;type==EMPLOYEE]',
      sort: 'name',
    })
      .then((res) => setKeepers(res?.data?.items))
      .catch((err) => {
        clearKeepersInterval();
        throw err;
      });
  };

  const handleShapeClick = () => {
    history.push('/sandwich')
  }


  useEffect(
    () => {
      const _map = initGlobalMap({mapId, position});
      setMap(_map);

      return () => {
        clearMap(mapId);
        setMap(undefined);
      };
    },
    // eslint-disable-next-line
    []
  );

  useEffect(() => {
    if (map) {
      if (!colorLayer) {
        createColorLayer({map, style, setColorLayer})
      } else {
        const color = mapColor ? mapColor : "#fff"
        addColorToLayer({mapColor: color, colorLayer});
      }
    }
  }, [mapColor, colorLayer])

  useEffect(
    () => {
      if (map) {
        if(!colorLayer){
          createColorLayer({map, style, setColorLayer})
        }

        for (const item of shapes) {
          addPolygon({
            map,
            coords: item.shape,
            options: {
              color: item.color,
              style: {
                zIndex: 500,
              }
            },
            handleShapeClick: handleShapeClick,
            tooltip: {
              text: item.name
            },
          });
        }
      }
    },
    [map],
  )

  useEffect(() => {
    requestKeepers();
    setKeepersInterval();
    return () => {
      clearKeepersInterval();
    };
  }, [])

  return (
    <React.Fragment>
      <div className="home-page-wrapper">
        <MapButtons
          showMapSettings={() => setIsKeeperDrawerVisible(true)}
          showFloors={false}
          zoomIn={zoomIn}
          zoomOut={zoomOut}
        />
        <div className="map-holder" id={mapId}/>
      </div>
      {!!keepers?.length && <KeeperCounter amount={keepers.length}/>}
      <UserDrawer
        visible={isUserDrawerVisible}
        keeper={mockKeeper}
        onClose={() => setIsUserSideDrawerVisible()}
      />
      <StackDrawer
        visible={isKeeperDrawerVisible}
        setVisible={setIsKeeperDrawerVisible}
        map={map}
      />
    </React.Fragment>
  );
};

export default React.memo(Home);
