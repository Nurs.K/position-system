import React, { useEffect, useState } from 'react';
import {useDispatch, useSelector} from 'react-redux';

// import BackIcon from '../../assets/icons/svg/BackIcon';
// import NextIcon from '../../assets/icons/svg/NextIcon';

import { getKeepers } from '../../services/keepers';
import {groupMapsByCategories, sortSandwichesByOrder} from './utils';

import { categoriesRequest } from '../../store/actions/categories';
import { getMapsListRequest } from '../../store/actions/svgMap';
import {makeSelectGetMapsList} from "../../store/selectors/svgMap";
import {makeSelectCategoryList} from "../../store/selectors/categories";
import {makeSelectKeepersData} from "../../store/selectors/keepers";

import SandwichColumn from './SandwichColumn';
import {DEFAULT_SIZE} from "./constants";

const SandwichPage = () => {
  const [sandwhichList, setSandwichList] = useState();
  const keepers = useSelector(makeSelectKeepersData);
  const categoriesList = useSelector(makeSelectCategoryList)
  const mapsList = useSelector(makeSelectGetMapsList)

  const dispatch = useDispatch()

  const requestKeepers = () => {
    getKeepers({ embed: 'tag' });
  };

  useEffect(() => {
    dispatch(categoriesRequest())
    dispatch(getMapsListRequest({size: DEFAULT_SIZE}))
    requestKeepers();
    const keepersInterval = setInterval(requestKeepers, 3000);
    return () => {
      clearInterval(keepersInterval);
    };
  }, []);

  useEffect(() => {
    if (keepers.length) {
      // const deepClone = JSON.parse(JSON.stringify(sandwhichList));
      // setSandwichList(updateSandwichAmount(keepers, deepClone));
    }
  }, [keepers]);

  useEffect(() => {
    if (categoriesList && mapsList) {
      const list = groupMapsByCategories(mapsList, categoriesList);
      const sorted = sortSandwichesByOrder(list)
      setSandwichList(sorted);
    }
  }, [categoriesList, mapsList]);

  return (
    <div className="content-block block-gallery-sandwich">
      <div className="block-gallery-wrapper">
        <div className="block-gallery-inner-wrapper">
          {sandwhichList &&
            !!sandwhichList.length &&
            sandwhichList.map((item, index) => {
              return <SandwichColumn key={index} data={item} />;
            })}
        </div>
      </div>
    </div>
  );
};

export default SandwichPage;
