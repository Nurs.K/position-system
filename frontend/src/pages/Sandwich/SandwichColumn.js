import React from 'react';
import { Link } from 'react-router-dom';

import { toShowMapId } from '../../consts/routes';

export default function SandwichColumn({ data: { name, thumbnail, list } }) {
  return (
    <div className="block-gallery">
      {list &&
        !!list.length ?
        list.map((item, index) => {
          return (
            <Link to={toShowMapId(item.id)} key={index}>
              <div className={`block-${item.blockColor || 'white'}`}>
                <h4
                  className={`block-${
                    item.blockColor !== 'white' ? 'white' : 'red'
                  }-text`}
                >
                  {item?.amount || 0}
                </h4>
                <div
                  className="block-red-inner"
                  style={{
                    backgroundImage: `url(${thumbnail})`,
                  }}
                >
                  <h4 className="block-inner-text">
                    {item.height && item.height + ' - '}
                    {item.name}
                  </h4>
                </div>
              </div>
            </Link>
          );
        }) : (
          <div className="sandwich-no-items">Пока карты не добавлены в эту категорию</div>
        )}
      <h2 className="block-text">{name}</h2>
    </div>
  );
}
