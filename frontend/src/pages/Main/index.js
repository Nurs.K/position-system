import React, {useState} from 'react';
import { connect } from 'react-redux';
import MapComponent from './MapComponent';
import CRSComponent from './CRSComponent';

import './index.css';
import MapButtons from '../../components/MapButtons';
import StackDrawer from "../../components/MapButtons/StackDrawer";

function Main({ mapType }) {
  const [isStackDrawerOpen, setIsStackDrawerOpen] = useState(false);
  function mapRender(mapType) {
    switch (mapType) {
      case 'MAP':
        return <MapComponent />;
      case 'BUILDING':
        return <>BUILDING</>;
      case 'PLAN':
        return <CRSComponent />;
      default:
        return <MapComponent />;
    }
  }

  return (
    <>
      <div className="map-wrapper">{mapRender(mapType)}</div>
      <MapButtons
        showMapSettings={() => setIsStackDrawerOpen(true)}
      />
      <StackDrawer
        visible={isStackDrawerOpen}
        setVisible={setIsStackDrawerOpen}
      />
    </>
  );
}

function mapStateToProps(state) {
  return {
    mapType: state.mapType.mapType,
  };
}

export default connect(mapStateToProps)(Main);
