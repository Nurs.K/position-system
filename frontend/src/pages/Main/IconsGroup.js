import React from 'react';
import { Tooltip} from 'antd';

import { ReactComponent as DefaultLogo } from '../../assets/icons/defaultlogo.svg';
import { ReactComponent as AimLogo } from '../../assets/icons/aim.svg';
import { ReactComponent as SosLogo } from '../../assets/icons/sos-medium.svg';
import { ReactComponent as CameraLogo } from '../../assets/icons/camera-large.svg';
import { ReactComponent as ClockLogo } from '../../assets/icons/clock.svg';

import './index.css';

function IconsGroup({ onShowSosModal, onClickClock, handleClickAim }) {
  const logos = [
    {
      id: 1,
      icon: <DefaultLogo />,
      text: 'Default text'
    },
    {
      id: 2,
      text: 'На карте',
      icon: <AimLogo onClick={handleClickAim} />,
    },
    {
      id: 3,
      icon: (
        <SosLogo
          onClick={() => {
            onShowSosModal();
          }}
        />
      ),
      text: 'Срочно'
    },
    {
      id: 4,
      icon: <CameraLogo />,
      text: 'Видеонаблюдение'
    },
    {
      id: 5,
      icon: (
        <ClockLogo
          onClick={() => {
            onClickClock();
          }}
        />
      ),
      text: 'История перемещений'
    },
  ];

  return (
    <div className="logos-wrapper">
      {logos.map((item) => {
        return (
          <Tooltip title={item.text}>
            <span key={item.id} onClick={() => {}}>
              {item.icon}
            </span>
          </Tooltip>
        );
      })}
    </div>
  );
}

export default IconsGroup;

// function mapDispatchToProps(dispatch) {
//   return {
//     changeMapTypes: (type) => dispatch({ type }),
//   };
// }

// export default connect(null, mapDispatchToProps)(MapTypes);
