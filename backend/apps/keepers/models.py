from django.contrib.postgres.fields import JSONField
from django.db import models


class Notification(models.Model):
    items = JSONField(default=dict, blank=True, null=True)

    def __str__(self):
        return f"{self.id}"
