import datetime

import requests

from Positions.celery import app
from utils.send_request import send_get_request
from apps.keepers.models import Notification


def auth():
    responce = requests.post(url='http://135.181.80.181/api/v1/auth/sign-in/', data={
        "username": "system",
        "password": "admin"
    })
    return responce.json()

@app.task
def spam_email():
    user_auth = auth()
    current_data = datetime.datetime.now().strftime('%Y-%m-%d')
    next_data = datetime.datetime.today() + datetime.timedelta(days=1)
    new_next_data = str(next_data).split(" ")
    data = send_get_request('system', 'admin',
                            f'v0.1/reports/events?offset=0&limit=10&filter=%5Bstart_time%3E%3D{current_data}T00%3A00%3A00.0%2B03%3A00%3Bend_time%3C%3D{new_next_data[0]}T00%3A00%3A00.0%2B03%3A00%5D',
                            user_auth['access'],
                            user_auth['cookies'])
    not_data = Notification.objects.get_or_create(items=data)
    return "Notification is created"


