import datetime

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework import views, viewsets
from rest_framework.response import Response
from apps.keepers.serializers import NotificationSerializer
from apps.keepers.models import Notification

from utils.send_request import send_get_request, send_post_request


class KeeperAPIView(views.APIView):
    offset_param = openapi.Parameter('offset', openapi.TYPE_INTEGER, description="start list position",
                                     type=openapi.TYPE_INTEGER)
    limit_param = openapi.Parameter('limit', openapi.TYPE_INTEGER, description="max items count",
                                    type=openapi.TYPE_INTEGER)
    filter_param = openapi.Parameter('filter', openapi.TYPE_STRING, description="",
                                     type=openapi.TYPE_STRING)
    fields_param = openapi.Parameter('fields_', openapi.TYPE_STRING, description="return only selected fields",
                                     type=openapi.TYPE_STRING)
    sorting_param = openapi.Parameter('sorting', openapi.TYPE_STRING, description="",
                                      type=openapi.TYPE_STRING)
    embed_param = openapi.Parameter('embed', openapi.TYPE_STRING,
                                    description="return selected reference as embedded objects instead of links",
                                    type=openapi.TYPE_STRING)

    @swagger_auto_schema(manual_parameters=[
        offset_param, limit_param, filter_param, fields_param, sorting_param, embed_param],
        responses={200: "Keepers get success"},
        operation_summary='Get all created keepers (employees, items, vehicles)')
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request(
            'system', 'admin', 'v0.1/keepers', token, cookie
        )

        return Response(data.json())


class KeeperWithFilterAPIView(views.APIView):
    offset_param = openapi.Parameter('offset', openapi.TYPE_INTEGER, description="start list position",
                                     type=openapi.TYPE_INTEGER)
    limit_param = openapi.Parameter('limit', openapi.TYPE_INTEGER, description="max items count",
                                    type=openapi.TYPE_INTEGER)
    filter_param = openapi.Parameter('filter', openapi.TYPE_STRING, description="",
                                     type=openapi.TYPE_STRING)
    fields_param = openapi.Parameter('fields_', openapi.TYPE_STRING, description="return only selected fields",
                                     type=openapi.TYPE_STRING)
    sorting_param = openapi.Parameter('sorting', openapi.TYPE_STRING, description="",
                                      type=openapi.TYPE_STRING)
    embed_param = openapi.Parameter('embed', openapi.TYPE_STRING,
                                    description="return selected reference as embedded objects instead of links",
                                    type=openapi.TYPE_STRING)

    @swagger_auto_schema(manual_parameters=[
        offset_param, limit_param, filter_param, fields_param, sorting_param, embed_param],
        responses={200: "Keeper With Filter get success"},
        operation_summary='Get all created keepers (employees, items, vehicles)')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request(
            'system', 'admin', f'v0.1/keepers?{filter_params}', token, cookie
        )

        return Response(data.json())


class LabelsAPIView(views.APIView):
    offset_param = openapi.Parameter('offset', openapi.TYPE_INTEGER, description="start list position",
                                     type=openapi.TYPE_INTEGER)
    limit_param = openapi.Parameter('limit', openapi.TYPE_INTEGER, description="max items count",
                                    type=openapi.TYPE_INTEGER)
    filter_param = openapi.Parameter('filter', openapi.TYPE_STRING, description="",
                                     type=openapi.TYPE_STRING)
    fields_param = openapi.Parameter('fields_', openapi.TYPE_STRING, description="return only selected fields",
                                     type=openapi.TYPE_STRING)
    sorting_param = openapi.Parameter('sorting', openapi.TYPE_STRING, description="",
                                      type=openapi.TYPE_STRING)
    embed_param = openapi.Parameter('embed', openapi.TYPE_STRING,
                                    description="return selected reference as embedded objects instead of links",
                                    type=openapi.TYPE_STRING)

    @swagger_auto_schema(manual_parameters=[
        offset_param, limit_param, filter_param, fields_param, sorting_param, embed_param],
        responses={200: "Labels get success"},
        operation_summary='Get all created lables (labels are created automatically on first member that use label)')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/labels',
                                token,
                                cookie)
        return Response(data.json())


class TagAggregateAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Tag Aggregate get success"},
                         operation_summary='Get how many tags are in each possible tag state')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/reports/tags/aggregated',
                                token,
                                cookie)

        return Response(data.json())


class ConfigAggregateAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Config Aggregate get success"},
                         operation_summary='Returns how many objects of different types (maps, tags, keepers, etc) are created in the system')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/config/aggregated',
                                token,
                                cookie)

        return Response(data.json())


class AnchorsAggregateAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Anchors Aggregate get success"},
                         operation_summary='Get how many anchors are in each possible anchor state')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/reports/anchors/aggregated',
                                token,
                                cookie)

        return Response(data.json())


class BatteryAggregateAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Battery Aggregate get success"},
                         operation_summary='Get how many keepers have specific battery state')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/reports/keepers/aggregated/battery',
                                token,
                                cookie)

        return Response(data.json())


class MapAggregateAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Map Aggregate get success"},
                         operation_summary='Get how many keepers present on each map (grouped by keeper type and map)')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/reports/keepers/aggregated/map?filter=deleted==false',
                                token,
                                cookie)

        return Response(data.json())


class AreaLogAggregateAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Area Logs Aggregate get success"},
                         operation_summary='Returns how much enter/exit events were detected by the system during requested period of time')
    def get(self, request, format=None):
        current_data = datetime.datetime.now().strftime('%Y-%m-%d')
        next_data = datetime.datetime.today() + datetime.timedelta(days=1)
        new_next_data = str(next_data).split(" ")
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/areas_log/aggregated?filter=%5Bstart_time%3E%3D{current_data}T00%3A00%3A00.0%2B03%3A00%3Bend_time%3C%3D{new_next_data[0]}T00%3A00%3A00.0%2B03%3A00%5D',
                                token,
                                cookie)

        return Response(data.json())


class MapsAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Maps Aggregate get success"},
                         operation_summary='Returns how much enter/exit events were detected by the system during requested period of time')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/maps?fields=id,name&filter=%5Btile!%3Dnull;deleted%3D%3Dfalse%5D&sort=z,name',
                                token,
                                cookie)

        return Response(data.json())


class ShiftsAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Shift get success"},
                         operation_summary='Get created shifts')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/shifts?filter=deleted%3D%3Dfalse&sort=name',
                                token,
                                cookie)

        return Response(data.json())


class EventAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Event get success"},
                         operation_summary='Get all event')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/events?filter=%5Btime%3E%3D2021-12-11T00:00:00.0%2B03:00;time%3C%3D2021-12-12T00:00:00.0%2B03:00%5D&limit=10&offset=0&sort=-time',
                                token,
                                cookie)

        return Response(data.json())


class AuditAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Audit get success"},
                         operation_summary='Get list of HTTP sessions created by users')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/audit?offset=0&limit=10&filter=%5Blogin_time%3E%3D2021-07-21T00%3A00%3A00.0%2B03%3A00%3Blogout_time%3C%3D2021-07-22T00%3A00%3A00.0%2B03%3A00%5D',
                                token,
                                cookie)

        return Response(data.json())


class TagHistoryAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "TagHistory get success"},
                         operation_summary='Get when tag was given to keeper')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/tags_history?offset=0&limit=10&filter=%5Bstart_time%3E%3D2021-07-21T00%3A00%3A00.0%2B03%3A00%3Bend_time%3C%3D2021-07-22T00%3A00%3A00.0%2B03%3A00%5D',
                                token,
                                cookie)

        return Response(data.json())


class Site_LOG_APIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Sites logs summary get success"},
                         operation_summary='Get when tag was given to keeper')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/sites_log_summary?filter=%5Bstart_time%3E%3D2021-12-11T00:00:00.0%2B03:00;end_time%3C%3D2021-12-12T00:00:00.0%2B03:00%5D&limit=10&offset=0&sort=keeper,site',
                                token,
                                cookie)

        return Response(data.json())


class AreaLogAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "AreaLog logs summary get success"},
                         operation_summary='Each row contains information abought a single area visit (when keeper entered and exited area, how long they were in the area etc)')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/areas_log?filter=%5Bstart_time%3E%3D2021-12-11T00:00:00.0%2B03:00;end_time%3C%3D2021-12-12T00:00:00.0%2B03:00%5D&limit=10&offset=0&sort=start_time',
                                token,
                                cookie)

        return Response(data.json())


class ActivityOnSiteAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "ActivityOnSite summary get success"},
                         operation_summary='The same as /reports/activity, but with tracking status')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/activity_onsite?filter=%5Bstart_time%3E%3D2021-12-11T00:00:00.0%2B03:00;end_time%3C%3D2021-12-12T00:00:00.0%2B03:00%5D&limit=10&offset=0&sort=keeper',
                                token,
                                cookie)

        return Response(data.json())


class ActivityAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Activity summary get success"},
                         operation_summary='Report of how long tag keepers were present on site, how long they were idle etc')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/reports/activity?filter=%5Bstart_time%3E%3D2021-12-11T00:00:00.0%2B03:00;end_time%3C%3D2021-12-12T00:00:00.0%2B03:00%5D&limit=10&offset=0&sort=keeper',
                                token,
                                cookie)

        return Response(data.json())


class NotificationsAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Notifications summary get success"},
                         operation_summary='request for events generated from last request')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                'v0.1/notifications',
                                token,
                                cookie)

        return Response(data.json())


class TagSignalAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Tag Signals summary get success"},
                         operation_summary='create TAG_SIGNAL_REQUEST event for tag linked with keeper')
    def post(self, request, keeperId: int, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_post_request('system', 'admin',
                                 f'v0.1/tag_signal/{keeperId}',
                                 token,
                                 cookie, )

        return Response(status=status.HTTP_201_CREATED)


class KeepersDetailAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Keepers Detail summary get success"},
                         operation_summary='Get keeper by ID')
    def get(self, request, id: int):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/keepers/{id}?embed=tag',
                                token,
                                cookie)

        return Response(data.json())


class ItemsWithFilterAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Items With Filter summary get success"},
                         operation_summary='Get created items')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/items?{filter_params}',
                                token,
                                cookie)
        return Response(data.json())


class ItemsAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Items summary get success"},
                         operation_summary='Get created items')
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/items', token, cookie)
        return Response(data.json())


class AreasAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Areas summary get success"},
                         operation_summary='Get created areas')
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/areas', token, cookie)
        return Response(data.json())


class AreasWithFilterAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Areas With Filter summary get success"},
                         operation_summary='Get created areas')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/areas?{filter_params}', token, cookie)
        return Response(data.json())


class EmployeesWithFilterAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Employees With Filter summary get success"},
                         operation_summary='Get created employees')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/employees?{filter_params}', token, cookie)
        return Response(data.json())


class EmployeesAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Employees summary get success"},
                         operation_summary='Get created employees')
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/employees', token, cookie)
        return Response(data.json())


class AnchorsAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Anchors summary get success"},
                         operation_summary='Get anchors list')
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/anchors', token, cookie)
        return Response(data.json())


class AnchorsWithFilterAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Anchors With Filter summary get success"},
                         operation_summary='Get anchors list')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/anchors?{filter_params}', token, cookie)
        return Response(data.json())


class UserWithFilterAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "User With Filter summary get success"},
                         operation_summary='Get created users')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/users?{filter_params}', token, cookie)
        return Response(data.json())


class UserAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "User summary get success"},
                         operation_summary='Get created users')
    def get(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/users', token, cookie)
        return Response(data.json())


class CustomNotificationAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Notification summary get success"},
                         operation_summary='Get created notification')
    def get(self, request, format=None):
        current_data = datetime.datetime.now().strftime('%Y-%m-%d')
        next_data = datetime.datetime.today() + datetime.timedelta(days=1)
        new_next_data = str(next_data).split(" ")
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/events?offset=0&limit=10&filter=%5Bstart_time%3E%3D{current_data}T00%3A00%3A00.0%2B03%3A00%3Bend_time%3C%3D{new_next_data[0]}T00%3A00%3A00.0%2B03%3A00%5D',
                                token,
                                cookie)
        return Response(data.json())


class TagAggregateFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Tag Aggregate Filter get success"},
                         operation_summary='Get how many tags are in each possible tag state')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/reports/tags/aggregated?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class ConfigAggregateFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Config Aggregate Filter get success"},
                         operation_summary='Returns how many objects of different types (maps, tags, keepers, etc) are created in the system')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/config/aggregated?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class AnchorsAggregateFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Anchors Aggregate Filter get success"},
                         operation_summary='Get how many anchors are in each possible anchor state')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/reports/anchors/aggregated?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class BatteryAggregateFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Battery Aggregate Filter get success"},
                         operation_summary='Get how many keepers have specific battery state')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/reports/keepers/aggregated/battery?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class MapAggregateFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Map Aggregate Filter get success"},
                         operation_summary='Get how many keepers present on each map (grouped by keeper type and map)')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/reports/keepers/aggregated/map?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class AreaLogAggregateFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Area Logs Aggregate Filter get success"},
                         operation_summary='Returns how much enter/exit events were detected by the system during requested period of time')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/areas_log/aggregated?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class TagHistoryFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "TagHistory Filter get success"},
                         operation_summary='Get when tag was given to keeper')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/tags_history?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class Site_LOG_FilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Sites logs Filter summary get success"},
                         operation_summary='Get when tag was given to keeper')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/sites_log_summary?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class AuditFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Audit Filter get success"},
                         operation_summary='Get list of HTTP sessions created by users')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/audit?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class ActivityFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Activity Filter summary get success"},
                         operation_summary='Report of how long tag keepers were present on site, how long they were idle etc')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/activity?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class EventFilterAPIVIew(views.APIView):
    @swagger_auto_schema(responses={200: "Event Filter get success"},
                         operation_summary='Get all event')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin',
                                f'v0.1/reports/events?{filter_params}',
                                token,
                                cookie)

        return Response(data.json())


class DictsAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Dicts get success"},
                         operation_summary='Get available server dictionary resources')
    def get(self, request, format=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', 'v0.1/dicts', token, cookie)
        return Response(data.json())


class DictsFilterAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Dicts Filter get success"},
                         operation_summary='Get available server dictionary resources')
    def get(self, request, filter_params=None):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/dicts?{filter_params}', token, cookie)
        return Response(data.json())


class DictsDetailAPIView(views.APIView):
    @swagger_auto_schema(responses={200: "Dicts Detail get success"},
                         operation_summary='Get available server dictionary resources')
    def get(self, request, event_type):
        token = request.META.get('HTTP_AUTHORIZATION', '')
        cookie = request.META.get('HTTP_JSESSIONID', '')
        data = send_get_request('system', 'admin', f'v0.1/dicts/{event_type}', token, cookie)
        return Response(data.json())


class NotificationAPIViewSet(viewsets.ModelViewSet):
    queryset = Notification.objects.all()
    serializer_class = NotificationSerializer
