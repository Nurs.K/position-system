from rest_framework import viewsets, generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.parsers import MultiPartParser

from .models import MapMarkerConfiguration, Device
from .serializers import MapMarkerConfigurationSerializer, DeviceSerializer, MacroscopConfigSerializer

# Create your views here.


class MapMarkerConfigurationViewSetAPIView(viewsets.ModelViewSet):
    serializer_class = MapMarkerConfigurationSerializer
    queryset = MapMarkerConfiguration.objects.all().order_by('-created_at')
    http_method_names = ['get', 'post', 'put', 'delete', 'options', 'head']
    parser_classes = (MultiPartParser,)
    # permission_classes = (IsAuthenticated,)

    def update(self, *args, **kwargs):
        return super(MapMarkerConfigurationViewSetAPIView, self).update(*args, **kwargs, partial=True)


class DeviceViewSetAPIView(viewsets.ModelViewSet):
    serializer_class = DeviceSerializer
    queryset = Device.objects.all().order_by('-created_at')
    http_method_names = ['get', 'post', 'put', 'delete', 'options', 'head']
    # permission_classes = (IsAuthenticated,)

    def update(self, *args, **kwargs):
        return super(DeviceViewSetAPIView, self).update(*args, **kwargs, partial=True)


class MacroscopConfigAPIView(generics.RetrieveAPIView, generics.CreateAPIView):
    serializer_class = MacroscopConfigSerializer
    # permission_classes = (IsAuthenticated,)

    def get_object(self):
        instance = self.serializer_class.Meta.model.objects.first()

        if not instance:
            instance = self.serializer_class.Meta.model.objects.create()

        return instance

    def perform_create(self, serializer):
        instance = self.get_object()
        update_serializer = self.serializer_class(instance=instance, data=serializer.validated_data, partial=True)
        update_serializer.is_valid(raise_exception=True)
        update_serializer.save()
