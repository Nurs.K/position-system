# Generated by Django 2.2.16 on 2021-10-25 14:38

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('common', '0002_device'),
    ]

    operations = [
        migrations.CreateModel(
            name='MacroscopCredentials',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Создано')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='Обновлено')),
                ('login', models.CharField(max_length=255)),
                ('password', models.CharField(max_length=255)),
                ('width', models.PositiveIntegerField(default=1024)),
                ('height', models.PositiveIntegerField(default=768)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
