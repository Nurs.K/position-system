import uuid

from django.db import models

from utils.files import SetUploadPath

from .constants import MARKER_TYPE_CHOICES
from .mixins import WithFileFieldModelMixin

# Create your models here.


class AbstractModel(models.Model):
    class Meta:
        abstract = True

    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, unique=True)
    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='Обновлено', auto_now=True, null=True)


class AbstractOrderModel(AbstractModel):
    class Meta:
        abstract = True
        ordering = ['order']

    order = models.PositiveIntegerField(default=0, blank=False, null=False)


class MapMarkerConfiguration(WithFileFieldModelMixin, AbstractModel):
    class Meta:
        verbose_name_plural = 'Конфигурации меток'
        verbose_name = 'Конфигурации меток'

    marker_type = models.CharField(max_length=50, choices=MARKER_TYPE_CHOICES)
    icon = models.FileField(upload_to=SetUploadPath('map-markers/'), null=True, blank=True)

    deleting_fields = ['icon']

    def __str__(self):
        return self.get_marker_type_display()


class Device(AbstractModel):
    class Meta:
        verbose_name = 'Устройство'
        verbose_name_plural = 'Устройства'

    name = models.CharField(max_length=255)
    status = models.BooleanField(default=False)

    def __str__(self):
        status = 'on' if self.status else 'off'
        return f'{self.name} (status: {status})'


class MacroscopConfig(AbstractModel):
    login = models.CharField(max_length=255, null=True, blank=True)
    password = models.CharField(max_length=255, null=True, blank=True)

    width = models.PositiveIntegerField(default=1024)
    height = models.PositiveIntegerField(default=768)

    def __str__(self):
        return 'Доступы от Macroscop'
