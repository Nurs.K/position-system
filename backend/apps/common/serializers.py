from rest_framework import serializers, exceptions

from .models import MapMarkerConfiguration, Device, MacroscopConfig


class MapMarkerConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = MapMarkerConfiguration
        fields = ['id', 'marker_type', 'icon']
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }

    def validate_marker_type(self, value):
        if MapMarkerConfiguration.objects.filter(marker_type=value).exists():
            raise exceptions.ValidationError(detail='Конфигурация на этот тип уже существует')
        return value


class DeviceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Device
        fields = ['id', 'name', 'status']
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }


class MacroscopConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = MacroscopConfig
        fields = ['login', 'password', 'width', 'height']
