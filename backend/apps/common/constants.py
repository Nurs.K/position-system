EMPLOYEE = 'EMPLOYEE'
ITEM = 'ITEM'
VEHICLE = 'VEHICLE'

MARKER_TYPE_CHOICES = (
    (EMPLOYEE, 'Сотрудник'),
    (ITEM, 'Объект'),
    (VEHICLE, 'Транспортное средство')
)
