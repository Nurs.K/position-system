class WithFileFieldModelMixin:
    deleting_fields = []

    def delete(self, *args, **kwargs):
        for field in self.deleting_fields:
            file = getattr(self, field)

            if not file:
                continue

            if not file.storage.exists(file.name):
                continue

            file.storage.delete(file.name)

        return super().delete(args, kwargs)

    def get_old_file(self, field):
        if not self.pk:
            return None

        try:
            instance = self.__class__.objects.get(pk=self.pk)
            old_file = getattr(instance, field)
        except self.DoesNotExist:
            return None

        return old_file

    def save(self, *args, **kwargs):
        if self.pk:
            for field in self.deleting_fields:
                old_file = self.get_old_file(field)

                if old_file and old_file != getattr(self, field):
                    if old_file.storage.exists(old_file.name):
                        old_file.storage.delete(old_file.name)

        return super().save(*args, **kwargs)
