from django.utils.deconstruct import deconstructible


@deconstructible
class AutoDeleteFileOnDelete:

    def __init__(self, *field_names):
        self.field_names = field_names

    def __call__(self, sender, instance, **kwargs):
        for field in self.field_names:
            self.remove_file(instance, field)

    def remove_file(self, instance, field_name):
        file = getattr(instance, field_name)

        if not file:
            return

        file.storage.delete(file.name)


@deconstructible
class AutoDeleteFileOnChange:

    def __init__(self, *field_names):
        self.field_names = field_names

    def __call__(self, sender, instance, **kwargs):
        print('lorem ipsum')
        if not instance.pk:
            return False

        for field in self.field_names:
            old_file = self.get_old_file(sender, instance, field)
            self.remove_file(instance, old_file, field)

    def get_old_file(self, sender, instance, field):
        try:
            old_instance = sender.objects.get(pk=instance.pk)
            old_file = getattr(old_instance, field)
        except sender.DoesNotExist:
            return None

        return old_file

    def remove_file(self, instance, old_file, field):
        if not old_file:
            return

        new_file = getattr(instance, field)

        if old_file == new_file:
            return

        if not old_file.storage.exists(old_file.name):
            return

        old_file.storage.delete(old_file.name)
