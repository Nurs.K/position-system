import os
import json

from celery import shared_task

from Positions.settings import DEVICE_STATUS_FILE_PATH

from apps.common.models import Device


@shared_task(
    name='apps.layouts.tasks.parse_device_status'
)
def parse_device_status():
    if not os.path.exists(DEVICE_STATUS_FILE_PATH):
        print('File does\'t exist')
        return

    with open(DEVICE_STATUS_FILE_PATH, 'r') as file:
        content = file.read()
        file.close()

    content_json: dict = json.loads(content)

    for key, value in content_json.items():
        if str(key).lower() == 'datetime':
            continue

        status = int(value) == 1

        try:
            device = Device.objects.get(name=key)
            device.status = status
            device.save()
        except Device.DoesNotExist:
            device = Device.objects.create(
                name=key,
                status=status
            )
