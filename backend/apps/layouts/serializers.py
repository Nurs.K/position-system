from rest_framework_gis.serializers import GeoFeatureModelSerializer
from rest_framework import serializers, exceptions

from apps.common.serializers import DeviceSerializer

from .models import LayoutCategory, Layout, Area, Camera, AreaPolygon, AreaCircle, AreaDevice


class LayoutCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LayoutCategory
        fields = ['id', 'name', 'thumbnail', 'order']
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }


class LayoutCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Layout
        fields = [
            'id',
            'name',
            'min_x',
            'max_x',
            'min_y',
            'max_y',
            'x',
            'y',
            'z',
            'segment_length',
            'floor',
            'category',
            'file',
            'is_favorite',
            'is_main',
            'remote_id',
            'point1',
            'point2',
            'point3',
        ]
        extra_kwargs = {
            'id': {
                'read_only': True
            },
            'name': {
                'required': True,
            },
        }

    is_favorite = serializers.SerializerMethodField(method_name='get_is_favorite', read_only=True)

    def get_is_favorite(self, instance: Layout):
        if 'request' not in self.context:
            print('Request not exists in context!')
            return False

        user = self.context['request'].user
        if user.is_authenticated:
            return user.favorite_layouts.filter(id=instance.id).exists()
        return False

    def validate_is_main(self, value):
        if value and Layout.objects.filter(is_main=True).exists():
            raise exceptions.ValidationError('Отключите текущую главную карту и затем сохраните эту карту как главную.')

        return value


class LayoutSerializer(LayoutCreateSerializer):
    category = LayoutCategorySerializer()


class LatLngSerializer(serializers.ListSerializer):
    child = serializers.FloatField()
    max_length = 2
    min_length = 2


class PolygonSerializer(serializers.ListSerializer):
    child = LatLngSerializer()


class AreaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Area
        fields = [
            'id',
            'name',
            'layout',
            'color',
            'figure_type',
            'attachment_type'
        ]
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }


class AreaPolygonSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = AreaPolygon
        fields = [
            'id',
            'area',
            'polygon'
        ]
        geo_field = 'polygon'
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }


class AreaCircleSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = AreaCircle
        fields = [
            'id',
            'area',
            'point',
            'radius'
        ]
        geo_field = 'point'
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }


class AreaDeviceSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = AreaDevice
        fields = [
            'id',
            'area',
            'point',
            'device'
        ]
        geo_field = 'point'
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }

    device = DeviceSerializer()


class CameraSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Camera
        fields = ['id', 'area', 'point', 'camera_id']
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }
        geo_field = 'point'

