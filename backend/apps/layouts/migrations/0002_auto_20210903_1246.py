# Generated by Django 2.2.16 on 2021-09-03 12:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layouts', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='layout',
            name='segment_length',
        ),
        migrations.RemoveField(
            model_name='layout',
            name='x',
        ),
        migrations.RemoveField(
            model_name='layout',
            name='y',
        ),
        migrations.AddField(
            model_name='layout',
            name='max_x',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='layout',
            name='max_y',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='layout',
            name='min_x',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='layout',
            name='min_y',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='layout',
            name='name',
            field=models.CharField(default='', max_length=255, verbose_name='Название'),
        ),
        migrations.AlterField(
            model_name='layout',
            name='z',
            field=models.FloatField(default=0),
        ),
    ]
