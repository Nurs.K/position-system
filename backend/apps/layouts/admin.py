from adminsortable2.admin import SortableAdminMixin
from jet.admin import CompactInline
from leaflet.admin import LeafletGeoAdmin

from django.contrib import admin
from django.contrib.gis import admin as gis_admin

from .models import Layout, LayoutCategory, Area


# Register your models here.


class AreaInline(CompactInline, admin.StackedInline):
    model = Area
    extra = 0


@admin.register(Layout)
class LayoutAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'category', 'file', 'floor']
    inlines = (AreaInline,)
    list_filter = ['category']


@admin.register(LayoutCategory)
class LayoutCategoryAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['name', 'id']


@gis_admin.register(Area)
class AreaAdmin(LeafletGeoAdmin, gis_admin.GeoModelAdmin):
    pass
