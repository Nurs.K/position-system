from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, response, status, exceptions, viewsets
from rest_framework.parsers import MultiPartParser

from apps.users.models import LayoutVisitHistory
from utils.pagination import SWAGGER_PAGINATION_KWARGS
from .models import LayoutCategory, Layout, Area
from .serializers import (
    LayoutCreateSerializer,
    LayoutSerializer,
    LayoutCategorySerializer,
    AreaSerializer,
    CameraSerializer,
    AreaPolygonSerializer,
    AreaCircleSerializer,
    AreaDeviceSerializer,
)


# Create your views here.


class LayoutCreateAPIView(generics.CreateAPIView):
    serializer_class = LayoutCreateSerializer

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        operation_summary='Upload layout',
        request_body=LayoutCreateSerializer(),
        responses={
            200: LayoutSerializer()
        }
    )
    def post(self, *args, **kwargs):
        return super(LayoutCreateAPIView, self).post(*args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        response_serializer = LayoutSerializer(instance=instance)
        return response.Response(response_serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        return serializer.save()


class LayoutDetailAPIView(generics.RetrieveAPIView):
    serializer_class = LayoutSerializer

    # permission_classes = (IsAuthenticated,)

    def get_object(self):
        try:
            layout = Layout.objects.get(id=self.kwargs.get('id'))
        except Layout.DoesNotExist:
            raise exceptions.ValidationError('Layout not found')
        except Exception as e:
            raise exceptions.ValidationError(str(e))

        LayoutVisitHistory.objects.create(user_id=self.request.user.id, layout_id=layout.id)

        return layout

    @swagger_auto_schema(
        operation_summary='Layout retrieve',
        responses={
            200: LayoutSerializer()
        },
    )
    def get(self, *args, **kwargs):
        return super(LayoutDetailAPIView, self).get(*args, **kwargs)


class LayoutListAPIView(generics.ListAPIView):
    serializer_class = LayoutSerializer
    # permission_classes = (IsAuthenticated,)
    queryset = Layout.objects.all().order_by('-created_at')

    @swagger_auto_schema(
        operation_summary='Layout LIST VIEW',
        **SWAGGER_PAGINATION_KWARGS,
    )
    def get(self, *args, **kwargs):
        return super(LayoutListAPIView, self).get(*args, **kwargs)


class LayoutDeleteAPIView(generics.DestroyAPIView):
    serializer_class = LayoutSerializer
    # permission_classes = (IsAuthenticated,)
    queryset = Layout.objects.all()

    def get_object(self) -> Layout:
        try:
            return Layout.objects.get(id=self.kwargs.get('id'))
        except Layout.DoesNotExist:
            raise exceptions.ValidationError(detail={
                'code': 400,
                'message': 'Layout not found'
            })
        except Exception as e:
            raise exceptions.ValidationError(detail={
                'code': 400,
                'message': str(e)
            })


class LayoutUpdateAPIView(generics.UpdateAPIView):
    serializer_class = LayoutCreateSerializer
    response_serializer_class = LayoutSerializer
    # permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)

    def get_object(self):
        try:
            return Layout.objects.get(id=self.kwargs.get('id'))
        except Layout.DoesNotExist:
            raise exceptions.NotFound(detail='Layout not found')
        except Exception as e:
            raise exceptions.ValidationError(detail=str(e))

    @swagger_auto_schema(auto_schema=None)
    def patch(self, request, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')

    @swagger_auto_schema(
        operation_summary='Update Layout',
        request_body=LayoutCreateSerializer(),
        responses={
            200: LayoutSerializer()
        }
    )
    def put(self, *args, **kwargs):
        return super(LayoutUpdateAPIView, self).put(*args, **kwargs)

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        response_serializer = self.response_serializer_class(instance=instance)
        return response.Response(response_serializer.data)

    def perform_update(self, serializer):
        is_main = serializer.validated_data.get('is_main')

        if is_main:
            items_to_update = []
            old_main = Layout.objects.filter(is_main=True)

            for item in old_main:
                item.is_main = False
                items_to_update.append(item)

            if len(items_to_update) > 0:
                Layout.objects.bulk_update(items_to_update, ['is_main'])

        return serializer.save()


class LayoutCategoryViewSetAPI(viewsets.ModelViewSet):
    serializer_class = LayoutCategorySerializer
    queryset = LayoutCategory.objects.all().order_by('order')
    http_method_names = ['get', 'post', 'put', 'delete', 'options', 'head']
    # permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser,)

    @swagger_auto_schema(auto_schema=None)
    def patch(self, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')

    def update(self, *args, **kwargs):
        return super(LayoutCategoryViewSetAPI, self).update(*args, partial=True, **kwargs)


class GetMainLayoutAPIView(generics.RetrieveAPIView):
    serializer_class = LayoutSerializer

    # permission_classes = (IsAuthenticated,)

    def get_object(self):
        target = Layout.objects.filter(is_main=True).first()

        if not target:
            raise exceptions.NotFound(detail='Layout not found')

        return target

    @swagger_auto_schema(
        operation_summary='Get main layout',
        responses={
            200: LayoutSerializer(),
            404: 'Layout not exists'
        }
    )
    def get(self, *args, **kwargs):
        return super(GetMainLayoutAPIView, self).get(*args, **kwargs)


class LayoutNextFloorAPIView(generics.RetrieveAPIView):
    serializer_class = LayoutSerializer

    # permission_classes = (IsAuthenticated,)

    @property
    def is_greater(self):
        return self.request.query_params.get('mode', 'up') == 'up'

    def get_object(self) -> Layout:
        try:
            return Layout.objects.get(id=self.kwargs.get('id'))
        except Layout.DoesNotExist:
            raise exceptions.ValidationError(detail={
                'code': 400,
                'message': 'Layout not found'
            })
        except Exception as e:
            raise exceptions.ValidationError(detail={
                'code': 400,
                'message': str(e)
            })

    @swagger_auto_schema(
        operation_summary='Получение следующего этажа',
        manual_parameters=[
            openapi.Parameter('id', openapi.IN_PATH, type=openapi.TYPE_STRING, format=openapi.FORMAT_UUID),
            openapi.Parameter('mode', openapi.IN_QUERY, type=openapi.TYPE_STRING, enum=['up', 'down'])
        ],
        responses={
            200: '{"next": "$uuid"}'
        }
    )
    def get(self, *args, **kwargs):
        mode = self.kwargs.get('mode', 'up')
        instance = self.get_object()
        category = instance.category_id

        if not category:
            raise exceptions.ParseError(detail={
                'code': 400,
                'message': 'У данной карты нет категории'
            })

        filters = dict(category_id=category)
        filters['floor__gt' if self.is_greater else 'floor__lt'] = instance.floor

        next_layout = Layout \
            .objects \
            .filter(**filters) \
            .order_by('floor' if self.is_greater else '-floor') \
            .first()

        if not next_layout:
            raise exceptions.ValidationError(detail={
                'code': 404,
                'message': 'Этот этаж последний'
            })

        return response.Response({
            'next': next_layout.id
        })


class AreaViewSet(viewsets.ModelViewSet):
    serializer_class = AreaSerializer
    queryset = Area.objects.all().order_by('-created_at')
    http_method_names = ['get', 'post', 'put', 'delete', 'options', 'head']

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(auto_schema=None)
    def patch(self, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')

    def update(self, *args, **kwargs):
        return super(AreaViewSet, self).update(*args, partial=True, **kwargs)

    def filter_queryset(self, queryset):
        layout_id = self.request.query_params.get('layout_id')
        figure_type = self.request.query_params.get('figure_type', None)
        attachment_type = self.request.query_params.get('attachment_type', None)

        filters = {}

        if layout_id:
            filters['layout_id'] = layout_id

        if attachment_type:
            filters['attachment_type'] = attachment_type

        if figure_type:
            filters['figure_type'] = figure_type

        return queryset.filter(**filters)


class AreaPolygonCreateAPIView(generics.CreateAPIView):
    serializer_class = AreaPolygonSerializer

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        operation_summary='Create polygon for area',
        request_body=AreaPolygonSerializer(),
        responses={
            200: AreaPolygonSerializer()
        }
    )
    def post(self, *args, **kwargs):
        return super().post(*args, **kwargs)


class AreaPolygonRetrieveUpdateAPIView(generics.RetrieveAPIView, generics.UpdateAPIView):
    serializer_class = AreaPolygonSerializer
    queryset = AreaPolygonSerializer.Meta.model.objects.all()

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(auto_schema=None)
    def patch(self, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')

    def update(self, *args, **kwargs):
        return super().update(*args, partial=True, **kwargs)

    def get_object(self):
        try:
            area = Area.objects.get(id=self.kwargs.get('id'))
        except Area.DoesNotExist:
            raise exceptions.ValidationError(detail='Area not found')

        polygon = self.queryset.filter(area_id=area.id).first()

        if not polygon:
            raise exceptions.NotFound(detail='polygon not found')
        return polygon


class AreaCircleCreateAPIView(generics.CreateAPIView):
    serializer_class = AreaCircleSerializer

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        operation_summary='Create circle for area',
        request_body=AreaCircleSerializer(),
        responses={
            200: AreaCircleSerializer()
        }
    )
    def post(self, *args, **kwargs):
        return super().post(*args, **kwargs)


class AreaCircleRetrieveUpdateAPIView(generics.RetrieveAPIView, generics.UpdateAPIView):
    serializer_class = AreaCircleSerializer
    queryset = AreaCircleSerializer.Meta.model.objects.all()

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(auto_schema=None)
    def patch(self, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')

    def update(self, *args, **kwargs):
        return super().update(*args, partial=True, **kwargs)

    def get_object(self):
        try:
            area = Area.objects.get(id=self.kwargs.get('id'))
        except Area.DoesNotExist:
            raise exceptions.ValidationError(detail='Area not found')

        circle = self.queryset.filter(area_id=area.id).first()

        if not circle:
            raise exceptions.NotFound(detail='Circle not found')
        return circle


class AreaDeviceCreateAPIView(generics.CreateAPIView):
    serializer_class = AreaDeviceSerializer

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        operation_summary='Create device for area',
        request_body=AreaDeviceSerializer(),
        responses={
            200: AreaDeviceSerializer()
        }
    )
    def post(self, *args, **kwargs):
        return super().post(*args, **kwargs)


class AreaDeviceRetrieveUpdateAPIView(generics.RetrieveAPIView, generics.UpdateAPIView):
    serializer_class = AreaDeviceSerializer
    queryset = AreaDeviceSerializer.Meta.model.objects.all()

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(auto_schema=None)
    def patch(self, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')

    def update(self, *args, **kwargs):
        return super().update(*args, partial=True, **kwargs)

    def get_object(self):
        try:
            area = Area.objects.get(id=self.kwargs.get('id'))
        except Area.DoesNotExist:
            raise exceptions.ValidationError(detail='Area not found')

        device = self.queryset.filter(area_id=area.id).first()

        if not device:
            raise exceptions.NotFound(detail='Device not found')
        return device


class CameraCreateAPIView(generics.CreateAPIView):
    serializer_class = CameraSerializer

    # permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        operation_summary='Создание метки и привязка камеры',
        request_body=CameraSerializer(),
        responses={
            201: CameraSerializer()
        }
    )
    def post(self, *args, **kwargs):
        return super().post(*args, **kwargs)


class CameraRetrieveUpdateAPIView(generics.RetrieveAPIView, generics.UpdateAPIView):
    serializer_class = CameraSerializer
    queryset = CameraSerializer.Meta.model.objects.all()

    # permission_classes = (IsAuthenticated,)

    def get_object(self):
        try:
            area = Area.objects.get(id=self.kwargs.get('id'))
        except Area.DoesNotExist:
            raise exceptions.ValidationError(detail='Area not found')

        camera = self.queryset.filter(area_id=area.id)
        if not camera:
            raise exceptions.NotFound(detail='Camera not found')
        return camera

    @swagger_auto_schema(
        operation_summary='Обновление камеры',
        request_body=CameraSerializer(),
        responses={
            200: CameraSerializer()
        }
    )
    def put(self, *args, **kwargs):
        return self.update(*args, **kwargs, partial=True)

    @swagger_auto_schema(auto_schema=None)
    def patch(self, *args, **kwargs):
        raise exceptions.MethodNotAllowed('PATCH')
