from django.core.management import BaseCommand

from apps.layouts.models import LayoutCategory

from utils.fixtures import LAYOUT_CATEGORIES


class Command(BaseCommand):
    help = 'Populate database with prepared seed for layout categories'

    def handle(self, *args, **options):
        self.stdout.write('Populating....', self.style.HTTP_INFO)
        for cat in LAYOUT_CATEGORIES:
            if LayoutCategory.objects.filter(name=cat).exists():
                self.stdout.write(f'Category with name `{cat}` doesn\'t exists, skiping...', self.style.HTTP_INFO)
                continue

            self.stdout.write(f'Creating `{cat}`...', self.style.HTTP_INFO)
            LayoutCategory.objects.create(name=cat)
            self.stdout.write('Done.', self.style.SUCCESS)
