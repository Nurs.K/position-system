from django.db import models
from django.contrib.gis.db import models as gis_models
from django.contrib.postgres.fields import JSONField

from apps.common.models import AbstractModel, AbstractOrderModel
from apps.common.mixins import WithFileFieldModelMixin

from utils.files import SetUploadPath


FIGURE_CHOICES = (
    ('polygon', 'Polygon'),
    ('circle', 'Circle')
)

ATTACHMENT_CHOICES = (
    ('none', 'None'),
    ('device', 'Device'),
    ('camera', 'Camera')
)

# Create your models here.


class LayoutCategory(WithFileFieldModelMixin, AbstractOrderModel):
    class Meta:
        verbose_name_plural = 'Категории планировки'
        verbose_name = 'Категории планировки'
        ordering = ('order',)

    name = models.CharField(max_length=255, verbose_name='Наименование')
    thumbnail = models.FileField(upload_to=SetUploadPath('layout-category'), null=True, blank=True)

    deleting_fields = ['thumbnail']

    def __str__(self):
        return self.name


class Layout(AbstractModel):
    class Meta:
        verbose_name = 'Планировка'
        verbose_name_plural = 'Планировка'
        ordering = ('floor',)

    is_main = models.BooleanField(default=False)

    name = models.CharField(max_length=255, verbose_name='Название', default='')
    min_x = models.FloatField(default=0.0)
    max_x = models.FloatField(default=0.0)
    min_y = models.FloatField(default=0.0)
    max_y = models.FloatField(default=0.0)
    x = models.FloatField(default=0.0)
    y = models.FloatField(default=0.0)
    z = models.FloatField(default=0.0)

    segment_length = models.FloatField(default=0.0)

    floor = models.IntegerField(default=0)
    category = models.ForeignKey(to='LayoutCategory', on_delete=models.SET_NULL, null=True, blank=True)

    file = models.FileField(upload_to=SetUploadPath('layouts'))

    remote_id = models.CharField(max_length=255, null=True, blank=True)

    point1 = JSONField(default=dict, blank=True, null=True)
    point2 = JSONField(default=dict, blank=True, null=True)
    point3 = JSONField(default=dict, blank=True, null=True)

    def __str__(self):
        if self.name:
            return self.name
        return f'<Layout floor="{self.floor}" file="{self.file}" category="{self.category}" />'


class Area(AbstractModel):
    class Meta:
        verbose_name = 'Область'
        verbose_name_plural = 'Области'

    name = models.CharField(max_length=255, null=True, blank=True)
    layout = models.ForeignKey(to='Layout', on_delete=models.CASCADE)
    color = models.CharField(max_length=19, null=True, blank=True)
    figure_type = models.CharField(max_length=50, choices=FIGURE_CHOICES, default='polygon')
    attachment_type = models.CharField(max_length=50, choices=ATTACHMENT_CHOICES, default='none')

    def __str__(self):
        if self.name:
            return self.name
        return f'{self.layout.__str__()} => area #{self.id}'


class AreaPolygon(AbstractModel):
    area = models.ForeignKey(to='Area', on_delete=models.CASCADE)
    polygon = gis_models.PolygonField(geography=False)


class AreaCircle(AbstractModel):
    area = models.ForeignKey(to='Area', on_delete=models.CASCADE)
    point = gis_models.PointField(geography=False)
    radius = models.FloatField(default=5.0)


class AreaDevice(AbstractModel):
    area = models.ForeignKey(to='Area', on_delete=models.CASCADE)
    point = gis_models.PointField(geography=False)
    device = models.ForeignKey(to='common.Device', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return str(self.area_id)


class Camera(AbstractModel):
    class Meta:
        verbose_name = 'Камера'
        verbose_name_plural = 'Камеры'

    area = models.ForeignKey(to='Area', on_delete=models.CASCADE)
    camera_id = models.CharField(max_length=255)
    point = gis_models.PointField(geography=False)

    def __str__(self):
        return self.camera_id
