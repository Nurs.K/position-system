from django.contrib import admin
from django.contrib.auth.admin import Group, UserAdmin as BaseUserAdmin

from .models import User


# Register your models here.


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    pass


admin.site.unregister(Group)
