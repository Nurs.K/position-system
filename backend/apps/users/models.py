import uuid

from django.db import models
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import AbstractUser

from apps.common.models import AbstractModel


# Create your models here.


class User(AbstractUser):
    id = models.UUIDField(unique=True, default=uuid.uuid4, editable=False, primary_key=True)
    email = models.EmailField(null=True, blank=True)

    session_auth = JSONField(default=dict, blank=True)

    favorite_layouts = models.ManyToManyField(to='layouts.Layout', blank=True)
    map_color = models.CharField(max_length=7, null=True, blank=True)

    def __str__(self):
        name = self.get_full_name()
        if not name:
            return self.username
        return name


class LayoutVisitHistory(AbstractModel):
    class Meta:
        verbose_name_plural = 'Layout visit log'
        verbose_name = 'Layout visit log'

    user = models.ForeignKey(to=User, on_delete=models.CASCADE, null=True, blank=True)
    layout = models.ForeignKey(to='layouts.Layout', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)

