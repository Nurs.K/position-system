RECENT = 'recent'
FREQUENTLY = 'frequently'

LAYOUT_HISTORY_ORDERING_CHOICES = (
    (RECENT, 'Recent'),
    (FREQUENTLY, 'Frequently Visited')
)
