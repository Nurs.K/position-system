from rest_framework import generics, views, exceptions, response, status
from rest_framework.permissions import IsAuthenticated
from rest_framework_simplejwt.views import TokenViewBase
from drf_yasg.utils import swagger_auto_schema

from django.db.models import Count, Subquery


from apps.users.serializers import LayoutVisitHistorySerializer
from apps.layouts.serializers import LayoutSerializer
from apps.layouts.models import Layout

from utils.pagination import SWAGGER_PAGINATION_KWARGS

from .models import LayoutVisitHistory, User
from .serializers import (
    LoginSerializer,
    UserColorSerializer,
    ProfileSerializer
)
from .swagger import TokenResponseSerializer


# Create your views here.


class SignInAPIView(TokenViewBase):
    serializer_class = LoginSerializer

    @swagger_auto_schema(
        operation_summary='Authorization',
        request_body=LoginSerializer(),
        responses={
            200: TokenResponseSerializer()
        }
    )
    def post(self, *args, **kwargs):
        return super(SignInAPIView, self).post(*args, **kwargs)


class RecentLayoutsAPIView(generics.ListAPIView):
    serializer_class = LayoutVisitHistorySerializer

    def get_queryset(self):
        # return LayoutVisitHistory.objects.filter(
        #     user_id=self.request.user.id
        # ).order_by('layout__id', '-created_at').distinct('layout')
        return LayoutVisitHistory.objects.filter(
            pk__in=Subquery(
                LayoutVisitHistory.objects.filter(
                    user_id=self.request.user.id
                ).order_by('layout__id').distinct('layout').values('pk')
            )
        ).order_by('-created_at')

    @swagger_auto_schema(
        operation_summary='Layout Visit history',
        **SWAGGER_PAGINATION_KWARGS
    )
    def get(self, *args, **kwargs):
        return super(RecentLayoutsAPIView, self).get(*args, **kwargs)


class FrequentlyLayoutsAPIView(generics.ListAPIView):
    serializer_class = LayoutSerializer

    def get_queryset(self):
        return Layout.objects.filter(layoutvisithistory__user_id=self.request.user.id).annotate(
            frequency=Count('layoutvisithistory')
        ).order_by('-frequency')

    @swagger_auto_schema(
        operation_summary='Frequently visited layouts',
        **SWAGGER_PAGINATION_KWARGS
    )
    def get(self, *args, **kwargs):
        return super(FrequentlyLayoutsAPIView, self).get(*args, **kwargs)


class FavoriteLayoutListAPIView(generics.ListAPIView):
    serializer_class = LayoutSerializer

    def get_queryset(self):
        return self.request.user.favorite_layouts.all()

    @swagger_auto_schema(
        operation_summary='Favorite layouts',
        **SWAGGER_PAGINATION_KWARGS
    )
    def get(self, *args, **kwargs):
        return super(FavoriteLayoutListAPIView, self).get(*args, **kwargs)


class AddRemoveFavoriteLayoutAPIView(views.APIView):

    def get_object(self):
        try:
            return Layout.objects.get(id=self.kwargs.get('id'))
        except Layout.DoesNotExist:
            raise exceptions.NotFound(detail='Layout not found')
        except Exception as e:
            raise exceptions.ValidationError(detail=str(e))

    @swagger_auto_schema(
        operation_summary='Add/Remove favorite layouts',
        responses={
            200: '{"result": "removed"}',
            202: '{"result": "added"}'
        }
    )
    def post(self, *args, **kwargs):
        instance = self.get_object()

        if self.request.user.favorite_layouts.filter(id=instance.id).exists():
            self.request.user.favorite_layouts.remove(instance)
            return response.Response({
                'result': 'removed'
            })

        self.request.user.favorite_layouts.add(instance)
        return response.Response({
            'result': 'added'
        }, status=status.HTTP_202_ACCEPTED)


class SaveUserColorAPIView(views.APIView):
    serializer_class = UserColorSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self) -> User:
        return self.request.user

    def get_serializer(self):
        if not self.serializer_class:
            raise exceptions.ValidationError(detail='No serializer class')
        return self.serializer_class

    @swagger_auto_schema(
        operation_summary='Сохранить цвет карты пользователя',
        request_body=UserColorSerializer(),
        responses={
            200: '{"map_color": "#000000"}',
        }
    )
    def post(self, *args, **kwargs):
        serializer = self.get_serializer()(data=self.request.data)
        serializer.is_valid(raise_exception=True)

        user = self.get_object()

        if user.map_color != serializer.validated_data.get('map_color'):
            user.map_color = serializer.validated_data.get('map_color')
            user.save(update_fields=['map_color'])

        return response.Response(serializer.data)


class ProfileAPIView(generics.RetrieveAPIView):
    serializer_class = ProfileSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user

    @swagger_auto_schema(
        operation_summary='Получение профиля'
    )
    def get(self, *args, **kwargs):
        return super(ProfileAPIView, self).get(*args, **kwargs)
