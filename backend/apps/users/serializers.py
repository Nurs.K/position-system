from django.utils import timezone
from rest_framework import serializers
from rest_framework_simplejwt.tokens import RefreshToken

from apps.layouts.serializers import LayoutSerializer
from utils.request_builder import RequestBuilder
from .models import User, LayoutVisitHistory


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(write_only=True, allow_null=False, allow_blank=False)
    password = serializers.CharField(write_only=True, allow_null=False, allow_blank=False)

    def __init__(self, *args, **kwargs):
        super(LoginSerializer, self).__init__(*args, **kwargs)
        self.user = None

    @classmethod
    def get_token(cls, user) -> RefreshToken:
        return RefreshToken.for_user(user)

    def get_user(self, attrs):
        try:
            user = User.objects.get(username=attrs.get('username'))
        except User.DoesNotExist:
            user = User(username=attrs.get('username'), is_active=True)
            user.set_password(attrs.get('password'))

        user.last_login = timezone.now()
        user.save()

        return user

    def read_cookies(self, response):
        cookies = {}

        for domain, paths in response.cookies._cookies.items():
            for path, data in paths.items():
                for name, cookie in data.items():
                    cookies[name] = {
                        'path': path,
                        'value': cookie.value
                    }

        return cookies

    def authenticate(self, attrs):

        request = RequestBuilder.post() \
            .with_path('auth') \
            .with_base_auth(**attrs) \
            .with_headers({
            'X-Requested-With': 'XMLHttpRequest',
        })

        response = request.send()
        cookies = self.read_cookies(response)

        self.user = self.get_user(attrs)
        self.user.session_auth = cookies
        self.user.save(update_fields=['session_auth'])

        return {'cookies': cookies}

    def validate(self, attrs):
        data = self.authenticate(attrs)

        refresh = self.get_token(self.user)

        data['access'] = refresh.access_token.__str__()

        return data


class LayoutVisitHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LayoutVisitHistory
        fields = (
            'id',
            'layout',
            'created_at'
        )

    layout = LayoutSerializer()


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'map_color',
            'id',
            'username'
        ]


class UserColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['map_color']
