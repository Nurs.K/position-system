from django.db import models

from apps.common.models import AbstractModel
from apps.common.mixins import WithFileFieldModelMixin

from utils.files import SetUploadPath

# Create your models here.


class NotificationConfiguration(WithFileFieldModelMixin, AbstractModel):
    class Meta:
        verbose_name_plural = 'Настройка уведомления'
        verbose_name = 'Настройка уведомления'

    is_active = models.BooleanField(default=True)
    title = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    audio = models.FileField(upload_to=SetUploadPath('notification/audio'), null=True, blank=True)
    thumbnail = models.FileField(upload_to=SetUploadPath('notification/thumbnails'), null=True, blank=True)
    event_type = models.CharField(max_length=255)

    deleting_fields = ['audio', 'thumbnail']

    def __str__(self):
        return self.event_type
