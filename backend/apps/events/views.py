from rest_framework import viewsets
from rest_framework.parsers import MultiPartParser

from .models import NotificationConfiguration
from .serializers import NotificationConfigurationSerializer


# Create your views here.


class NotificationConfigurationViewSetAPIView(viewsets.ModelViewSet):
    serializer_class = NotificationConfigurationSerializer
    queryset = NotificationConfiguration.objects.all().order_by('-created_at')
    http_method_names = ['get', 'post', 'put', 'delete', 'options', 'head']
    parser_classes = (MultiPartParser,)

    def update(self, *args, **kwargs):
        return super(NotificationConfigurationViewSetAPIView, self).update(*args, partial=True, **kwargs)
