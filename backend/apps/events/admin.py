from django.contrib import admin
from .models import NotificationConfiguration


# Register your models here.

@admin.register(NotificationConfiguration)
class NotificationConfigurationAdmin(admin.ModelAdmin):
    list_display = ['id', 'is_active', 'title', 'audio', 'thumbnail', 'event_type']
