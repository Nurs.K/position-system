from rest_framework import serializers

from .models import NotificationConfiguration


class NotificationConfigurationSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationConfiguration
        fields = [
            'id',
            'title',
            'description',
            'audio',
            'thumbnail',
            'event_type'
        ]
        extra_kwargs = {
            'id': {
                'read_only': True
            }
        }
