"""Positions URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib.gis import admin
from django.urls import path, include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework.routers import DefaultRouter

from Positions import settings
from apps.common import views as common_views
from apps.events import views as event_views
from apps.keepers import views as keepers_urls
from apps.layouts import views as layouts_views
from apps.users import views as user_views

schema_view = get_schema_view(
    openapi.Info(
        title="Positioning API",
        default_version='v1',
        description="API Documentation",
    ),
    public=True,
)

router = DefaultRouter()
router.register('categories', layouts_views.LayoutCategoryViewSetAPI)

area_router = DefaultRouter()
area_router.register('areas', layouts_views.AreaViewSet)

event_router = DefaultRouter()
event_router.register('events', event_views.NotificationConfigurationViewSetAPIView)

marker_router = DefaultRouter()
marker_router.register('markers', common_views.MapMarkerConfigurationViewSetAPIView)

device_router = DefaultRouter()
device_router.register('devices', common_views.DeviceViewSetAPIView)

notification_router = DefaultRouter()
notification_router.register('', keepers_urls.NotificationAPIViewSet)

urlpatterns = [
    path('jet/', include('jet.urls', 'jet')),
    path('admin/', admin.site.urls),

    path('api/', include([
        path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
        path('v1/', include([
            path('auth/', include([
                path('sign-in/', user_views.SignInAPIView.as_view(), name='sign_in'),
                path('layout/history/', user_views.RecentLayoutsAPIView.as_view(), name='layout_history'),
                path(
                    'layout/frequently-visited/',
                    user_views.FrequentlyLayoutsAPIView.as_view(),
                    name='layout_frequency'
                ),
                path('favorites/', user_views.FavoriteLayoutListAPIView.as_view(), name='favorite_layout_list'),
                path(
                    'favorites/<uuid:id>/',
                    user_views.AddRemoveFavoriteLayoutAPIView.as_view(),
                    name='favorite_layout_add'
                ),
                path(
                    'map-color/',
                    user_views.SaveUserColorAPIView.as_view(),
                    name='user_map_color',
                ),
                path(
                    'me/',
                    user_views.ProfileAPIView.as_view(),
                    name='profile'
                ),
            ])),

            path('layouts/', include([
                path('', include(router.urls)),
                path('', include(area_router.urls + [
                    path('areas/camera/', layouts_views.CameraCreateAPIView.as_view(), name='create_camera'),
                    path('areas/polygon/', layouts_views.AreaPolygonCreateAPIView.as_view(), name='create_polygon'),
                    path('areas/circle/', layouts_views.AreaCircleCreateAPIView.as_view(), name='create_circle'),
                    path('areas/device/', layouts_views.AreaDeviceCreateAPIView.as_view(), name='create_device'),
                    path(
                        'areas/<uuid:id>/camera/',
                        layouts_views.CameraRetrieveUpdateAPIView.as_view(),
                        name='update_camera'
                    ),
                    path(
                        'areas/<uuid:id>/polygon/',
                        layouts_views.AreaPolygonRetrieveUpdateAPIView.as_view(),
                        name='update_polygon'
                    ),
                    path(
                        'areas/<uuid:id>/circle/',
                        layouts_views.AreaCircleRetrieveUpdateAPIView.as_view(),
                        name='update_circle'
                    ),
                    path(
                        'areas/<uuid:id>/device/',
                        layouts_views.AreaDeviceRetrieveUpdateAPIView.as_view(),
                        name='update_device'
                    )
                ])),
                path('', include(device_router.urls)),
                path('upload/', layouts_views.LayoutCreateAPIView.as_view(), name='layout_upload'),
                path('list/', layouts_views.LayoutListAPIView.as_view(), name='layout_list'),
                path('<uuid:id>/delete/', layouts_views.LayoutDeleteAPIView.as_view(), name='layout_delete'),
                path('main/', layouts_views.GetMainLayoutAPIView.as_view(), name='layout_main'),
                path('<uuid:id>/', layouts_views.LayoutDetailAPIView.as_view(), name='layout_detail'),
                path('<uuid:id>/update/', layouts_views.LayoutUpdateAPIView.as_view(), name='layout_update'),
                path('<uuid:id>/next-floor/', layouts_views.LayoutNextFloorAPIView.as_view(), name='next_layout'),
            ])),

            path('common/', include([
                path('config/', include(marker_router.urls + [
                    path('macroscop/', common_views.MacroscopConfigAPIView.as_view(), name='macroscop_config'),
                ]))
            ])),

            path('', include(event_router.urls)),
            path('keepers/', keepers_urls.KeeperAPIView.as_view(), name='keepers'),
            path('keepers/<str:filter_params>', keepers_urls.KeeperWithFilterAPIView.as_view(), name='keepers_filter'),
            path('labels/', keepers_urls.LabelsAPIView.as_view(), name='labels'),

            # reports
            path('reports/tags/aggregated/', keepers_urls.TagAggregateAPIVIew.as_view(), name='tag_aggregate'),
            path('reports/config/aggregated/', keepers_urls.ConfigAggregateAPIVIew.as_view(), name='config_aggregate'),
            path('reports/anchors/aggregated/', keepers_urls.AnchorsAggregateAPIVIew.as_view(),
                 name='anchors_aggregate'),
            path('reports/keepers/aggregated/battery/', keepers_urls.BatteryAggregateAPIVIew.as_view(), name='battery'),
            path('reports/keepers/aggregated/map/', keepers_urls.MapAggregateAPIVIew.as_view(), name='map-aggregate'),
            path('reports/areas_log/aggregated/', keepers_urls.AreaLogAggregateAPIVIew.as_view(), name='area_log'),
            path('reports/tags_history/', keepers_urls.TagHistoryAPIVIew.as_view(), name='tag_history'),
            path('reports/sites_log_summary/', keepers_urls.Site_LOG_APIVIew.as_view(), name='sites-logs'),
            path('reports/audit/', keepers_urls.AuditAPIVIew.as_view(), name='audit'),  #
            path('reports/activity/', keepers_urls.ActivityAPIVIew.as_view(), name='activity'),
            path('reports/events/', keepers_urls.EventAPIVIew.as_view(), name='events'),
            path('maps/', keepers_urls.MapsAPIVIew.as_view(), name='maps'),

            # filter reports
            path('reports/tags/aggregated/<str:filter_params>', keepers_urls.TagAggregateFilterAPIVIew.as_view(),
                 name='tag_aggregate_filter'),
            path('reports/config/aggregated/<str:filter_params>', keepers_urls.ConfigAggregateFilterAPIVIew.as_view(),
                 name='config_aggregate_filter'),
            path('reports/anchors/aggregated/<str:filter_params>', keepers_urls.AnchorsAggregateFilterAPIVIew.as_view(),
                 name='anchors_aggregate_filter'),
            path('reports/keepers/aggregated/battery/<str:filter_params>',
                 keepers_urls.BatteryAggregateFilterAPIVIew, name='battery'),
            path('reports/keepers/aggregated/map/<str:filter_params>', keepers_urls.MapAggregateFilterAPIVIew.as_view(),
                 name='map-aggregate_filter'),
            path('reports/areas_log/aggregated/<str:filter_params>',
                 keepers_urls.AnchorsAggregateFilterAPIVIew.as_view(),
                 name='area_log_filter'),
            path('reports/tags_history/<str:filter_params>', keepers_urls.TagHistoryFilterAPIVIew.as_view(),
                 name='tag_history_filter'),
            path('reports/sites_log_summary/<str:filter_params>', keepers_urls.Site_LOG_FilterAPIVIew.as_view(),
                 name='sites-logs_filter'),
            path('reports/audit/<str:filter_params>', keepers_urls.AuditFilterAPIVIew.as_view(), name='audit_filter'),
            path('reports/activity/<str:filter_params>', keepers_urls.ActivityFilterAPIVIew.as_view(),
                 name='activity_filter'),
            path('reports/events/<str:filter_params>', keepers_urls.EventFilterAPIVIew.as_view(), name='events_filter'),

            path('shifts/', keepers_urls.ShiftsAPIVIew.as_view(), name='shifts'),
            path('notifications/', keepers_urls.NotificationsAPIView.as_view(), name='notifications'),
            path('tag_signal/<int:keeperId>/', keepers_urls.TagSignalAPIView.as_view(), name='tag-signals'),
            path('keepers/<int:id>/', keepers_urls.KeepersDetailAPIView.as_view(), name='keepers-detail'),
            path('items/<str:filter_params>', keepers_urls.ItemsWithFilterAPIView.as_view(), name='items_filter'),
            path('items/', keepers_urls.ItemsAPIView.as_view(), name='item'),
            path('areas/', keepers_urls.AreasAPIView.as_view(), name='areas'),
            path('areas/<str:filter_params>', keepers_urls.AreasWithFilterAPIView.as_view(), name='areas_filter'),
            path('employees/<str:filter_params>', keepers_urls.EmployeesWithFilterAPIView.as_view(),
                 name='employees_filter'),
            path('employees/', keepers_urls.EmployeesAPIView.as_view(), name='employees'),
            path('anchors/', keepers_urls.AnchorsAPIView.as_view(), name='anchors'),
            path('anchors/<str:filter_params>', keepers_urls.AnchorsWithFilterAPIView.as_view(), name='anchors_filter'),
            path('user/<str:filter_params>', keepers_urls.UserWithFilterAPIView.as_view(), name='user_filter'),
            path('user/', keepers_urls.UserAPIView.as_view(), name='user'),
            path('custom/notification/', include(notification_router.urls)),
            path('dicts/', keepers_urls.DictsAPIView.as_view(), name='dicts'),
            path('dicts/<str:filter_params>', keepers_urls.DictsFilterAPIView.as_view(), name='dicts_filter'),
            path('dicts/<str:event_type>', keepers_urls.DictsDetailAPIView.as_view(), name='dicts_detail'),

        ]))
    ]))
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
