from utils.request_builder import RequestBuilder


def send_get_request(username, password, path, access_token, cookies):
    request = RequestBuilder.get() \
        .with_path(path) \
        .with_base_auth(username, password) \
        .with_headers({
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': f'{access_token}',
        'JSESSIONID': f'{cookies}',
    })

    response = request.send()
    print(response.json())
    return response


def send_post_request(username, password, path, access_token, cookies):
    request = RequestBuilder.post() \
        .with_path(path) \
        .with_base_auth(username, password) \
        .with_headers({
        'X-Requested-With': 'XMLHttpRequest',
        'Authorization': f'{access_token}',
        'JSESSIONID': f'{cookies}', })

    response = request.send()
    print(request)
    return response

