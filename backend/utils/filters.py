from collections import OrderedDict

from django.db.models import Q
from django_filters import MultipleChoiceFilter
from django_filters.filterset import BaseFilterSet, FilterSetMetaclass
from django_filters.rest_framework import ModelMultipleChoiceFilter


class AvailableUIIDMultipleChoiceFilter(ModelMultipleChoiceFilter):
    def filter(self, qs, value):
        if not value:
            # Even though not a noop, no point filtering if empty.
            return qs

        if self.is_noop(qs, value):
            return qs

        if not self.conjoined:
            q = Q()
        for v in set(value):
            if v == self.null_value:
                v = None
            predicate = self.get_filter_predicate(v)
            for key in predicate:
                predicate[key] = str(predicate[key])

            if self.conjoined:
                qs = self.get_method(qs)(**predicate)
            else:
                q |= Q(**predicate)

        if not self.conjoined:
            qs = self.get_method(qs)(q)

        return qs.distinct() if self.distinct else qs


class VolatileFilterSetMetaclass(FilterSetMetaclass):
    @classmethod
    def get_declared_filters(cls, bases, attrs):
        result = super().get_declared_filters(bases, attrs)

        extra_filters = []

        if 'volatile_filters' in attrs:
            volatile_filters = attrs['volatile_filters']

            if isinstance(volatile_filters, list):
                for filter in volatile_filters:
                    extra_filters += [(field, filter['filter'](field)) for field in filter['fields']]

        return OrderedDict(list(result.items()) + extra_filters)


class VolatileFilterSet(BaseFilterSet, metaclass=VolatileFilterSetMetaclass):

    @classmethod
    def get_fields(cls):
        fields = super().get_fields()

        if 'volatile_filters' in cls.__dict__ and isinstance(cls.volatile_filters, list):
            for item in cls.volatile_filters:
                fields = OrderedDict(
                    list(fields.items())
                    + list([(field, ['exact']) for field in item['fields']])
                )
        return fields


class MultipleChoiceFilterWithValueEqualize(MultipleChoiceFilter):

    def __init__(self, *args, **kwargs):
        self.value_equalise = kwargs.pop('value_equalise', {})
        super().__init__(*args, **kwargs)

    def get_filter_predicate(self, v):
        res = super().get_filter_predicate(v)

        if v not in self.value_equalise:
            return res

        return {f'{self.field_name}__{self.value_equalise.get(v)}': v}
