import requests
from requests.auth import HTTPBasicAuth
from rest_framework import exceptions



class RequestBuilder:
    def __init__(self, method):
        self.method = method

        self.base_url = 'https://87.255.220.134:4433/BA-API/'

        self.path = ''
        self.data = None
        self.cookies = None
        self.auth = None
        self.headers = None
        self.verify = False

    @classmethod
    def post(cls):
        return cls(requests.post)

    @classmethod
    def get(cls):
        return cls(requests.get)

    @classmethod
    def put(cls):
        return cls(requests.put)

    @property
    def full_path(self):
        return f'{self.base_url}{self.path}'

    def with_path(self, path):
        self.path = path
        return self

    def with_data(self, data):
        if isinstance(data, dict):
            if not self.data:
                self.data = {}

            self.data.update(data)
        return self

    def with_cookies(self, cookie):
        if isinstance(cookie, dict):
            if not self.cookies:
                self.cookies = {}

            self.cookies.update(cookie)
        return self

    def with_headers(self, headers):
        if isinstance(headers, dict):
            if not self.headers:
                self.headers = {}

            self.headers.update(headers)
        return self

    def with_base_auth(self, username, password):
        self.auth = HTTPBasicAuth(username, password)
        return self

    def set_verify(self, value):
        self.verify = value
        return self

    def send(self):
        try:
            response = self.method(
                self.full_path,
                data=self.data,
                cookies=self.cookies,
                headers=self.headers,
                auth=self.auth,
                verify=False
            )
        except Exception as e:
            raise exceptions.ValidationError(detail=str(e))

        if response.status_code == 401:
            raise exceptions.AuthenticationFailed(response.json())
        if response.status_code not in [200, 201]:
            error = response.json()
            default_error = f'[BL API] HTTP {response.status_code}'
            if 'error' in error:
                raise exceptions.ValidationError(error.get('descr', default_error))
            raise exceptions.ValidationError(default_error)

        return response
